#pragma once

#include "af_blend.h"
#include "afterrain.h"
#include "afline.h"
#include <AntTweakBar.h>

namespace af
{
    //typedef enum {FOOT_UP, FOOT_TOE, FOOT_HEEL, FOOT_FLAT} FootState;

    class IKfixup : public blendnode
    {
    public:
        Terrain *terrain;
        
        // Variable indicating
        //float m_footliftratio;
        float m_hipsink;
        //unsigned m_predictionframes;
        //FootState m_leftfootstate;
        //FootState m_rightfootstate;

        // A bunch of constants hardcoded for the win
        //float m_leftfoottoesize, m_rightfoottoesize;
        //float m_leftminfootheight, m_rightminfootheight;
        //float m_leftmintoeheight, m_rightmintoeheight;

        // Current? I think this is how it should go
        float m_leftfootheight, m_rightfootheight;
        float m_lefttoeheight, m_righttoeheight;

        framedata *m_lefttoe, *m_leftfoot, *m_righttoe, *m_rightfoot;
        framedata *m_leftthigh, *m_leftcalf, *m_rightthigh, *m_rightcalf;
        framedata *m_leftthigh2, *m_rightthigh2;
        framedata *m_spine;

        //quat m_rightthighquat, m_rightcalfquat, m_leftthighquat, m_leftcalfquat;
        float m_leftthighangle, m_rightthighangle, m_leftcalfangle, m_rightcalfangle;

        Axis *m_leftfootaxis, *m_rightfootaxis;
        Line *m_preRotVector, *m_postRotVector, *m_rotAxis;

        vec3f m_rotaxis;

        vec3f m_a, m_k, m_v;
        float m_A, m_B, m_C;

        const framedata *const work();
        IKfixup();
        vec3f adjusthip(vec3f &tran, float rotation);
        void adjustlegs(matrix4x4f &vpmat, matrix4x4f &modelMat);
        virtual ~IKfixup();
    };
}