uniform sampler2D tex;
uniform vec3 lightPos;

varying vec2 texcoord;

float luminance(vec4 source)
{
	return .2126*source.x + .7152*source.y + .0722*source.z;
}

vec4 maxlum(vec2 texa, vec2 texb, vec2 texc, vec2 texd)
{
	vec4 lum;
	
	lum.x = luminance(texture2D(tex, texa));
	lum.y = luminance(texture2D(tex, texb));
	lum.z = luminance(texture2D(tex, texc));
	lum.w = luminance(texture2D(tex, texd));
	//return lum.xyzw;
					
	if(lum.x > lum.y && lum.x > lum.z && lum.x > lum.w)
		return texture2D(tex, texa);
	if(lum.y > lum.z && lum.y > lum.w)
		return texture2D(tex, texb);
	if(lum.z > lum.w)
		return texture2D(tex, texc);
	return texture2D(tex, texd);
}

void main()
{
	vec2 a, b, c, d;
	
	//gl_FragColor = vec4(lightPos.x*1000, lightPos.y*1000, 1.0, 1.0);
	
	a = texcoord + vec2(lightPos.x, lightPos.y);
	b = texcoord + vec2(-lightPos.x, lightPos.y);
	c = texcoord + vec2(lightPos.x, -lightPos.y);
	d = texcoord + vec2(-lightPos.x, -lightPos.y);
	
	gl_FragColor = maxlum(a, b, c, d);
}