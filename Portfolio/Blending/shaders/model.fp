uniform sampler2D difftex;

varying vec3 normal;
varying vec2 texcoord;
varying vec3 lightnormal;

void main()
{
    gl_FragColor = texture2D(difftex, texcoord) * max( dot( normalize(lightnormal), normalize( normal ) ), 0.0 );
}