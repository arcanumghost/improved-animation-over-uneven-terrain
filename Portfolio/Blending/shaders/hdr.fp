uniform sampler2D tex;
uniform sampler2D lighttex;

varying vec2 texcoord;

float luminance(vec4 source)
{
	return .2126*source.x + .7152*source.y + .0722*source.z;
}

void main()
{
	gl_FragColor = vec4((texture2D(tex, texcoord)/luminance(texture2D(lighttex, vec2(.5, .5)))).xyz,1.0);
}