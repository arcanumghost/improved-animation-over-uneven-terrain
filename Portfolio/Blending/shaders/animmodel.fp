uniform sampler2D difftex;
uniform sampler2D opactex;

varying vec3 normal;
varying vec2 texcoord;
varying vec3 lightnormal;

void main()
{
	if(texture2D(opactex, texcoord).rgb != vec3(1.0, 1.0, 1.0))
		discard;

	gl_FragColor = texture2D(difftex, texcoord) * max( dot( normalize(lightnormal).xyz, normalize( normal ).xyz ), 0.0 );
}