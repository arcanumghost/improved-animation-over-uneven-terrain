#pragma once

#include "af_model.h"

namespace af
{
    // This class is highly specialized for this application, it doesn't extend
    // to other uses.
    class Terrain : public Model
    {

    public:
        Terrain(char *path, Shader *_shader, Shader *_animshader = nullptr);
        vec3f getPosition(const vec3f &pos);

        // The data for the terrain, 200x200, [x][z], 51x51
        // Quads are divided diagonally by [1][0] [0][1] and coordinates from above
        // follow x right, z down orientation, as readily apparent from debug axes
        vector<vector<ModelVertex>> m_terrainVertices;
    };
}