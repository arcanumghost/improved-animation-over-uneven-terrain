#include "afterrain.h"

#include <vector>

using namespace std;

namespace af
{
    Terrain::Terrain(char *path, Shader *_shader, Shader *_animshader)
        : Model(path, _shader, _animshader)
    {
        vector<ModelVertex> vertices;
        vector<unsigned> indices;
        short header;
        fstream infile(path, std::ios::binary | std::ios::in );

        // Static seek to the terrain data of land.afm
        infile.seekg(4203);

        // Rip the terrain data right out of the afm file.
        {
            // Ripped code from Model to read terrain data
            infile.read((char *)&header, 2);
            unsigned int vertexlistsize;    infile.read((char *)&vertexlistsize, 4);

            vertices.resize((vertexlistsize-6)/32);
            infile.read((char *)&vertices[0], vertexlistsize-6);

            // Write default values in to the terrain vertex data
            m_terrainVertices.resize(51);
            for(unsigned x=0; x<m_terrainVertices.size(); ++x)
            {
                m_terrainVertices[x].resize(51);

                //* We can check these values on the other side to 
                // make sure that the whole terrain has been filled
                for(unsigned z=0; z<m_terrainVertices[x].size(); ++z)
                    m_terrainVertices[x][z].position = vec3f(-1,-1,-1);
                //*/
            }

            // Walk the vertices, placing their data appropriately in the terrain data
            for(unsigned i=0; i<vertices.size(); ++i)
                m_terrainVertices[vertices[i].position.x/4 + 25][vertices[i].position.z/4 + 25] = vertices[i];

            // Break for unfilled vertices. Should have worked out perfectly.
            for(unsigned x=0; x<m_terrainVertices.size(); ++x)
            {
                for(unsigned z=0; z<m_terrainVertices[x].size(); ++z)
                    if(m_terrainVertices[x][z].position.x == -1)
                        DebugBreak();
            }//*/
        }
    }

    vec3f Terrain::getPosition(const vec3f &pos)//, vec3f &normal)
    {
        // Index in to terrain
        int xindex = floor(pos.x/4.0f + 25.0f);
        int zindex = floor(pos.z/4.0f + 25.0f);

        vec3f dquad = (pos - vec3f((xindex - 25) * 4, 0, (zindex - 25) * 4))/4.0f;

        //* Needs to be replaced with logic that prevents character from leaving environment
        if(xindex < 0 || xindex >= 50 || zindex < 0 || zindex >= 50)
            return getPosition(vec3f(0,0,0));//*/

        ModelVertex &tv1 = m_terrainVertices[xindex][zindex+1];
        ModelVertex &tv2 = m_terrainVertices[xindex+1][zindex];
        ModelVertex &tv3 = (dquad.x + dquad.z) > 1.0f ? 
            m_terrainVertices[xindex+1][zindex+1] :
            m_terrainVertices[xindex][zindex];

        float det = (tv2.position.z - tv3.position.z)*(tv1.position.x - tv3.position.x) + 
                    (tv1.position.z - tv3.position.z)*(tv3.position.x - tv2.position.x);
        float w1 = ((tv2.position.z - tv3.position.z)*(pos.x - tv3.position.x) +
                   (tv3.position.x - tv2.position.x)*(pos.z - tv3.position.z))/det;
        float w2 = ((tv3.position.z - tv1.position.z)*(pos.x - tv3.position.x) +
                   (tv1.position.x - tv3.position.x)*(pos.z - tv3.position.z))/det;
        float w3 = 1.0f - w1 - w2;

        return w1*tv1.position + w2*tv2.position + w3*tv3.position;
    }
}