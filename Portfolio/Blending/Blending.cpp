// AFEngine.cpp : Defines the entry point for the application.
//
#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <cassert>
#include <crtdbg.h>
#include <iostream>
#include <sstream>

#include "Blending.h"

// Debugging includes
#include "guicon.h"
#include "af_error.h"
#include "af_test.h"

// Engine includes
#include "af_rndrr.h"
#include "aflinearblender.h"
#include "af_anim.h"
#include "af_time.h"
#include "af_cam.h"
#include "af_frstm.h"
#include "af_input.h"

// Object includes
#include "af_model.h"
#include "afterrain.h"
#include "af_axis.h"
#include "af_bsp.h"
#include "af_cube.h"
#include "afikfixup.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
HWND				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

// Frame cycle calls
void Input( HWND );
void Update( float, af::Axis *, af::Model &, af::Terrain & ); //, af::BSP &, af::Cube & );
void Render( HDC, HWND );

// Global objects for camera and frustum
//af::Camera g_camera;
af::Frustum g_frustum;

// constant hip displacement to ensure legs don't overextend.
// Later, calculate from 
float hipsink = 0.0f; //1.756f;

/*
af::value pause()
{
    g_paused = !g_paused;
    af::value v;
    v.m_type = af::V_ERROR;
    v.m_fptr = 0x0badf00d;
    return v;
}*/

int APIENTRY _tWinMain(HINSTANCE hInstance,
                       HINSTANCE hPrevInstance,
                       LPTSTR    lpCmdLine,
                       int       nCmdShow)
{
    // Set up Memory allocation debugging
#ifdef _DEBUG
    af::guicon::init();
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif  // _DEBUG
    // Uncomment this line and put the memory allocation # for where there is a leak
	// _CrtSetBreakAlloc( 5790 ); 

	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_BLENDING, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
    HWND hWnd = InitInstance (hInstance, nCmdShow);
    if(!hWnd)
	{
        MessageBox( NULL, TEXT( "Failed to Initialize Window" ), 
            TEXT( "Windows Error" ), MB_OK | MB_ICONERROR);
	}
	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_BLENDING));

	af::g_camera.init();
    
    // Initialize the renderer
    RECT clientRect;
    GetClientRect(hWnd, &clientRect); 
    float aspectRatio = 
        clientRect.right / static_cast< float >( clientRect.bottom );
    g_frustum.init( 45.0f, aspectRatio, 1.0f, 10000.0f );
    af::Renderer::initialize(hWnd, clientRect);

    TwAddVarRW(af::Renderer::getInstance().myBar, "hipsink", TW_TYPE_FLOAT, &hipsink, 
        "label='hipsink' min=0.0 max=3.0 step=.001");

    // Define default shader and axis that use it.
    af::Shader *defaultshader = af::Renderer::getInstance().createShader("shaders\\default");
    af::Axis axes[] = {af::Axis(5.0f, defaultshader), af::Axis(3.0f, defaultshader)};
    
    // Define model shader and the model that uses it.
    af::Shader *modelshader = af::Renderer::getInstance().createShader("shaders\\model");
    af::Shader *animshader = af::Renderer::getInstance().createShader("shaders\\animmodel");

    // Load up character, animations, and set up blending tree
    af::Model model = af::Model("girlidle.afm", modelshader, animshader);
    af::IKfixup *IKfixupnode;
    af::linearblender *idlewalknode;
    af::Anim *idleanim, *walkanim;

    //* Full blend tree, idle/walk blender
    model.m_blendtree = IKfixupnode = new af::IKfixup();
    IKfixupnode->source = idlewalknode = new af::linearblender();
    idlewalknode->m_baseinput = idleanim = new af::Anim("girlidle");
    idlewalknode->m_weightedinput = walkanim = new af::Anim("girl");

    af::blah.registerInputFunction(af::xboxeventtype(af::XBOX_ANGLED_STICK, VK_PAD_LTHUMB_PRESS), &af::Model::moveXBox, &model);
    af::blah.registerInputFunction(af::xboxeventtype(af::XBOX_DOWN, XINPUT_GAMEPAD_B), &af::Model::debugbreak, &model);

    af::Terrain level("land.afm", modelshader, animshader);
    ((af::IKfixup *)model.m_blendtree)->terrain = &level;

    // Set the camera position
    af::g_camera.setPosition(af::vec3f(10, 10, 40));

    // render function
    af::SystemClocks nextFrameTime = 0;
    float time = 0.0f;

    // Center the mouse so there are no adverse initial rotation effects
    SetCursorPos( 400, 400 );

	// Main message loop:
    bool quitgame = false;
    while(!quitgame)
    {
	    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	    {
            if( msg.message == WM_QUIT || (msg.message == WM_KEYDOWN && msg.wParam == VK_ESCAPE))
                quitgame = true;

		    if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		    {
			    TranslateMessage(&msg);
			    DispatchMessage(&msg);
		    }
	    }
        else
        {
            // Input
            if(GetFocus() == hWnd)
                af::blah.update( );

            // Update Game Objects
            Update( time, axes, model, level );
             
            // Render
            af::Renderer::getInstance().render();

            // Spin lock operations, once every frame
            {
                const static float SECONDS_PER_FRAME = 1.0f/60.0f;
                const static af::SystemClocks CLOCKS_PER_FRAME = af::SecondsToClocks( SECONDS_PER_FRAME );

                af::SystemClocks now = af::GetAbsoluteTimeClocks();
                while(now < nextFrameTime)
                    now = af::GetAbsoluteTimeClocks();

                nextFrameTime = now + CLOCKS_PER_FRAME;
                //if(!g_paused)
                time += SECONDS_PER_FRAME;
            }
        }
    }

    // clean up renderer and renderable objects
    af::Renderer::deleteInstance();
    
    // should be a destructor, but like everything else, this is a HACK.
    af::blah.clear();

    // clean up shaders
    delete defaultshader;
    delete modelshader;
    delete animshader;

    TwTerminate();

	return (int) msg.wParam;
}

void Update( float time, af::Axis *axes, af::Model &model, af::Terrain &level ) //, af::BSP &level, af::Cube &cube )
{
    model.blend();

    /* Debug tran / rot
    {
        model.m_rotation.y = -1.5165775;
        model.m_translation = af::vec3f(-26.745163, .34665966, 29.235832);
    }//*/
    
    
    model.m_translation = ((af::IKfixup *)model.m_blendtree)->adjusthip(
                                level.getPosition(model.m_translation),
                                model.m_rotation.y);
    model.m_translation = level.getPosition(model.m_translation);

    // Position the camera at the current orientation, positioned 100 units away
    // from the model's location.
    af::g_camera.setPosition(af::g_camera.camOrientation() * 20 + model.m_translation + af::vec3f(0, 5, 0));

    // update camera and frustum based on camera
    af::g_camera.update();
    g_frustum.update(af::g_camera);
    
    // Set up model rotations and translations:
    //af::vec3f translation(500, 50, -600);
    af::vec3f translation(0,0,0);
	af::matrix4x4f modelMatrix(af::IDENTITY);
	af::matrix4x4f yrotMatrix(af::IDENTITY);
	//af::matrix4x4f zrotMatrix(af::IDENTITY);
    af::matrix4x4f modelInverse(af::IDENTITY);

    modelMatrix[12] = model.m_translation.x;
    modelMatrix[13] = model.m_translation.y;
    modelMatrix[14] = model.m_translation.z;

	// HACK rotate the model to the desired location 
    yrotMatrix[0] =   yrotMatrix[10] = -cos(model.m_rotation.y);
    yrotMatrix[8] = -(yrotMatrix[2]  = -sin(model.m_rotation.y));

    ((af::IKfixup *)model.m_blendtree)->adjustlegs(af::g_camera.camView()*g_frustum.getProjectionMatrix(), yrotMatrix*modelMatrix);

    // update models
	level.update(time, af::g_camera.camView()*g_frustum.getProjectionMatrix(), af::matrix4x4f(af::IDENTITY), af::g_camera.lightPosition() );
	model.update(time, af::g_camera.camView()*g_frustum.getProjectionMatrix(), yrotMatrix*modelMatrix, af::g_camera.lightPosition(), true );

	// update axes
    axes[0].update( af::g_camera.camView()*g_frustum.getProjectionMatrix(), af::vec3f(0.0f, 0.0f, 0.0f) );
    axes[1].update( af::g_camera.camView()*g_frustum.getProjectionMatrix(), model.m_translation);
    //axes[1].update( af::g_camera.camView()*g_frustum.getProjectionMatrix(), level.getPosition(model.m_translation));
}









//// Boiler Plate, this stuff never changes
// CODE this needs to be wrapped in an application class of some kind

//
//  FUNCTION: MyRegisterClass()
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= af::InputController::WindowsProcedure;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_BLENDING));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= nullptr;
	wcex.lpszMenuName	= nullptr;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
HWND InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      200, 200, 1280, 720, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return hWnd;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}