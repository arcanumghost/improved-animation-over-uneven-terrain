#include "afikfixup.h"
#include "af_rndrr.h"

/* For reference, the bones in the Biped, by name
 * Bip001
 *   Bip001 Pelvis
 *     Bip001 Spine
 *       Bip001 R Thigh
 *         Bip001 R Calf
 *           Bip001 R Foot
 *             Bip001 R Toe0
 */

namespace af
{
    //TwEnumVal footEV[] = {{FOOT_UP, "Up"}, {FOOT_TOE, "Toe"}, {FOOT_HEEL, "Heel"}, {FOOT_FLAT, "Flat"}};
    //TwType FootType;

    IKfixup::IKfixup()
        : /*m_footliftratio(0.10f), m_predictionframes(15), 
        m_leftfootstate(FOOT_FLAT), m_rightfootstate(FOOT_FLAT),*/
        m_leftthigh2(new framedata()), m_rightthigh2(new framedata())
    {
        /*FootType = TwDefineEnum("FootType", footEV, 4);
        TwAddVarRW(Renderer::getInstance().myBar, "m_footliftratio", TW_TYPE_FLOAT, &m_footliftratio,
            "label='Foot-Lift Ratio' min=0.0 max=1.0 step=.001");
        TwAddVarRW(Renderer::getInstance().myBar, "m_predictionframes", TW_TYPE_INT32, &m_predictionframes,
            "label='Prediction Frames' min=5 max=30");
        TwAddVarRO(Renderer::getInstance().myBar, "m_leftfootstate", FootType, &m_leftfootstate,
            "label='Left Foot'");
        TwAddVarRO(Renderer::getInstance().myBar, "m_rightfootstate", FootType, &m_rightfootstate,
            "label='Right Foot'");*/

        /*TwAddVarRO(Renderer::getInstance().myBar, "lthighang", TW_TYPE_FLOAT, &m_leftthighangle, 
            "label='L Thigh Angle'");
        TwAddVarRO(Renderer::getInstance().myBar, "lcalfang", TW_TYPE_FLOAT, &m_leftcalfangle, 
            "label='L calf Angle'");*/

        TwAddVarRO(Renderer::getInstance().myBar, "m_a", TW_TYPE_DIR3F, &m_a,
            "label='m_a'");
        TwAddVarRO(Renderer::getInstance().myBar, "m_k", TW_TYPE_DIR3F, &m_k,
            "label='m_k'");
        TwAddVarRO(Renderer::getInstance().myBar, "m_v", TW_TYPE_DIR3F, &m_v,
            "label='m_v'");

        TwAddVarRO(Renderer::getInstance().myBar, "m_A", TW_TYPE_FLOAT, &m_A, 
            "label='m_A'");
        TwAddVarRO(Renderer::getInstance().myBar, "m_B", TW_TYPE_FLOAT, &m_B, 
            "label='m_B'");
        TwAddVarRO(Renderer::getInstance().myBar, "m_C", TW_TYPE_FLOAT, &m_C, 
            "label='m_C'");

        m_leftfootaxis = new Axis(2, af::Renderer::getInstance().createShader("shaders\\default"));
        m_rightfootaxis = new Axis(2, af::Renderer::getInstance().createShader("shaders\\default"));

        m_preRotVector = new Line(af::Renderer::getInstance().createShader("shaders\\default"));
        m_postRotVector = new Line(af::Renderer::getInstance().createShader("shaders\\default"));
        m_rotAxis = new Line(af::Renderer::getInstance().createShader("shaders\\default"));

        // This should be detected from the model, but I have better things to do right now
        //m_leftfoottoesize = 1.677495f;
        //m_rightfoottoesize = 1.711163f;
        //m_leftminfootheight = .951461f;
        //m_leftmintoeheight = .101049f;
        //m_rightminfootheight = .8619955f;
        //m_rightmintoeheight = -.0334065f;
    }

    IKfixup::~IKfixup()
    {

    }

    const framedata *const IKfixup::work()
    {
        //m_leftfootstate = m_rightfootstate = FOOT_UP;

        source->work();

        std::queue<framedata *> srcqueue;
        std::queue<framedata *> destqueue;

        srcqueue.push(&(source->m_workingframe));
        destqueue.push(&m_workingframe);

        framedata *fd = destqueue.front();
        framedata *fs = srcqueue.front();

        fd->m_rot = fs->m_rot;
        fd->m_tran = fs->m_tran;
        fd->m_scale = fs->m_scale;
        fd->m_name = fs->m_name;
        fd->m_matrix = fd->m_rot.mat4(fd->m_tran, fd->m_scale);

        while(srcqueue.size())
        {
            for(int i=0; i<srcqueue.front()->m_children.size(); ++i)
            {
                if(destqueue.front()->m_children.size() == i)
                    destqueue.front()->m_children.push_back(new framedata());

                fd = destqueue.front()->m_children[i];
                fs = srcqueue.front()->m_children[i];

                fd->m_rot = fs->m_rot;
                fd->m_tran = fs->m_tran;
                fd->m_scale = fs->m_scale;
                fd->m_name = fs->m_name;
                //printf("%s\n", fd->m_name.c_str());
                fd->m_matrix = fd->m_rot.mat4(fd->m_tran, fd->m_scale)*destqueue.front()->m_matrix;

                srcqueue.push(fs);
                destqueue.push(fd);

                if(!fd->m_name.compare("Bip001 R Toe0"))
                {
                    m_righttoeheight = fd->m_matrix.m[13];
                    m_righttoe = fd;
                }
                if(!fd->m_name.compare("Bip001 L Toe0"))
                {
                    m_lefttoeheight = fd->m_matrix.m[13];
                    m_lefttoe = fd;
                }
                if(!fd->m_name.compare("Bip001 R Foot"))
                {
                    m_rightfootheight = fd->m_matrix.m[13];
                    m_rightfoot = fd;
                }
                if(!fd->m_name.compare("Bip001 L Foot"))
                {
                    m_leftfootheight = fd->m_matrix.m[13];
                    m_leftfoot = fd;
                }
                if(!fd->m_name.compare("Bip001 R Thigh"))
                {
                    //ang = fd->m_rot.euler_angles();
                    //m_rightthighquat = fd->m_rot;
                    //fd->m_rot.normalize();
                    //m_rightthighquat = fd->m_rot;
                    m_rightthigh = fd;
                }
                if(!fd->m_name.compare("Bip001 R Calf"))
                {
                    //m_rightcalfquat = fd->m_rot;
                    m_rightcalf = fd;
                }
                if(!fd->m_name.compare("Bip001 L Thigh"))
                {
                    //m_leftthighquat = fd->m_rot;
                    m_leftthigh = fd;
                }
                if(!fd->m_name.compare("Bip001 L Calf"))
                {
                    //m_leftcalfquat = fd->m_rot;
                    m_leftcalf = fd;
                }
                if(!fd->m_name.compare("Bip001 Spine"))
                {
                    m_spine = fd;
                }
            }

            srcqueue.pop();
            destqueue.pop();
        }

        /*/ Left foot
        if(m_leftfootheight-m_leftminfootheight < m_footliftratio*m_leftfoottoesize)
            m_leftfootstate = FOOT_HEEL;
        if(m_lefttoeheight-m_leftmintoeheight < m_footliftratio*m_leftfoottoesize)
            m_leftfootstate = (m_leftfootstate == FOOT_HEEL) ? FOOT_FLAT : FOOT_TOE;

        // Right foot
        if(m_rightfootheight-m_rightminfootheight < m_footliftratio*m_rightfoottoesize)
            m_rightfootstate = FOOT_HEEL;
        if(m_righttoeheight-m_rightmintoeheight < m_footliftratio*m_rightfoottoesize)
            m_rightfootstate = (m_rightfootstate == FOOT_HEEL) ? FOOT_FLAT : FOOT_TOE;

        //printf("%f %f %f %f\n", m_leftfootheight, m_leftminfootheight, m_rightfootheight, m_rightminfootheight);//*/

        return &m_workingframe;
    }

    vec3f IKfixup::adjusthip(vec3f &tran, float rotation)
    {
        // Make modelMat
        matrix4x4f modelMat;
        {
            matrix4x4f yrotMat = matrix4x4f(af::IDENTITY);
            yrotMat[0] =   yrotMat[10] = -cos(rotation);
            yrotMat[8] = -(yrotMat[2]  = -sin(rotation));
            matrix4x4f tranMat = matrix4x4f(af::IDENTITY);
            tranMat[12] = tran.x;
            tranMat[13] = tran.y;
            tranMat[14] = tran.z;
            modelMat = yrotMat*tranMat;
        }

        // Left first ;D
        // Calculate length of leg
        // Find point on the terrain
        vec3f groundpos = terrain->getPosition(pos(m_leftfoot->m_matrix*modelMat)) + vec3f(0, m_leftfoot->m_matrix.m[13], 0);
        vec3f footpos = pos(m_leftfoot->m_matrix*modelMat);
        vec3f thighpos = pos(m_leftthigh->m_matrix*modelMat);
        vec3f calfpos = pos(m_leftcalf->m_matrix*modelMat);
        float leglength = (thighpos-calfpos).dist() + (calfpos-footpos).dist();
        float groundhiplength = (groundpos-thighpos).dist();

        // Do we need to fix it?
        if(groundhiplength > leglength)
        {
            // Fix it with Pythagoras. Hooray 11th grade.
            float grounddist = sqrt(pow(thighpos.z-groundpos.z, 2) + pow(thighpos.x-groundpos.x, 2));
            float newthighheight = sqrt(pow(leglength, 2) - pow(grounddist, 2));
            float oldthighheight = thighpos.y - groundpos.y;
            tran.y -= (oldthighheight - newthighheight);
        }

        {
            matrix4x4f yrotMat = matrix4x4f(af::IDENTITY);
            yrotMat[0] =   yrotMat[10] = -cos(rotation);
            yrotMat[8] = -(yrotMat[2]  = -sin(rotation));
            matrix4x4f tranMat = matrix4x4f(af::IDENTITY);
            tranMat[12] = tran.x;
            tranMat[13] = tran.y;
            tranMat[14] = tran.z;
            modelMat = yrotMat*tranMat;
        }

        // Right next
        // Calculate length of leg
        // Find point on the terrain
        groundpos = terrain->getPosition(pos(m_leftfoot->m_matrix*modelMat)) + vec3f(0, m_leftfoot->m_matrix.m[13], 0);
        footpos = pos(m_leftfoot->m_matrix*modelMat);
        thighpos = pos(m_leftthigh->m_matrix*modelMat);
        calfpos = pos(m_leftcalf->m_matrix*modelMat);
        leglength = (thighpos-calfpos).dist() + (calfpos-footpos).dist();
        groundhiplength = (groundpos-thighpos).dist();

        // Do we need to fix it?
        if(groundhiplength > leglength)
        {
            // Fix it with Pythagoras. Hooray 11th grade.
            float grounddist = sqrt(pow(thighpos.z-groundpos.z, 2) + pow(thighpos.x-groundpos.x, 2));
            float newthighheight = sqrt(pow(leglength, 2) - pow(grounddist, 2));
            float oldthighheight = thighpos.y - groundpos.y;
            tran.y -= (oldthighheight - newthighheight);
        }

        return tran;
    }

    void IKfixup::adjustlegs(matrix4x4f &vpmat, matrix4x4f &modelMat)
    {
        if(true)//m_leftfootstate != FOOT_UP)
        {
            matrix4x4f leftfootworld = m_leftfoot->m_matrix*modelMat;
            matrix4x4f leftthighworld = m_leftthigh->m_matrix*modelMat;
            matrix4x4f leftcalfworld = m_leftcalf->m_matrix*modelMat;

            m_k = zvec(leftthighworld);
            
            float lthigh, lcalf, lleg;
            vec3f groundpos = terrain->getPosition(pos(leftfootworld));
            vec3f footpos = groundpos + vec3f(0, m_leftfoot->m_matrix.m[13], 0);
            lthigh = (pos(leftthighworld) - pos(leftcalfworld)).dist();
            lcalf = (pos(leftcalfworld) - pos(leftfootworld)).dist();
            lleg = (pos(leftthighworld) - footpos).dist();

            // do law of cosines to determine the hip-foot angle from the straight-down angle
            // uses pos(leftthighworld), footpos, and pos(leftthighworld)-vec3f(0,5,0) (obv length 5)
            vec3f tthirdpoint = pos(leftthighworld) - vec3f(0, 5, 0);
            float ttptfoot = (footpos - tthirdpoint).dist();
            float hipfootangle = acos((lleg*lleg + 25 - ttptfoot*ttptfoot)/(10*lleg));

            //printf("%d %f %f %f %f\n", lthigh+lcalf > lleg, lthigh, lcalf, lleg, lthigh+lcalf);

            // law of cosines
            m_leftthighangle = acos((lthigh*lthigh + lleg*lleg - lcalf*lcalf)/(2*lthigh*lleg));
            m_leftcalfangle = PI - acos((lcalf*lcalf + lthigh*lthigh - lleg*lleg)/(2*lthigh*lcalf));

            if(lthigh+lcalf<=lleg)
            {
                m_leftthighangle = .5f;
                m_leftcalfangle = 0.0f;
            }
            
                // Set up triangle
                m_leftthigh->m_rot = quat(0, PI/2.0f, 0); // I can't believe this hack works. Amazing
                m_leftcalf->m_rot = quat(0, m_leftcalfangle, 0);
                m_leftfoot->comp();
                m_leftthigh->comp();
                m_leftcalf->comp();

                // Get the current position of the foot
                matrix4x4f agregate = m_leftfoot->m_matrix*m_leftcalf->m_matrix*m_leftthigh->m_matrix*m_spine->m_matrix*modelMat;
            
                // Determine an axis-angle rotation to get the foot in the right place
                vec3f thighpos, thighfoot, tthighfoot, tfootpos;
                tfootpos = pos(agregate);
                thighpos = pos(leftthighworld);
                m_a = thighfoot = footpos-thighpos;
                tthighfoot = tfootpos-thighpos;

                agregate = m_leftthigh->m_matrix*m_spine->m_matrix*modelMat;

                m_rotaxis = crossproduct(thighfoot, tthighfoot).normalized();
                m_rotaxis = quat(agregate).rotate(m_rotaxis);
                normalize(m_rotaxis);
                float rotangle = acos(dotproduct(thighfoot, tthighfoot)/(thighfoot.dist()*tthighfoot.dist()));

                m_rotAxis->update(agregate*vpmat, vec3f(0,0,0), (10*m_rotaxis));
                m_preRotVector->update(vpmat, thighpos, tfootpos);

                float tempx = m_leftthigh->m_rot.length();

                m_leftthigh->m_rot = quat::from_axis_angle(m_rotaxis, rotangle) * m_leftthigh->m_rot;

                //* Rotate the leg axis in to the axis most correct according to the artist animation.
                m_leftthigh->comp();
                m_leftthigh->m_matrix = m_leftthigh->m_matrix*m_spine->m_matrix;
                leftthighworld = m_leftthigh->m_matrix*modelMat;
                m_leftcalf->comp();
                m_leftcalf->m_matrix = m_leftcalf->m_matrix*m_leftthigh->m_matrix;
                leftcalfworld = m_leftcalf->m_matrix*modelMat;

                m_rotaxis = thighfoot.normalized();
                m_rotaxis = quat(leftthighworld).rotate(m_rotaxis);

                m_v = zvec(leftthighworld);

                m_A = dotproduct(m_a, m_v)*dotproduct(m_a,m_k) - dotproduct(crossproduct(crossproduct(m_a, m_v),m_a),m_k);
                m_B = dotproduct(crossproduct(m_a,m_v),m_k);
                m_C = dotproduct(m_v,m_k);

                float theta = atan(2*m_B/(m_C-m_A));

                if((((m_A-m_C)*cos(theta)/2.0f) - (m_B*sin(theta))) >= 0.0f)
                    theta += PI;

                m_leftthighangle = theta;

                m_leftthigh->m_rot = quat::from_axis_angle(m_rotaxis, theta) * m_leftthigh->m_rot;

                m_postRotVector->update(leftthighworld*vpmat, vec3f(0,0,0), 10*m_rotaxis);//*/

                /* Naive rotation around thighfoot axis to expected location.
                m_leftthigh->comp();
                agregate = m_leftthigh->m_matrix*m_spine->m_matrix*modelMat;
                m_rotaxis = thighfoot.normalized();
                m_rotaxis = quat(agregate).rotate(m_rotaxis);

                m_leftthigh->m_rot = quat::from_axis_angle(m_rotaxis, PI) * m_leftthigh->m_rot;
                m_leftthighangle = rotangle;
                */

                // Get the thigh in a place that puts the foot in the right spot.
                /*m_rightthighquat = quat(m_spine->m_matrix*modelMat);
                m_rightcalfquat = -quat(m_spine->m_matrix*modelMat);
                m_leftthighquat = m_spine->m_rot;
                m_leftcalfquat = quat(matrix4x4f(af::IDENTITY));

                // Comp matrices
                m_leftthigh->comp();
                m_leftthigh2->comp();
                m_leftcalf->comp();
                m_leftfoot->comp();

                // Find out which left thigh to use
                {
                    matrix4x4f agregate1 = m_leftfoot->m_matrix*m_leftcalf->m_matrix*m_leftthigh->m_matrix*m_spine->m_matrix*modelMat;
                    matrix4x4f agregate2 = m_leftfoot->m_matrix*m_leftcalf->m_matrix*m_leftthigh2->m_matrix*m_spine->m_matrix*modelMat;
                    float delta1 = (pos(agregate1) - footpos).dist();
                    float delta2 = (pos(agregate2) - footpos).dist();
                    if(delta2 < delta1)
                        m_leftthigh->m_rot = m_leftthigh2->m_rot;
                }*/

            

            m_leftfootaxis->update(vpmat, footpos);
        }
        if(true)//m_rightfootstate != FOOT_UP)
        {
            matrix4x4f rightfootworld = m_rightfoot->m_matrix*modelMat;
            matrix4x4f rightthighworld = m_rightthigh->m_matrix*modelMat;
            matrix4x4f rightcalfworld = m_rightcalf->m_matrix*modelMat;

            m_k = zvec(rightthighworld);
            
            float lthigh, lcalf, lleg;
            vec3f groundpos = terrain->getPosition(pos(rightfootworld));
            vec3f footpos = groundpos + vec3f(0, m_rightfoot->m_matrix.m[13], 0);
            lthigh = (pos(rightthighworld) - pos(rightcalfworld)).dist();
            lcalf = (pos(rightcalfworld) - pos(rightfootworld)).dist();
            lleg = (pos(rightthighworld) - footpos).dist();

            // do law of cosines to determine the hip-foot angle from the straight-down angle
            // uses pos(rightthighworld), footpos, and pos(rightthighworld)-vec3f(0,5,0) (obv length 5)
            vec3f tthirdpoint = pos(rightthighworld) - vec3f(0, 5, 0);
            float ttptfoot = (footpos - tthirdpoint).dist();
            float hipfootangle = acos((lleg*lleg + 25 - ttptfoot*ttptfoot)/(10*lleg));

            //printf("%d %f %f %f %f\n", lthigh+lcalf > lleg, lthigh, lcalf, lleg, lthigh+lcalf);

            // law of cosines
            m_rightthighangle = acos((lthigh*lthigh + lleg*lleg - lcalf*lcalf)/(2*lthigh*lleg));
            m_rightcalfangle = PI - acos((lcalf*lcalf + lthigh*lthigh - lleg*lleg)/(2*lthigh*lcalf));

            if(lthigh+lcalf<=lleg)
            {
                m_rightthighangle = .5f;
                m_rightcalfangle = 0.0f;
            }
            
                // Set up triangle
                m_rightthigh->m_rot = quat(0, PI/2.0f, 0); // I can't believe this hack works. Amazing
                m_rightcalf->m_rot = quat(0, m_rightcalfangle, 0);
                m_rightfoot->comp();
                m_rightthigh->comp();
                m_rightcalf->comp();

                // Get the current position of the foot
                matrix4x4f agregate = m_rightfoot->m_matrix*m_rightcalf->m_matrix*m_rightthigh->m_matrix*m_spine->m_matrix*modelMat;
            
                // Determine an axis-angle rotation to get the foot in the right place
                vec3f thighpos, thighfoot, tthighfoot, tfootpos;
                tfootpos = pos(agregate);
                thighpos = pos(rightthighworld);
                m_a = thighfoot = footpos-thighpos;
                tthighfoot = tfootpos-thighpos;

                agregate = m_rightthigh->m_matrix*m_spine->m_matrix*modelMat;

                m_rotaxis = crossproduct(thighfoot, tthighfoot).normalized();
                m_rotaxis = quat(agregate).rotate(m_rotaxis);
                normalize(m_rotaxis);
                float rotangle = acos(dotproduct(thighfoot, tthighfoot)/(thighfoot.dist()*tthighfoot.dist()));

                m_rotAxis->update(agregate*vpmat, vec3f(0,0,0), (10*m_rotaxis));
                m_preRotVector->update(vpmat, thighpos, tfootpos);

                float tempx = m_rightthigh->m_rot.length();

                m_rightthigh->m_rot = quat::from_axis_angle(m_rotaxis, rotangle) * m_rightthigh->m_rot;

                //* Rotate the leg axis in to the axis most correct according to the artist animation.
                m_rightthigh->comp();
                m_rightthigh->m_matrix = m_rightthigh->m_matrix*m_spine->m_matrix;
                rightthighworld = m_rightthigh->m_matrix*modelMat;
                m_rightcalf->comp();
                m_rightcalf->m_matrix = m_rightcalf->m_matrix*m_rightthigh->m_matrix;
                rightcalfworld = m_rightcalf->m_matrix*modelMat;

                m_rotaxis = thighfoot.normalized();
                m_rotaxis = quat(rightthighworld).rotate(m_rotaxis);

                m_v = zvec(rightthighworld);

                m_A = dotproduct(m_a, m_v)*dotproduct(m_a,m_k) - dotproduct(crossproduct(crossproduct(m_a, m_v),m_a),m_k);
                m_B = dotproduct(crossproduct(m_a,m_v),m_k);
                m_C = dotproduct(m_v,m_k);

                float theta = atan(2*m_B/(m_C-m_A));

                if((((m_A-m_C)*cos(theta)/2.0f) - (m_B*sin(theta))) >= 0.0f)
                    theta += PI;

                m_rightthighangle = theta;

                m_rightthigh->m_rot = quat::from_axis_angle(m_rotaxis, theta) * m_rightthigh->m_rot;

                m_postRotVector->update(rightthighworld*vpmat, vec3f(0,0,0), 10*m_rotaxis);//*/

            m_rightfootaxis->update(vpmat, footpos);
        }
    }
}