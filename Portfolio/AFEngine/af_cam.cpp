#include "af_cam.h"
#include "af_utils.h"
#include "af_input.h"
#include <cmath>

const float CAMERA_DRAG = .2f;

namespace af
{
	const float Camera::KEYBOARD_IMPULSE_SCALAR = .8f;
	const float Camera::KEYBOARD_ROTATION_SCALAR = .03f;

    Camera g_camera;
 
	Camera::Camera( )
        : m_cam_velocity(0,0,0), m_cam_position(0,0,0), m_cam_impulse(0,0,0), m_cam_rotdelta(0,0,0),
          m_cam_rotation(0,0,0), m_updatePVSPosition(true), m_updateLightPosition(true)
    {
	}

	void Camera::init()
	{
		blah.registerInputFunction(keyeventtype(KEY_DOWN, 'W'), &Camera::moveForward, this);
		blah.registerInputFunction(keyeventtype(KEY_DOWN, 'A'), &Camera::moveLeft, this);
		blah.registerInputFunction(keyeventtype(KEY_DOWN, 'S'), &Camera::moveBack, this);
		blah.registerInputFunction(keyeventtype(KEY_DOWN, 'D'), &Camera::moveRight, this);

		blah.registerInputFunction(keyeventtype(KEY_DOWN, VK_CONTROL), &Camera::moveDown, this);
		blah.registerInputFunction(keyeventtype(KEY_DOWN, VK_SPACE), &Camera::moveUp, this);
		
		blah.registerInputFunction(keyeventtype(KEY_DOWN, VK_LEFT), &Camera::rotLeft, this);
		blah.registerInputFunction(keyeventtype(KEY_DOWN, VK_UP), &Camera::rotUp, this);
		blah.registerInputFunction(keyeventtype(KEY_DOWN, VK_RIGHT), &Camera::rotRight, this);
		blah.registerInputFunction(keyeventtype(KEY_DOWN, VK_DOWN), &Camera::rotDown, this);

		blah.registerInputFunction(keyeventtype(KEY_PRESS, 'L'), &Camera::negateLightUpdate, this);
        
        //blah.registerInputFunction(xboxeventtype(XBOX_ANGLED_STICK, VK_PAD_LTHUMB_PRESS), &Camera::moveXBox, this);
        blah.registerInputFunction(xboxeventtype(XBOX_ANGLED_STICK, VK_PAD_RTHUMB_PRESS), &Camera::rotXBox, this);        
	}

    void Camera::moveXBox(xboxeventdata &e)
    {
        m_cam_impulse.x = KEYBOARD_IMPULSE_SCALAR*e.pos.x;
        m_cam_impulse.z = -KEYBOARD_IMPULSE_SCALAR*e.pos.y;
    }

    void Camera::rotXBox(xboxeventdata &e)
    {
        m_cam_rotdelta.x = KEYBOARD_ROTATION_SCALAR*e.pos.y;
        m_cam_rotdelta.y = -KEYBOARD_ROTATION_SCALAR*e.pos.x;
        rotate(m_cam_rotdelta);
    }

	void Camera::moveForward(keyeventtype &e)
    {
		m_cam_impulse.z -= KEYBOARD_IMPULSE_SCALAR;
	}

	void Camera::moveBack(keyeventtype &e)
	{
		m_cam_impulse.z += KEYBOARD_IMPULSE_SCALAR;
	}

	void Camera::moveUp(keyeventtype &e)
	{
		m_cam_impulse.y += KEYBOARD_IMPULSE_SCALAR;
	}

	void Camera::moveDown(keyeventtype &e)
	{
		m_cam_impulse.y -= KEYBOARD_IMPULSE_SCALAR;
	}

	void Camera::moveLeft(keyeventtype &e)
	{
		m_cam_impulse.x -= KEYBOARD_IMPULSE_SCALAR;
	}

	void Camera::moveRight(keyeventtype &e)
	{
		m_cam_impulse.x += KEYBOARD_IMPULSE_SCALAR;
	}

	void Camera::rotLeft(keyeventtype &e)
	{
		m_cam_rotdelta.y += KEYBOARD_ROTATION_SCALAR;
	}
	
	void Camera::rotRight(keyeventtype &e)
	{
		m_cam_rotdelta.y -= KEYBOARD_ROTATION_SCALAR;
	}
	
	void Camera::rotUp(keyeventtype &e)
	{
		m_cam_rotdelta.x += KEYBOARD_ROTATION_SCALAR;
	}
	
	void Camera::rotDown(keyeventtype &e)
	{
		m_cam_rotdelta.x -= KEYBOARD_ROTATION_SCALAR;
	}

    // Apply a camera-aligned impulse on the camera position
    void Camera::applyImpulse(const vec3f& impulse)
    {
        vec3f rotImp(cos(m_cam_rotation.x)*sin(m_cam_rotation.y)*impulse.z,
                    -sin(m_cam_rotation.x)*impulse.z,
                     cos(m_cam_rotation.x)*cos(m_cam_rotation.y)*impulse.z);

        rotImp += vec3f(cos(m_cam_rotation.y)*impulse.x,
                        0.0f,
                       -sin(m_cam_rotation.y)*impulse.x);

        rotImp += vec3f(sin(m_cam_rotation.x)*sin(m_cam_rotation.y)*impulse.y,
                        cos(m_cam_rotation.x)*impulse.y,
                        sin(m_cam_rotation.x)*cos(m_cam_rotation.y)*impulse.y);
        m_cam_velocity += rotImp;
    }

    // Forward Z from the camera
    vec3f Camera::camOrientation() const
    {
        return vec3f(cos(m_cam_rotation.x)*sin(m_cam_rotation.y),
                     -sin(m_cam_rotation.x),
                     cos(m_cam_rotation.x)*cos(m_cam_rotation.y));
    }

    // Update
    void Camera::update()
    {
		applyImpulse(m_cam_impulse);
		/* HACK moving rotate for camera to event handler for controller so
        // it can be updated before the model input event handler which needs
        // the orientation to set the camera position
        rotate(m_cam_rotdelta); //*/

        // update velocity and position
        //m_cam_velocity -= m_cam_velocity*CAMERA_DRAG;
        //m_cam_position += m_cam_velocity;

        // set the view matrix
        setViewMatrix( m_cam_position, m_cam_rotation, m_cam_view);

        // update position for potentially visible surfaces
        if(m_updatePVSPosition)
            m_pvs_position = m_cam_position;
        if(m_updateLightPosition)
        {
            m_light_position = m_cam_position;
            m_light_view = m_cam_view;
        }

		m_cam_impulse = m_cam_rotdelta = vec3f(0,0,0);
    }

    // rotate the camera
    void Camera::rotate( const vec3f& change )
    {
        m_cam_rotation += change;
        clamp(m_cam_rotation.x, -HALF_PI, HALF_PI);
    }

    // create a view matrix
    void Camera::setViewMatrix( const vec3f& m_position, const vec3f& m_rotation, matrix4x4f& m_view, bool inv)
    {
        matrix4x4f rotx(IDENTITY), roty(IDENTITY), tran(IDENTITY);
        
        roty[0] = roty[10] = cos(m_rotation.y);
        roty[8] = -(roty[2] = sin(m_rotation.y));

        rotx[5] = rotx[10] = cos(m_rotation.x);
        rotx[6] = -(rotx[9] = sin(m_rotation.x));

        tran[12] = -m_position.x;
        tran[13] = -m_position.y;
        tran[14] = -m_position.z;

        m_view = tran*roty*rotx;
    }
}