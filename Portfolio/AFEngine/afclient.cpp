#include "afclient.h"

namespace af
{
	client::client(boost::asio::io_service &_service, const char *ipaddr, short port)
		: service(_service), sock(_service), serveraddr(address::from_string(ipaddr), port)
	{
		sock.open(udp::v4());
	}

	void client::send()
	{}

	void client::handle_send_to(const boost::system::error_code &error,
		size_t bytes)
	{
		if(!error & (bytes > 0))
			send();
	}

	void client::handle_receive_from(const boost::system::error_code& error, size_t bytes)
	{
		if (!error && bytes > 0)
			receive(bytes);
		start_receive();
	}

	void client::start_send()
	{
		sock.async_send_to(boost::asio::buffer(packetout.data.raw, packetout.datalength),
			serveraddr,
			boost::bind(&client::handle_send_to, this,
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
	}

	void client::start_receive()
	{
		sock.async_receive_from(
			boost::asio::buffer(packetin.raw, MAXPACKETSIZE),
			serveraddr,
			boost::bind(&client::handle_receive_from, this,
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
	}

	packet client::packetout;
	packet::packetdata client::packetin;
}