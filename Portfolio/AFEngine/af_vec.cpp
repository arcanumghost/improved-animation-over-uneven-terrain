#include "af_vec.h"

namespace af
{
    // common vec3f functions
    float dotproduct(const vec3f& l, const vec3f& r)
    {
        return (l.x*r.x) + (l.y*r.y) + (l.z*r.z);
    }

    vec3f crossproduct(vec3f& l, vec3f& r)
    {
        return vec3f(l.y*r.z - l.z*r.y, l.z*r.x - l.x*r.z, l.x*r.y - l.y*r.x);
    }

    void normalize(vec3f& vec)
    {
        float length = sqrt(dotproduct(vec, vec));
        if(length == 0.0f)
        {
            vec.x = vec.y = vec.z = 1.0f;
            length = 3.0f;
        }
        vec /= length;
    }
}