#include "targetver.h"

#ifdef _WIN32
#include <Windows.h>
#endif

#include "af_time.h"

// Time helper functions
namespace af
{
#ifdef _WIN32
    double GetAbsoluteTimeSeconds()
    {
        return GetAbsoluteTimeClocks() / GetClockFrequencyDouble();
    }

    SystemClocks GetAbsoluteTimeClocks()
    {
        LARGE_INTEGER time;
        QueryPerformanceCounter( &time );
        return time.QuadPart;
    }

    SystemClocks GetClockFrequency()
    {
        LARGE_INTEGER frequency;
        QueryPerformanceFrequency( &frequency );
        return frequency.QuadPart;
    }

    double GetClockFrequencyDouble()
    {
        return static_cast< double >( GetClockFrequency() );
    }

    double ClocksToSeconds( SystemClocks clocks )
    {
        return clocks / GetClockFrequencyDouble();
    }

    SystemClocks SecondsToClocks( double seconds )
    {
        return static_cast< SystemClocks >( seconds  *GetClockFrequency() );
    }
#endif
}