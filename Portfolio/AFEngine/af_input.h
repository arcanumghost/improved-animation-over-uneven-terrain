#pragma once

#include "targetver.h"

#ifdef _WIN32
#include <Windows.h>
#endif

#include "XBOX_Controller.h"
#include "afevent.h"
#include "afkeys.h"
#include <vector>

using namespace std;

namespace af
{
	struct value;

    class InputController
    {
        // constant mouse location, locked within the window
        static const int MOUSE_CENTER_X = 400;
        static const int MOUSE_CENTER_Y = 400;
        
        // current states for the keyboard
        static BYTE prevStates[256];
		static BYTE currentStates[256];

#define keyeventhandler pair<keyeventtype, rootfunctionbind<keyeventtype> *>
		vector<keyeventhandler> keyeventhandlers;

#define mouseeventhandler pair<mouseeventtype, rootfunctionbind<mouseeventdata> *>
		vector<mouseeventhandler> mouseeventhandlers;

#define xboxeventhandler pair<xboxeventtype, rootfunctionbind<xboxeventdata> *>
		vector<xboxeventhandler> xboxeventhandlers;

        static XBOX_Controller s_controller;

        // inline helper function, only used for space bar
        inline static bool IsKeyDown(int vKey)
        {
            return ( GetAsyncKeyState( vKey ) & 0x8000 ) == 0x8000;
        }

    public:
        // Need the initial mouse position before updating
		void update();
		static LRESULT CALLBACK WindowsProcedure(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
        
        template<typename functiontype, typename classtype>
		void registerInputFunction(keyeventtype keyevent, functiontype f, classtype thisptr)
		{
			keyeventhandlers.push_back(keyeventhandler(keyevent, new functionbind<functiontype, classtype, keyeventtype>(f, thisptr)));
		}

		template<typename functiontype, typename classtype>
		void registerInputFunction(mouseeventtype mouseevent, functiontype f, classtype thisptr)
		{
			mouseeventhandlers.push_back(mouseeventhandler(mouseevent, new functionbind<functiontype, classtype, mouseeventdata>(f, thisptr)));
		}

		template<typename functiontype, typename classtype>
		void registerInputFunction(xboxeventtype xboxevent, functiontype f, classtype thisptr)
		{
			xboxeventhandlers.push_back(xboxeventhandler(xboxevent, new functionbind<functiontype, classtype, xboxeventdata>(f, thisptr)));
		}
        
        // Native functions used by runescript
        static value keystate(value &key);
        static value keydown(value &key);
        static value keyup(value &key);

        // HACK temporary destructor, a real destructor should be used when this is encapsulated
        // within a scope instead of using the "blah" extern below.
        static void clear();
    };

	// oh my god, Charlie, learn c++
	extern InputController blah;
}