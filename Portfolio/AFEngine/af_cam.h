#ifndef _AF_CAMERA_H_
#define _AF_CAMERA_H_

#include "af_matrx.h"
#include "af_vec.h"
#include "afevent.h"
#include "af_model.h"

namespace af
{
    class Camera
    {
		// Impulse for Keyboard input
		static const float KEYBOARD_IMPULSE_SCALAR; // = .8f;
		static const float KEYBOARD_ROTATION_SCALAR; // = .03f;

        // PVS variables
        vec3f m_pvs_position;
        bool m_updatePVSPosition;

        // Light variables
        vec3f m_light_position;
        bool m_updateLightPosition;

        // Camera-relative variables
        vec3f m_cam_position;
        vec3f m_cam_rotation;
        vec3f m_cam_velocity;
		vec3f m_cam_impulse;
		vec3f m_cam_rotdelta;
        
        // Camera tracks the view matrix
        matrix4x4f m_cam_view;
        matrix4x4f m_light_view;

    public:
        Camera( );
		
		void moveForward(keyeventtype &);
		void moveBack(keyeventtype &);
		void moveUp(keyeventtype &);
		void moveDown(keyeventtype &);
		void moveLeft(keyeventtype &);
		void moveRight(keyeventtype &);
        void moveXBox(xboxeventdata &e);

		void rotLeft(keyeventtype &);
		void rotRight(keyeventtype &);
		void rotUp(keyeventtype &);
		void rotDown(keyeventtype &);
        void rotXBox(xboxeventdata &e);

        // Lots of set and update methods
        void update( );
		void init( );
        void rotate( const vec3f& dRotation );
        void applyImpulse( const vec3f& impulse );
        void setViewMatrix( const vec3f& m_position, const vec3f& m_rotation, matrix4x4f& m_view, bool inv = false );
        void setLookAtMatrix( const vec3f& m_position, vec3f& zDir, matrix4x4f& m_view );

        // All the gets and sets, very Java / C#
        vec3f modelForward();
        const vec3f& camPosition() const            { return m_cam_position; }
        void setPosition( const vec3f& position )   { m_cam_position = position; }
        const vec3f& pvsPosition() const            { return m_pvs_position; }
        const vec3f& lightPosition() const          { return m_light_position; }
        const vec3f& rotation() const               { return m_cam_rotation; }
        vec3f camOrientation() const;
        void setRotation( const vec3f& rotation )   { m_cam_rotation = rotation; }
        const matrix4x4f& camView()                 { return m_cam_view; }
        const matrix4x4f& lightView()               { return m_light_view; }
        bool updatePVSPosition()                    { return m_updatePVSPosition; }
        bool updateLightPosition()                  { return m_updateLightPosition; }
        void negatePVSUpdate()                      { m_updatePVSPosition = !m_updatePVSPosition; }
        void negateLightUpdate(keyeventtype &e)		{ m_updateLightPosition = !m_updateLightPosition; }

        Model *m_focus;
    };

    extern Camera g_camera;
}

#endif