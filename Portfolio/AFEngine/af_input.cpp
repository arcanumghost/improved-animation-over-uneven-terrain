#include "af_input.h"
#include "XBOX_Controller.h"
#include <algorithm>
#include "af_utils.h"
#include <AntTweakBar.h>
//#include "af_blend.h"

namespace af
{
	InputController blah;
    
    // state variables for pvs and light
	BYTE InputController::prevStates[256];
	BYTE InputController::currentStates[256];
	//vector<eventhandler> InputController::eventhandlers;

    XBOX_Controller InputController::s_controller( 0 );

    void InputController::update()
    {
        //const float KEYBOARD_IMPULSE_SCALAR = .8f;
		//vec3f cameraImpulse( 0, 0, 0 );

		//vec2f modelcoords;

        /*if( s_controller.IsConnected() )
        {
            s_controller.Update();

			float yrot = c.rotation().y;
			vec2f joycoords = s_controller.GetLJoyCoordinates();

            modelcoords.x = (joycoords.y*sin(yrot) - joycoords.x*cos(yrot));
            modelcoords.y = (joycoords.y*cos(yrot) + joycoords.x*sin(yrot));

			AnimBlender &blender = AnimBlender::getInstance();
            blender.m_goalweights[0] = 1.0f - joycoords.dist();
            blender.m_goalweights[1] = joycoords.dist();

            clamp(modelcoords.x, -1.0f, 1.0f);
            clamp(modelcoords.y, -1.0f, 1.0f);

            if( abs(modelcoords.y) >= .05f || abs(modelcoords.x) >= .05f)
            {
                m.m_rotation_goal.y = acos(modelcoords.x);
                if(modelcoords.y < 0.0f)
                    m.m_rotation_goal.y *= -1.0f;F
            }
            // cout << acos(modelcoords.x) << "  ";
			if(m.m_translation.y < .05f)
				m.m_jumping = false;
			if(s_controller.IsDown(XINPUT_GAMEPAD_A) && !m.m_jumping)
			{
				m.m_jumping = true;
				m.m_verticalspeed = 200;
				blender.play("gryjump", 3.0f);
				blender.m_frames[2] = 0;
			}
        }*/

        // Check keys for movement
        /*
        if( IsKeyDown( 'W' ))
        {
            cameraImpulse.z -= KEYBOARD_IMPULSE_SCALAR;
        }
        if( IsKeyDown( 'S' ))
        {
            cameraImpulse.z += KEYBOARD_IMPULSE_SCALAR;
        }
        if( IsKeyDown( 'A' ))
        {
            cameraImpulse.x -= KEYBOARD_IMPULSE_SCALAR;
        }
        if( IsKeyDown( 'D' ))
        {
            cameraImpulse.x += KEYBOARD_IMPULSE_SCALAR;
        }
        if( IsKeyDown( VK_SPACE ))
        {
            cameraImpulse.y += KEYBOARD_IMPULSE_SCALAR;
        }
        if( IsKeyDown( VK_CONTROL ) || IsKeyDown( 'C' ))
        {
            cameraImpulse.y -= KEYBOARD_IMPULSE_SCALAR;
        }//*/

        /*bool stateup = IsKeyDown( VK_PRIOR );
        if( IsKeyDown( VK_PRIOR ) && !prevup )
        {
            modelindex--;
            if(modelindex < 0)
                modelindex = nmodels-1;
        }
        prevup = stateup;
        bool statedown = IsKeyDown( VK_NEXT );
        if( IsKeyDown( VK_NEXT ) && !prevdown)
        {
            modelindex++;
            if(modelindex > nmodels-1)
                modelindex = 0;
        }
        prevdown = statedown;*/
        
        // Check keys for pvs and light update states
        /*bool state_p = IsKeyDown( 'P' );
        if( IsKeyDown( 'P' ) && !prevStates['P'])
        {
            c.negatePVSUpdate();
        }
        prevStates['P'] = state_p;*/
        
        // Check keys for pvs and light update states
        /*bool state_x = IsKeyDown( 'X' );
        if( IsKeyDown( 'X' ) && !prevState_x)
        {
            model.negateTimeUpdate();
        }
        prevState_x = state_x;*/

        /*bool state_l = IsKeyDown( 'L' );
        if( IsKeyDown( 'L' ) && !prevStates['L'])
        {
            c.negateLightUpdate();
        }*/

		GetKeyboardState( currentStates );

		for_each(keyeventhandlers.begin(), keyeventhandlers.end(), [&](keyeventhandler ehandler)
		{
			switch(ehandler.first.event)
			{
			case KEY_DOWN:
				if(currentStates[ehandler.first.key] & KEY_UPDOWN)
					ehandler.second->fire(ehandler.first);
				break;
			case KEY_PRESS:
				if(currentStates[ehandler.first.key] & KEY_UPDOWN &&
					!(prevStates[ehandler.first.key] & KEY_UPDOWN))
						ehandler.second->fire(ehandler.first);
				break;
			/*case KEY_PRESS:
				if((currentStates[ehandler.first.key] & KEY_PRESSRELEASE) ==
					prevStates)
			/*case KEY_UP:
				if(currentStates[ehandler.first.key]])*/
			}
		});

		//printf("Up: %d / %d\n", currentStates[38], prevStates[38]);

		GetKeyboardState( prevStates );

        s_controller.Update();

        for_each(xboxeventhandlers.begin(), xboxeventhandlers.end(), [&](xboxeventhandler ehandler)
        {
            switch(ehandler.first.event)
            {
            case XBOX_ANGLED_STICK:
                if(ehandler.first.key == VK_PAD_LTHUMB_PRESS)
                {
                    xboxeventdata tempxboxdata(ehandler.first, s_controller.GetLJoyCoordinates());
                    ehandler.second->fire(tempxboxdata);
                }
                else if(ehandler.first.key == VK_PAD_RTHUMB_PRESS)
                {
                    xboxeventdata tempxboxdata(ehandler.first, s_controller.GetRJoyCoordinates());
                    ehandler.second->fire(tempxboxdata);
                }
                break;
            }
        });

        /*for(unsigned i = 0; i<256; ++i)
            InputController::prevStates[i] = InputController::IsKeyDown(i);*/

        // apply the impulse to the camera
        // c.applyImpulse( cameraImpulse );

        // Rotate camera
        /*POINT mousePos;
        GetCursorPos( &mousePos );
        float rotationSpeed = 0.03f;
        vec3f mouseDelta( rotationSpeed * s_controller.GetRJoyCoordinates().y ,
                          -rotationSpeed * s_controller.GetRJoyCoordinates().x );*/
        /*c.rotate( mouseDelta );

        c.setPosition( c.camOrientation() * 100.0f + m.m_translation + vec3f(0, 20.0f, 0));*/

        /*cout << c.camOrientation().x << " " << c.camOrientation().y << " " << c.camOrientation().z << " " <<
            m.m_translation.x << " " << m.m_translation.y << " " << m.m_translation.z << endl;*/

		/*if(m.m_rotation.y > 3.14159f/2.0f && m.m_rotation_goal.y < -3.14159f/2.0f)
			m.m_rotation_goal.y += 6.28318f;
		else if(m.m_rotation.y < -3.14159f/2.0f && m.m_rotation_goal.y > 3.14159f/2.0f)
			m.m_rotation_goal.y -= 6.28318f;
		
		//cout << m.m_rotation.y << " " << m.m_rotation_goal.y << endl;
		
		m.m_rotation.y = m.m_rotation.y*.9f + m.m_rotation_goal.y*.1f;
		
		if(m.m_rotation.y < -3.14159f)
			m.m_rotation.y += 6.28318f;
		else if(m.m_rotation.y > 3.14159f)
			m.m_rotation.y  -= 6.28318f;

		m.m_translation.x -= KEYBOARD_IMPULSE_SCALAR * cos(m.m_rotation.y) * modelcoords.dist();
		m.m_translation.z -= KEYBOARD_IMPULSE_SCALAR * sin(m.m_rotation.y) * modelcoords.dist();*/

        // Recenter mouse
        //SetCursorPos( MOUSE_CENTER_X, MOUSE_CENTER_Y );
    }

	LRESULT CALLBACK InputController::WindowsProcedure(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
        if( TwEventWin(hWnd, message, wParam, lParam))
            return 0;

		int wmId, wmEvent;

		switch (message)
		{
		case WM_COMMAND:
			wmId    = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			// Parse the menu selections:
			switch (wmId)
			{
			/*case IDM_ABOUT:
				DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
				break;
			case IDM_EXIT:
				DestroyWindow(hWnd);
				break;*/
			default:
				return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_MOUSEMOVE:
			//printf("mousemoved %d, %d\n", LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_LBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_LBUTTONDBLCLK:
			break;
		case WM_RBUTTONDOWN:
		case WM_RBUTTONUP:
		case WM_RBUTTONDBLCLK:
			break;
		case WM_MBUTTONDOWN:
		case WM_MBUTTONUP:
		case WM_MBUTTONDBLCLK:
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		return 0;
	}

    void InputController::clear()
    {
        for(int i=0; i<blah.keyeventhandlers.size(); ++i)
            delete blah.keyeventhandlers[i].second;
        for(int i=0; i<blah.mouseeventhandlers.size(); ++i)
            delete blah.mouseeventhandlers[i].second;
        for(int i=0; i<blah.xboxeventhandlers.size(); ++i)
            delete blah.xboxeventhandlers[i].second;
    }
}