#include "af_blend.h"
#include "af_quat.h"
#include "guicon.h"

namespace af
{
    AnimBlender::AnimBlender()
        : m_nanims(0)
    {
        for(unsigned i=0; i<8; ++i)
        {
            m_anims[i] = nullptr;
            m_frames[i] = 0;
            m_weights[i] = 0.0f;
        }
    }

    void AnimBlender::initialize()
    {
        setInstance(new AnimBlender());
    }

    void AnimBlender::registeranim(char *path)
    {
        if(!m_animdigest.count(path))
            m_animdigest[path] = new Anim(path);
    }

    void AnimBlender::play(std::string animname, float weight)
    {
        AnimBlender &blender = AnimBlender::getInstance(); 
        if(blender.m_nanims < 8)
        {
            blender.m_anims[blender.m_nanims] = blender.m_animdigest[animname];
            blender.m_goalweights[blender.m_nanims] = weight;
            blender.m_frames[blender.m_nanims] = 0;
            blender.m_nanims++;
        }
    }

    void AnimBlender::update()
    {
        if(!m_nanims)
        {
            play("girlidle", .5f);
            play("girl", .5f);
        }
        if(m_frames[0] == m_anims[0]->m_nframes)
            m_frames[0] = 0;

		//*
		if(m_frames[1] == m_anims[1]->m_nframes)
            m_frames[1] = 0;//*/

		if(m_anims[2] && m_anims[2]->m_nframes-m_frames[2] == 30)
		{
			m_goalweights[2] = 0.0f;
		}
		//cout << m_weights[2] << " ";

        float totalweight = 0.0f;
        unsigned i=0;

        for(unsigned i=0; i<m_nanims; i++)
            m_weights[i] = m_weights[i]*.9f + (m_goalweights[i])*.1f;

        for(i=0; i<m_nanims; ++i)
        {
            //cout << m_nanims << endl;
            if(m_frames[i] < m_anims[i]->m_nframes)
            {
                // Most of the work happens here
                //m_anims[i]->update(m_frames[i]);
                totalweight += m_weights[i];
                m_animqueues[i].push(m_anims[i]);
            }
            else
            {
                --m_nanims;
                for(unsigned j=i+1; j<8; ++j)
                {
                    m_anims[j-1] = m_anims[j];
                    m_frames[j-1] = m_frames[j];
                    m_weights[j-1] = m_weights[j];
                }
                m_anims[m_nanims] = nullptr;
                m_frames[m_nanims] = 0;
                m_weights[m_nanims] = 0.0f;
            }
            ++m_frames[i];
        }

        //matrix4x4f tmatrix;

        for(; m_nanims > 0 && m_animqueues[0].size();)
        {  
            float pastweight = totalweight = 0.0f;
            //m_bonedata[i] = matrix4x4f(ZERO);

            vec3f tran(0,0,0);
			vec3f scale(0,0,0);
            quat rot;

            for(unsigned j=0; j<m_nanims; ++j)
            {
                if(m_weights[j] != 0.0f)
                {
                    af::Anim *b = m_animqueues[j].front();
                
                    totalweight += m_weights[j];

					//tmatrix = b->m_m_matrix;
					/*if(i>=43)
					{
						//printf("%d %f %f %f %f\n", i, b->m_rot.s, b->m_rot.v.x, b->m_rot.v.y, b->m_rot.v.z);
						/*int x=3; x;
						tmatrix[ 8] *= -1;
						tmatrix[ 9] *= -1;
						tmatrix[10] *= -1;
					}*/
                    //source = quat(tmatrix);

					tran += m_weights[j]*b->m_tran;
					scale += m_weights[j]*b->m_scale;
                    /*if(totalweight == 0.0)
                        DebugBreak();*/
                    rot = quat::slerp(b->m_rot, rot, pastweight/totalweight);
                
                    // Update the part taken care of
                    pastweight = totalweight;
                }
            }

            // Change the result back in to a matrix4x4f
            tran /= totalweight;
			scale /= totalweight;

            // Place that matrix in the first animation
			//matrix4x4f tmatrix2 = rot.mat4(tran);
			/*if(tmatrix != tmatrix2)
			{
				int x=5; x;
			}*/
			/*if(i==43)
			{
				tmatrix2[8] *= -1;
				tmatrix2[9] *= -1;
				tmatrix2[10] *= -1;
			}*/
			/*
			if(i == 43)
				tmatrix2 = tmatrix;//*/
            m_animqueues[0].front()->m_rot = rot;
			m_animqueues[0].front()->m_tran = tran;
			m_animqueues[0].front()->m_scale = scale;
			//= tmatrix2;

            for(int j=0; j<m_nanims; ++j)
            {
                for(int k=0; k<m_animqueues[j].front()->m_children.size(); ++k)
                {
                    m_animqueues[j].push(m_animqueues[j].front()->m_children[k]);
                }
                m_animqueues[j].pop();
            }
        }

		// todo just let the model do this step
        // Let the first animation walk its structure, performing multiplication
        // on the relative matrices
        if(m_nanims > 0)
            m_anims[0]->updateanim(matrix4x4f(IDENTITY));

        i=0;

        // Save the final data in to the bone data matrices the model will pull
        for(; m_nanims > 0 && i<m_anims[0]->m_boneslist->size(); ++i)
            m_bonedata[i] = (*m_anims[0]->m_boneslist)[i]->m_m_matrix;
        for(;i<100; ++i)
            m_bonedata[i] = matrix4x4f(IDENTITY);
    }
}
