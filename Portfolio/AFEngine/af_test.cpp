#include "af_test.h"
#include "guicon.h"

namespace af
{
    unsigned passedtests = 0;
    unsigned totaltests = 0; 

    void inctest() { totaltests++; }
    void incpass() { passedtests++; }

    void checktests()
    {
        unsigned tpassed = passedtests;
        unsigned ttotal = totaltests;

        // Example first test case
        testcase("Testing basic tests", [&]() -> bool
        {
            return true;
        });

        // Test exceptions
        testerrorcase<af::afexception>("Testing exception tests", [&]() -> bool
        {
            throw af::afexception();
        });

        report();

        passedtests = tpassed;
        totaltests = ttotal;
    }

    void testcase(char *testname, std::function<bool ()> test)
    {
#ifdef _WIN32
        guicon::color(0x000F);
#endif

        inctest();
        std::cout << testname << ": ... ";

        bool passed;
        try
        {
            passed = test();
        }
        catch( afexception &e )
        {
            std::cout << ' ' << e.what() << ' ';
            passed = false;
        }
        catch( ... )
        {
            std::cout << " Unknown error throw ";
            passed = false;
        }

        if(passed)
            incpass();

#ifdef _WIN32
        guicon::color( passed ? FOREGROUND_GREEN : FOREGROUND_RED);
#endif
        std::cout << (passed ? " OK\n" : " Failed\n");
#ifdef _WIN32
        guicon::color(0x000F);
#endif
    }

    void report()
    {
#ifdef _WIN32
        guicon::color( passedtests == totaltests ? FOREGROUND_GREEN : FOREGROUND_RED);
#endif
        std::cout << "\nResults: " << passedtests << '/' << totaltests << "\n\n";
#ifdef _WIN32
        guicon::color(0x000F);
#endif
    }
}