#include "aflinearblender.h"
#include <queue>

namespace af
{
    linearblender::~linearblender()
    {
        delete m_baseinput;
        delete m_weightedinput;
    }

    // Walks the animqueues provided by previous nodes, 
    const framedata *const linearblender::work()
    {
        // Make source framedata do their work
        // BUG needs to do this once per frame, if two blendnodes source the same data, this could happen twice
        // need a failsafe here.
        m_baseinput->work();
        m_weightedinput->work();

        // Queues for walking framedata structures
        std::queue<framedata *> m_animqueues[2];
        std::queue<framedata *> m_destqueue;
        
        // Set initial framedata for sources and destinations
        m_animqueues[0].push(&(m_baseinput->m_workingframe));
        m_animqueues[1].push(&(m_weightedinput->m_workingframe));
        m_destqueue.push(&m_workingframe);

        // Continue working until there are no more framedata nodes
        while(m_animqueues[0].size())
        {
            // blend in to destqueue directly from sources, all on the front framedata
            m_destqueue.front()->m_tran = (1.0f - m_weight)*m_animqueues[0].front()->m_tran
                                                 + m_weight*m_animqueues[1].front()->m_tran;

            m_destqueue.front()->m_scale = (1.0f - m_weight)*m_animqueues[0].front()->m_scale
                                                  + m_weight*m_animqueues[1].front()->m_scale;

            m_destqueue.front()->m_rot = quat::slerp(m_animqueues[0].front()->m_rot,
                                                     m_animqueues[1].front()->m_rot, m_weight);

            m_destqueue.front()->m_name = m_animqueues[0].front()->m_name;

            // walk children, creating framedata children for m_workingframe if necessary, and adding
            // children to all three framedata queues.
            for(int j=0; j<m_animqueues[0].front()->m_children.size(); ++j)
            {
                if(m_destqueue.front()->m_children.size() == j)
                    m_destqueue.front()->m_children.push_back(new framedata());

                m_animqueues[0].push(m_animqueues[0].front()->m_children[j]);
                m_animqueues[1].push(m_animqueues[1].front()->m_children[j]);
                m_destqueue.push(m_destqueue.front()->m_children[j]);
            }
            m_animqueues[0].pop();
            m_animqueues[1].pop();
            m_destqueue.pop();
        }

        return &m_workingframe;
    }
}