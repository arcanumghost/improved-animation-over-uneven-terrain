#ifndef _AF_CUBE_H_
#define _AF_CUBE_H_

#include "af_obj.h"

namespace af
{
    // Cube Vertex
    struct CubeVertex
    {
        vec3f position;
        vec2f texCoord;
        vec3f normal;
        float tangent[4];
    };

    // Cube object
    class Cube : public Object
    {
        // Sets up vertices with normals, tangents, rotated and translated
        void setUpVertices( vec3f position, std::vector<CubeVertex> &vBuffer, std::vector<uint> &iBuffer, int nVertices, int nIndices );

    public:
        // initializer and updater
        Cube(vec3f position, float side, wchar_t *basetexturename, Shader *shader);
        void update( matrix4x4f &mvp_matrix, matrix4x4f &m_matrix, vec3f &lightPos, vec3f &lightPos2, vec3f &camPos );

        void objectShadowRender();

        // Variables for uniforms
        matrix4x4f m_lvpMatrix;
        vec3f m_lightPos;
        vec3f m_lightPos2;
        vec3f m_camPos;
        vec3f m_center;
    };
}

#endif