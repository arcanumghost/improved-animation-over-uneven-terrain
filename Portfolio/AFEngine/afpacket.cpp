#include "afpacket.h"
#include <string>

namespace af
{
	packet::packet()
	{
		data.header.sequenceid = 0;
		data.header.ptype = NONGUARANTEED;
		timestamp = af::GetAbsoluteTimeClocks();
		datalength = sizeof(packetdata::packetheader);
	}

	bool packet::pushmsg(void *pdata, unsigned size, PacketType ptype)
	{
		memcpy(((char *)&data.raw) + datalength, &size, sizeof(int));
		datalength += sizeof(int);

		memcpy(((char *)&data.raw) + datalength, pdata, size);
		datalength += size;

		if(ptype == GUARANTEED)
			data.header.ptype = GUARANTEED;

		// For now we assume all our dinky, few messages will fit by the time it gets sent
		return true;
	}

	void packet::renew()
	{
		if(data.header.ptype == GUARANTEED)
			data.header.sequenceid++;
		data.header.ptype = NONGUARANTEED;
		datalength = sizeof(packetdata::packetheader);
	}
}