#pragma once

#include "af_obj.h"
#include "af_axis.h"

namespace af
{
    class Line : public Object
    {
    public:
        Line(Shader *shader);
        ~Line();

        void update(matrix4x4f &vpMatrix, vec3f p1, vec3f p2);
    };
}