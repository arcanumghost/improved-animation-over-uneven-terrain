#include "af_rndbl.h"
#include "af_rndrr.h"
#include <algorithm>

namespace af
{
	Renderable::Renderable(unsigned int _renderMode)
        : m_active(false), m_shader(nullptr), m_initialized(false), m_renderMode(_renderMode)
    {
	}

    // Clean up uniform data
    Renderable::~Renderable()
    {
        //delete [] m_uniformdata;
    }

    // Set Render mode for GL
    void GLRenderable::setRenderMode(RenderMode renderMode)
    {
        switch(renderMode)
        {
        case AF_LINES:
            m_renderMode = GL_LINES;
            break;
        case AF_TRIANGLES:
        default:
            m_renderMode = GL_TRIANGLES;
            break;
        }
    }

    GLRenderable::GLRenderable()
        : m_VBO(0), m_IBO(0), m_iCount(0), m_vCount(0),
          Renderable(GL_TRIANGLES)
	{
		if(!s_shadowShader)
			s_shadowShader = new GLShader("shaders\\shadow.vs",  "shaders\\shadow.ps");
	}

    // initialize a renderable GL object - creates buffers and uniform data
    void GLRenderable::init(float *vBuffer, uint nVertices, uint vertexSize, std::vector<uint> &iBuffer, Shader *shader)
    {
        REQUIRES( !initialized() );

        m_shader = shader;
        m_uniformdata = new void *[m_shader->m_uniforms.size()];

        glGenBuffers( 1, &m_VBO );
        glBindBuffer( GL_ARRAY_BUFFER, m_VBO );
            
        glBufferData( GL_ARRAY_BUFFER, 
                      nVertices*vertexSize, 
                      vBuffer, 
                      GL_STATIC_DRAW );

        glGenBuffers( 1, &m_IBO );
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_IBO );
            
        glBufferData( GL_ELEMENT_ARRAY_BUFFER, 
                      sizeof( int ) *iBuffer.size(), 
                      &(iBuffer.front()), 
                      GL_STATIC_DRAW );

        m_iCount = unsigned(iBuffer.size());
        m_vCount = nVertices;

        m_initialized = m_active = true;
    }

    void GLRenderable::updateVBO(float *vBuffer, uint nVertices, uint vertexSize)
    {
        glBindBuffer( GL_ARRAY_BUFFER, m_VBO );
        glBufferData( GL_ARRAY_BUFFER,
                      nVertices*vertexSize,
                      vBuffer,
                      GL_STATIC_DRAW);
    }
    
    // delete textures (if any) and buffers on GPU
    GLRenderable::~GLRenderable()
    {
        std::for_each(m_textures.begin(), m_textures.end(), [&](Texture *pTexture)
        {
            //delete pTexture;
        });
        m_textures.clear();

        glDeleteBuffers(1, &m_VBO);
        glDeleteBuffers(1, &m_IBO);
        
        delete [] m_uniformdata;
	}

	// RenderElements wrapper
	void GLRenderable::render()
	{
		renderElements(0, m_vCount, 0, m_iCount);
	}

	// RenderElements wrapper
	void GLRenderable::shadowRender()
	{
		// Bind Buffers
		glBindBuffer( GL_ARRAY_BUFFER, m_VBO );
		glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_IBO );

		// Set up Shader
		s_shadowShader->m_stride = m_shader->m_stride;
		((GLShader *)s_shadowShader)->makeActiveShader();

		// Set Uniforms and Textures
		glUniformMatrix4fv( s_shadowShader->m_uniforms[0]->m_uniformid, 1, false, (GLfloat *)m_uniformdata[0]);
		
		/*for(unsigned int u=0, t=0, i=0; u<m_shader->m_uniforms.size(); ++u)
		{
			ShaderUniform *uniform = m_shader->m_uniforms[u];
			switch(uniform->m_type)
			{
			case SHADER_INT1:
				glActiveTexture( GL_TEXTURE0 + t );
				if(int(t) > int(m_textures.size())-1)
					glBindTexture( GL_TEXTURE_2D, m_othertextureids[t-m_textures.size()]);
				else
					glBindTexture( GL_TEXTURE_2D, m_textures[t]->getTextureUint() );
				glUniform1i( uniform->m_uniformid, t++ );
				break;
			case SHADER_VEC3F:
				glUniform3fv( uniform->m_uniformid, 1, (GLfloat *)m_uniformdata[i++]);
				break;
			case SHADER_MAT4:
				glUniformMatrix4fv( uniform->m_uniformid, 1, false, (GLfloat *)m_uniformdata[i++]);
				break;
			case SHADER_FLOAT:
				glUniform1f( uniform->m_uniformid, *((GLfloat *)m_uniformdata[i++]));
				break;
			case SHADER_BONEARRAY:
				glUniformMatrix4fv( uniform->m_uniformid, 100, false, (GLfloat *)m_uniformdata[i++]);
				break;
			}
		}*/

		// Draw
		glDrawRangeElementsBaseVertex( m_renderMode, 0, m_vCount, m_iCount, 
			GL_UNSIGNED_INT, reinterpret_cast< void* >(0), 0);

		// Disable Textures
		/*for(unsigned int i=0; i < m_textures.size(); ++i)
		{
			glActiveTexture( GL_TEXTURE0 + i);
			glBindTexture( GL_TEXTURE_2D, NULL );
		}*/
	}

    // Render this object
    void GLRenderable::renderElements(uint startVertex, uint nVertices, uint startIndex, uint nIndices)
    {
        REQUIRES(initialized());

        // Bind Buffers
        glBindBuffer( GL_ARRAY_BUFFER, m_VBO );
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_IBO );

        // Set up Shader
        ((GLShader *)m_shader)->makeActiveShader();

        // Set Uniforms and Textures
        for(unsigned int u=0, t=0, i=0; u<m_shader->m_uniforms.size(); ++u)
        {
            ShaderUniform *uniform = m_shader->m_uniforms[u];
            switch(uniform->m_type)
            {
            case SHADER_INT1:
                glActiveTexture( GL_TEXTURE0 + t );
				if(int(t) > int(m_textures.size())-1)
					glBindTexture( GL_TEXTURE_2D, m_othertextureids[t-m_textures.size()]);
				else
					glBindTexture( GL_TEXTURE_2D, m_textures[t]->getTextureUint() );
                glUniform1i( uniform->m_uniformid, t++ );
                break;
            case SHADER_VEC3F:
                glUniform3fv( uniform->m_uniformid, 1, (GLfloat *)m_uniformdata[i++]);
                break;
            case SHADER_MAT4:
                glUniformMatrix4fv( uniform->m_uniformid, 1, false, (GLfloat *)m_uniformdata[i++]);
                break;
            case SHADER_FLOAT:
                glUniform1f( uniform->m_uniformid, *((GLfloat *)m_uniformdata[i++]));
                break;
            case SHADER_BONEARRAY:
                glUniformMatrix4fv( uniform->m_uniformid, 100, false, (GLfloat *)m_uniformdata[i++]);
                break;
            }
        }

        // Draw
        glDrawRangeElementsBaseVertex( m_renderMode, startVertex, nVertices+startVertex, nIndices, 
                GL_UNSIGNED_INT, reinterpret_cast< void* >(startIndex*sizeof(uint)), startVertex );

        // Disable Textures
        for(unsigned int i=0; i < m_textures.size(); ++i)
        {
            glActiveTexture( GL_TEXTURE0 + i);
            glBindTexture( GL_TEXTURE_2D, NULL );
        }
    }

    // texture pushback wrapper
    void GLRenderable::addTexture(Texture *texture)
    {
        REQUIRES(initialized());
        m_textures.push_back(texture);
    }

    Shader *Renderable::s_shadowShader = nullptr;

    DXRenderable::DXRenderable(ID3D11Device *device, ID3D11DeviceContext *context)
        : m_VBO(nullptr), m_IBO(nullptr), m_device(device), m_context(context),
        Renderable(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST)
    {
        if(!s_shadowShader)
            s_shadowShader = new DXShader(L"shaders\\shadow.fx", device);
    }

    // release textures and buffers
    DXRenderable::~DXRenderable()
    {
        std::for_each(m_textures.begin(), m_textures.end(), [&](Texture *pTexture)
        {
            delete pTexture;
        });
        m_textures.clear();

        m_IBO->Release();
        m_VBO->Release();

        if(s_shadowShader)
        {
            delete s_shadowShader;
            s_shadowShader = nullptr;
        }
    }

    // Set DX Render mode
    void DXRenderable::setRenderMode(RenderMode renderMode)
    {
        switch(renderMode)
        {
        case AF_LINES:
            m_renderMode = D3D11_PRIMITIVE_TOPOLOGY_LINELIST;
            break;
        case AF_TRIANGLES:
        default:
            m_renderMode = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
            break;
        }
    }

    // Initializes buffers for DX
    void DXRenderable::init(float *vBuffer, uint nVertices, uint vertexSize, std::vector<uint> &iBuffer, Shader *shader)
    {
        REQUIRES( !initialized() );

        m_shader = shader;
        m_uniformdata = new void*[m_shader->m_uniforms.size()];

        // Create and set up Index Buffer
        D3D11_SUBRESOURCE_DATA srd = { &iBuffer[0], 0, 0 };

        m_iCount = unsigned(iBuffer.size());
        m_vCount = nVertices;
    
        HV(m_device->CreateBuffer(&CD3D11_BUFFER_DESC(unsigned(iBuffer.size()*sizeof(unsigned int)),
            D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_IMMUTABLE), &srd, &m_IBO));

        // Create and set up Vertex Buffer
        D3D11_SUBRESOURCE_DATA srd2 = { vBuffer, 0, 0 };

        HV(m_device->CreateBuffer(&CD3D11_BUFFER_DESC(nVertices*vertexSize,
            D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_IMMUTABLE), &srd2, &m_VBO));

        m_initialized = m_active = true;
    }

    void DXRenderable::updateVBO(float *vBuffer, uint nVertices, uint vertexSize)
    {
        // TODO
    }

    // render elements wrapper for DX
    void DXRenderable::render()
    {
        renderElements(0, m_vCount, 0, m_iCount);
    }

    // renders elements of a DX object
    void DXRenderable::renderElements(uint startVertex, uint nVertices, uint startIndex, uint nIndices)
    {
        // Bind Buffers
        uint stride = m_shader->stride();
        uint offset = 0;
        m_context->IASetVertexBuffers(0, 1, &m_VBO, &stride, &offset);
        m_context->IASetIndexBuffer(m_IBO, DXGI_FORMAT_R32_UINT, 0);
        m_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY(m_renderMode));
        
        // Set Uniforms and Textures
        for(unsigned int u=0, t=0, i=0; u<m_shader->m_uniforms.size(); ++u)
        {
            ShaderUniform *uniform = m_shader->m_uniforms[u];
            ID3DX11EffectVariable *effectvar = ((DXShaderUniform *)uniform)->m_effectvariable;
            Texture *texture;
            switch(uniform->m_type)
            {
            case SHADER_INT1:
                texture = m_textures[t++];
                effectvar->AsShaderResource()->SetResource(((DXTexture *)texture)->getShaderResource());
                break;
            case SHADER_VEC3F:
                effectvar->AsVector()->SetFloatVector((float *)m_uniformdata[i++]);
                break;
            case SHADER_MAT4:
                effectvar->AsMatrix()->SetMatrix((float *)m_uniformdata[i++]);
                break;
            case SHADER_FLOAT:
                effectvar->AsScalar()->SetFloat(*(float *)m_uniformdata[i++]);
                break;
            }
        }

        // Set up Shader
        ((DXShader *)m_shader)->makeActiveShader(m_context);
        
        // Draw
        m_context->DrawIndexed(nIndices, startIndex, startVertex);

        // Disable Shader?
        ((DXShader *)m_shader)->deactivateShader(m_context);
    }

    // add texture wrapper for DX
    void DXRenderable::addTexture(Texture *texture)
    {
        REQUIRES(initialized());
        m_textures.push_back(texture);
    }
}