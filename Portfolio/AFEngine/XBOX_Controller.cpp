#include "targetver.h"
#include "XBOX_Controller.h"

namespace af
{
	XBOX_Controller::XBOX_Controller( int playerNumber )
	{
		assert( playerNumber >= 0 && playerNumber < 4 );

		// Sets the Controller Number
		m_controllerNum = playerNumber;

		for( int i = 0; i < MAX_BUTTONS; ++i )
		{
			m_buttonStates[ i ] = false;
			m_lastButtonStates[ i ] = false;
		}
		for( int i = 0; i < MAX_ALT_BUTTON; ++i )
		{
			m_altButtonStates[ i ] = false;
			m_lastAltButtonStates[ i ] = false;
		}

	}

	XINPUT_STATE XBOX_Controller::GetState()
	{
		// Zeroise the state
		ZeroMemory( &m_controllerState, sizeof( XINPUT_STATE ) );

		// Get the state
		XInputGetState( m_controllerNum, &m_controllerState );

		return m_controllerState;
	}

	bool XBOX_Controller::IsConnected()
	{
		// Zeroise the state
		ZeroMemory( &m_controllerState, sizeof( XINPUT_STATE ) );

		DWORD result = XInputGetState( m_controllerNum, &m_controllerState );

		return result == ERROR_SUCCESS ? true : false;
	}

	void XBOX_Controller::Vibrate( int leftVal /* = 0 */, int rightVal /* = 0 */ )
	{
		// Create a Vibration State
		XINPUT_VIBRATION vibration;

		if( leftVal != 0 || rightVal != 0 )
			m_timeVibrate = 0;

		// Zeroise the Vibration
		ZeroMemory( &vibration, sizeof( XINPUT_VIBRATION ) );

		// Set the Vibration Values
		vibration.wLeftMotorSpeed = leftVal;
		vibration.wRightMotorSpeed = rightVal;

		// Vibrate the controller
		XInputSetState( m_controllerNum, &vibration );
	}

	bool XBOX_Controller::IsDown( unsigned short button )
	{
		if( IsConnected() )
		{
			int arButton = GetButton( button );
			assert( arButton != -1 );

			return m_buttonStates[ arButton ];
		}
		else
			return false;
	}

	bool XBOX_Controller::DidGoDown( unsigned short button )
	{
		if( IsConnected() )
		{
			int arButton = GetButton( button );
			assert( arButton != -1 );

			return m_buttonStates[ arButton ] && !m_lastButtonStates[ arButton ];
		}
		else
			return false;
	}

	bool XBOX_Controller::DidGoUp( unsigned short button )
	{
		if( IsConnected() )
		{
			int arButton = GetButton( button );
			assert( arButton != -1 );

			return !m_buttonStates[ arButton ] && m_lastButtonStates[ arButton ];
		}
		else
			return false;
	}

	bool XBOX_Controller::AltIsDown( unsigned short dir )
	{
		if( IsConnected() )
			return m_altButtonStates[ dir ];
		else
			return false;
	}

	bool XBOX_Controller::AltGoDown( unsigned short dir )
	{
		if( IsConnected() )
			return m_altButtonStates[ dir ] && !m_lastAltButtonStates[ dir ];
		else
			return false;
	}

	bool XBOX_Controller::AltGoUp( unsigned short dir )
	{
		if( IsConnected() )
			return !m_altButtonStates[ dir ] && m_lastAltButtonStates[ dir ];
		else
			return false;
	}

	vec2f XBOX_Controller::GetLJoyNormal()
	{
		float length = sqrt( (float)Lx * Lx + (float)Ly * Ly );

		if( length == 0 )
			return vec2f();
		else
			return vec2f( (float)Lx / length, (float)Ly / length );
	}

	vec2f XBOX_Controller::GetRJoyNormal()
	{
		float length = sqrt( (float)Rx * Rx + (float)Ry * Ry );

		if( length == 0 )
			return vec2f();
		else
			return vec2f( (float)Rx / length, (float)Ry / length );
	}

    vec2f XBOX_Controller::GetLJoyCoordinates()
    {
        vec2f result;

        if(std::abs(Lx) >= 10000 || std::abs(Ly) >= 10000)
        {
            result.x = float(-Lx) / SHRT_MIN;
            result.y = float(-Ly) / SHRT_MIN;
        }

        return result;
    }

    vec2f XBOX_Controller::GetRJoyCoordinates()
    {
        vec2f result;

        if(std::abs(Rx) >= 10000 || std::abs(Ry) >= 10000)
        {
            result.x = float(-Rx) / SHRT_MIN;
            result.y = float(-Ry) / SHRT_MIN;
        }

        return result;
    }

	float XBOX_Controller::GetLJoyAngle()
	{
		vec2f normalized = GetLJoyNormal();

		return ( 180 / 3.14159f ) * atan2( -normalized.y, normalized.x );
	}

	float XBOX_Controller::GetRJoyAngle()
	{
		vec2f normalized = GetRJoyNormal();

		return ( 180 / 3.14159f ) * atan2( -normalized.y, normalized.x );
	}

	void XBOX_Controller::Update()
	{
		XINPUT_STATE currentState;

		currentState = GetState();

		Lx = currentState.Gamepad.sThumbLX;
		Ly = currentState.Gamepad.sThumbLY;

        Rx = currentState.Gamepad.sThumbRX;
        Ry = currentState.Gamepad.sThumbRY;

        for( int i = 0; i < MAX_BUTTONS; ++i )
		{
			m_lastButtonStates[ i ] = m_buttonStates[ i ];
			m_buttonStates[ i ] = currentState.Gamepad.wButtons & GetRealButton( i ) ? true : false;
		}

// 		if( currentState.Gamepad.sThumbLX < 5000 && 
// 			currentState.Gamepad.sThumbLX > -5000 )
// 		{
// 			
// 		}
// 		if( currentState.Gamepad.sThumbLY < 5000 && 
// 			currentState.Gamepad.sThumbLY > -5000 )
// 		{
// 			
// 		}

// 		Lx = 0;
// 		Ly = 0;

		for( int i = 0; i < MAX_ALT_BUTTON; ++i )
		{
			m_lastAltButtonStates[ i ] = m_altButtonStates[ i ];
		}

		if( currentState.Gamepad.sThumbLX < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE && 
			currentState.Gamepad.sThumbLX > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE )
		{
			m_altButtonStates[ LSTICK_XPOS ] = false;
			m_altButtonStates[ LSTICK_XNEG ] = false;
		}
		else
		{
			if( currentState.Gamepad.sThumbLX >= XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE )
			{
				m_altButtonStates[ LSTICK_XPOS ] = true;
				m_altButtonStates[ LSTICK_XNEG ] = false;
			}
			else
			{
				m_altButtonStates[ LSTICK_XPOS ] = false;
				m_altButtonStates[ LSTICK_XNEG ] = true;
			}
		}

		if( currentState.Gamepad.sThumbLY < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE && 
			currentState.Gamepad.sThumbLY > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE )
		{
			m_altButtonStates[ LSTICK_YPOS ] = false;
			m_altButtonStates[ LSTICK_YNEG ] = false;
		}
		else
		{
			if( currentState.Gamepad.sThumbLY >= XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE )
			{
				m_altButtonStates[ LSTICK_YPOS ] = true;
				m_altButtonStates[ LSTICK_YNEG ] = false;
			}
			else
			{
				m_altButtonStates[ LSTICK_YPOS ] = false;
				m_altButtonStates[ LSTICK_YNEG ] = true;
			}
		}

		if( currentState.Gamepad.bLeftTrigger < XINPUT_GAMEPAD_TRIGGER_THRESHOLD )
			m_altButtonStates[ LEFT_TRIGGER ] = false;
		else
			m_altButtonStates[ LEFT_TRIGGER ] = true;

		if( currentState.Gamepad.bRightTrigger < XINPUT_GAMEPAD_TRIGGER_THRESHOLD )
			m_altButtonStates[ RIGHT_TRIGGER ] = false;
		else
			m_altButtonStates[ RIGHT_TRIGGER ] = true;

		if( m_timeVibrate >= FRAMES_TO_VIBRATE )
		{
			Vibrate( 0, 0 );
		}
		else
			++m_timeVibrate;
	}

	int XBOX_Controller::GetButton( unsigned short button )
	{
		switch( button )
		{
		case XINPUT_GAMEPAD_DPAD_UP:
			return 0;
		case XINPUT_GAMEPAD_DPAD_DOWN:
			return 1;
		case XINPUT_GAMEPAD_DPAD_LEFT:
			return 2;
		case XINPUT_GAMEPAD_DPAD_RIGHT:
			return 3;
		case XINPUT_GAMEPAD_START:
			return 4;
		case XINPUT_GAMEPAD_BACK:
			return 5;
		case XINPUT_GAMEPAD_LEFT_THUMB:
			return 6;
		case XINPUT_GAMEPAD_RIGHT_THUMB:
			return 7;
		case XINPUT_GAMEPAD_LEFT_SHOULDER:
			return 8;
		case XINPUT_GAMEPAD_RIGHT_SHOULDER:
			return 9;
		case XINPUT_GAMEPAD_A:
			return 10;
		case XINPUT_GAMEPAD_B:
			return 11;
		case XINPUT_GAMEPAD_X:
			return 12;
		case XINPUT_GAMEPAD_Y:
			return 13;
		default:
			return -1;
		}
	}

	unsigned short XBOX_Controller::GetRealButton( unsigned short button )
	{
		switch( button )
		{
		case 0:
			return XINPUT_GAMEPAD_DPAD_UP;
		case 1:
			return XINPUT_GAMEPAD_DPAD_DOWN;
		case 2:
			return XINPUT_GAMEPAD_DPAD_LEFT;
		case 3:
			return XINPUT_GAMEPAD_DPAD_RIGHT;
		case 4:
			return XINPUT_GAMEPAD_START;
		case 5:
			return XINPUT_GAMEPAD_BACK;
		case 6:
			return XINPUT_GAMEPAD_LEFT_THUMB;
		case 7:
			return XINPUT_GAMEPAD_RIGHT_THUMB;
		case 8:
			return XINPUT_GAMEPAD_LEFT_SHOULDER;
		case 9:
			return XINPUT_GAMEPAD_RIGHT_SHOULDER;
		case 10:
			return XINPUT_GAMEPAD_A;
		case 11:
			return XINPUT_GAMEPAD_B;
		case 12:
			return XINPUT_GAMEPAD_X;
		case 13:
			return XINPUT_GAMEPAD_Y;
		default:
			return 0xFFFF;
		}
	}
}