#include "af_obj.h"
#include "af_rndrr.h"

namespace af
{
    // Object base doesn't do much on its own, but creates a renderable and initializes mvpMatrix
    Object::Object(char *name)
        : m_mvpMatrix(IDENTITY), m_active(true)
    {
		if(name)
			m_name = name;
        Renderer::getInstance().addObject(this);
    }

    void Object::createRenderable(float *vBuffer, uint nVertices, uint vertexSize, std::vector<uint> &iBuffer, Shader *shader)
    {
        m_renderables.push_back(Renderer::getInstance().createRenderable(vBuffer, nVertices, vertexSize, iBuffer, shader));
    }

    void Object::updateVertices(float *vBuffer, uint nVertices, uint vertexSize, uint index)
    {
        m_renderables[index]->updateVBO(vBuffer, nVertices, vertexSize);
    }

    void Object::createShadowRenderable(float *vBuffer, uint nVertices, uint vertexSize, std::vector<uint> &iBuffer, Shader *shader)
    {
        m_shadowRenderables.push_back(Renderer::getInstance().createRenderable(vBuffer, nVertices, vertexSize, iBuffer, shader));
        m_shadowRenderables[0]->deactivate();
	}
	
	void Object::shadowRender()
	{
		std::for_each(m_renderables.begin(), m_renderables.end(), [&](Renderable *pRenderable)
		{
			if(pRenderable->active())
				pRenderable->shadowRender();
		});
	}

    void Object::render()
    {
		//printf("Render %s\n", m_name.c_str());
		std::for_each(m_renderables.begin(), m_renderables.end(), [&](Renderable *pRenderable)
        {
            if(pRenderable->active())
                pRenderable->render();
        });
    }

    Object::~Object()
    {
        for(unsigned i=0; i<m_renderables.size(); ++i)
            delete m_renderables[i];
    }
}