#ifndef _AF_FRUSTUM_H
#define _AF_FRUSTUM_H

#include "af_cam.h"

namespace af
{
    // Plane used for intersection tests
    struct Plane
    {
        vec3f normal;
        float distance;

        // Set using 3 points
        void set(vec3f& p1, vec3f& p2, vec3f& p3)
        {
            vec3f aux1, aux2;
            aux1 = p1-p2;
            aux2 = p3-p2;
            normal = crossproduct(aux1, aux2);
            normalize(normal);
            distance = -(dotproduct(normal, p2));
        }
    };

    // Frustum class
    class Frustum
    {
    private:
        float angle, ratio, zNear, zFar, tang;
        float nWidth, nHeight, fWidth, fHeight;
        vec3f nbl, nbr, ntl, ntr, fbl, fbr, ftl, ftr;
        Plane m_planes[6];
        matrix4x4f m_projMatrix, m_lightprojMatrix;

    public:
        void init(float fovy, float aspect, float zNear, float zFar);
        // void initlight(float fovy, float aspect, float zNear, float zFar);
        void update(Camera&);
        bool cullGeometry(vec3f&, float);
        matrix4x4f& getProjectionMatrix( );
        matrix4x4f& getLightProjectionMatrix( );

    };
}

#endif