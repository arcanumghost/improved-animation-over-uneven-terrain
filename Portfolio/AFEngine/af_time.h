#ifndef _AF_TIME_H_
#define _AF_TIME_H_

namespace af
{
    typedef __int64 SystemClocks;

    double GetAbsoluteTimeSeconds();
    SystemClocks GetAbsoluteTimeClocks();
    SystemClocks GetClockFrequency();
    double GetClockFrequencyDouble();
    double ClocksToSeconds( SystemClocks clocks );
    SystemClocks SecondsToClocks( double seconds );
}

#endif