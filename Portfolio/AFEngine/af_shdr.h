#ifndef _AF_SHADER_H_
#define _AF_SHADER_H_

#include <vector>

#include "af_gfx.h"
#include "af_vec.h"

namespace af
{
    // Position, texture coordinate, and normals, all in a row to be referenced as a VBO
    /*struct RenderableVertex
    {
        vec3f position;
        vec3f color;
        vec2f texCoord;
        vec2f lightmapTexCoord;
        vec3f normal;
        float tangent[4];
    };*/

    // Used for Shader Attribute Indices
    enum ShaderAttributeType
    {
        SHADER_POSITION,         
        SHADER_COLOR,            
        SHADER_TEXCOORD,         
        SHADER_LIGHTTEX_COORD,          
        SHADER_NORMAL,    
        SHADER_TANGENT,
        SHADER_WEIGHTS,
        SHADER_BONES,
        SHADER_ATTRIBUTE_LENGTH
    };

    // base shader attribute, mostly empty
    struct ShaderAttribute
    {
        virtual void enable(int stride) = 0;
        virtual void disable() = 0;
    };

    // OpenGL shader attribute variant
    struct GLShaderAttribute : public ShaderAttribute
    {
        GLuint m_index;
        GLuint m_size;
        GLenum m_type;
        GLboolean m_normalized;
        GLvoid *m_pointer;

        GLShaderAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLvoid *pointer);
        void enable(int stride);
        void disable();
    };

    // DirectX shader attribute variant
    struct DXShaderAttribute : public ShaderAttribute
    {
        D3D11_INPUT_ELEMENT_DESC m_desc;

        DXShaderAttribute( D3D11_INPUT_ELEMENT_DESC &_desc );
        void enable(int stride);
        void disable();
    };

    // Used to determine type of Shader Uniform functions
    enum ShaderUniformType
    {
        SHADER_INT1,
        SHADER_VEC3F,
        SHADER_MAT4,
        SHADER_FLOAT,
        SHADER_BONEARRAY,
        SHADER_UNIFORM_LENGTH
    };

    // base shader uniform
    struct ShaderUniform
    {
        std::string m_nameString;
        ShaderUniformType m_type;
        unsigned int m_uniformid;
        ShaderUniform( std::string nameString, ShaderUniformType type);
    };

    // OpenGL variant of the shader uniform
    struct GLShaderUniform : public ShaderUniform
    {
        GLShaderUniform( std::string nameString, ShaderUniformType type);
        void setup( GLint shaderID );
    };

    // DirectX variant of the shader uniform
    struct DXShaderUniform : public ShaderUniform
    {
        ID3DX11EffectVariable *m_effectvariable;

        DXShaderUniform( std::string nameString, ShaderUniformType type);
        void setup( ID3DX11Effect *effect );
    };

    // Shader base - mostly keeps track of attributes and uniforms
    struct Shader
    {
        unsigned int m_stride;

        std::vector<ShaderAttribute *> m_attributes;
        std::vector<ShaderUniform *> m_uniforms;
        unsigned int stride()   { return m_stride; }
        virtual ~Shader();
    };

    // OpenGL variant of the Shader
    struct GLShader : public Shader
    {
        // Member variables
        GLint m_program;
        GLint m_vshader;
        GLint m_pshader;

        int LoadShader( const char *filename );

        // Loads and compiles shaders
        GLShader(const char *vsource, const char *psource);
        ~GLShader();

        // Sets the current shader to this shader, sets uniforms and binds VertexAttribArrays
        void makeActiveShader();

        GLint getProgram() { return m_program; }
    };

    // OpenGL variant of the Shader
    struct DXShader : public Shader
    {
        ID3D10Blob *m_vsCode, *m_psCode;
        ID3D11VertexShader *m_vShader;
        ID3D11PixelShader *m_pShader;
        ID3D11InputLayout *m_inputLayout;

        ID3DX11Effect *m_effect;
        ID3DX11EffectTechnique *m_technique;
        ID3DX11EffectPass *m_pass;
        D3DX11_PASS_DESC m_passDesc;

    public:
        // Loads and compiles shader
        DXShader(const wchar_t *fsource, ID3D11Device *device);
        ~DXShader();

        void makeActiveShader(ID3D11DeviceContext *m_context);
        void deactivateShader(ID3D11DeviceContext *m_context);
        ID3D11InputLayout *getInputLayout() { return m_inputLayout; }
    };
}

#endif