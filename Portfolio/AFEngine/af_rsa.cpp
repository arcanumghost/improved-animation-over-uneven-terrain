#include "af_rsa.h"

#include <ctype.h>
#include <fstream>
#include <iostream>

namespace af
{
    char *RuneStrings[] =
    {
        "EOF",
        "Identifier",
        "Assign",
        "String",
        "Number",
        "Vec Begin",
        "Vec End",
        "+",
        "-",
        "*",
        "/",
        "==",
        "!=",
        "<",
        ">",
        "&&",
        "||",
        "!",
        "{",
        "}",
        "(",
        ")",
        ";",
        ",",
        "if",
        "else",
        "return",
        "while",
        "error"
    };

    RuneAnalyzer::RuneAnalyzer(char *filename)
        : m_token(TOKEN_END_OF_FILE), m_line(1), c(0), current(nullptr), code(nullptr), m_error(false)
    {
        // Open source file
        fstream file( filename, std::ios::in | std::ios::binary | std::ios::ate );
        if(file.fail())
        {
            printf("File %s does not exist\n", filename);
            return;
        }

        int length = (int)file.tellg();
        file.seekg(0);

        code = new unsigned char[length + 1];
        file.read((char*)code, length);
        code[length] = 0;

        file.close();

        current = code;
    }

    void RuneAnalyzer::next()
    {
        m_strdata.clear();
            
        try{ for(;;) switch(c = *current++)
        {
        case 0:     m_token = TOKEN_END_OF_FILE; current--; return;
        case '+':   m_token = TOKEN_ADD; return;
        case '-':   m_token = TOKEN_SUB; return;
        case '*':   m_token = TOKEN_MUL; return;
        case '/':   if(*current=='*') 
                    { 
                        current++; 
                        while(*current!='*' || current[1]!='/') 
                        { 
                            if(*current == '\n')
                                ++m_line;
                            if(*current == 0 || current[1]==0)
                            {
                                m_token = TOKEN_END_OF_FILE;
                                throw LexicalError(m_line, "Unexpected EOF in comment");
                            }
                            current++; 
                        }
                        current+=2;
                        break; 
                    }
                    else if(*current == '/')
                    {
                        while(*current!='\n')
                        {
                            if(*current == 0)
                            {
                                m_token = TOKEN_END_OF_FILE;
                                throw LexicalError(m_line, "Unexpected EOF in comment");
                            }
                            current++;
                        }
                        break;
                    }
                    m_token = TOKEN_DIV; return;
        case '\n':  ++m_line; break;
        case '\"':  while((c = *current++) != '\"')
                    {
                        if(c == 0)
                            throw LexicalError(m_line, "Unexpected EOF in string");
                        if(c == '\\')
                            switch(c = *current++)
                            {
                            case '\"':  m_strdata += c;     break;
                            case 'n':   m_strdata += '\n';  break;
                            case '\\':
                            default:    m_strdata += c;     break;
                            }
                        else
                            m_strdata += c;
                    }
                    m_token = TOKEN_STRING;       return;
        case '(':   m_token = TOKEN_GROUP_BEGIN;  return;
        case ')':   m_token = TOKEN_GROUP_END;    return;
        case '{':   m_token = TOKEN_BLOCK_BEGIN;  return;
        case '}':   m_token = TOKEN_BLOCK_END;    return;
        case '[':   m_token = TOKEN_VEC_BEGIN;    return;
        case ']':   m_token = TOKEN_VEC_END;      return;
        case '<':   m_token = TOKEN_LT;           return;
        case '>':   m_token = TOKEN_GT;           return;
        case '|':   if(*current=='|') { current++; m_token = TOKEN_OR;  return; } 
                    throw LexicalError(m_line, "Incomplete OR");
        case '&':   if(*current=='&') { current++; m_token = TOKEN_AND; return; } 
                    throw LexicalError(m_line, "Incomplete AND");
        case ';':   m_token = TOKEN_SEMICOLON;    return;
        case '!':   if(*current=='=') { current++; m_token = TOKEN_NEQ; return; }
                    else              { m_token = TOKEN_NOT;    return; }
        case ',':   m_token = TOKEN_SEPARATOR;    return;
        case '=':   if(*current=='=') { current++; m_token = TOKEN_EQ;  return; }
                    else              { m_token = TOKEN_ASSIGN; return; }
        default:
            if(isalpha(c) || c == 0x5F)
            {
                // Build the string
                while(isalpha(c) || isdigit(c) || c == 0x5F )
                {
                    m_strdata += c;
                    c = *current++;
                }
                current--;

                // Evaluate out keywords
                if(!m_strdata.compare("if"))      { m_token = TOKEN_IF;     return; }
                if(!m_strdata.compare("else"))    { m_token = TOKEN_ELSE;   return; }
                if(!m_strdata.compare("return"))  { m_token = TOKEN_RETURN; return; }
                if(!m_strdata.compare("while"))   { m_token = TOKEN_WHILE;  return; }
                if(!m_strdata.compare("main"))    { m_token = TOKEN_MAIN;   return; }
                m_token = TOKEN_IDENTIFIER;       return;
            }
            else if(isdigit(c))
            {
                bool usedperiod = false;
                while(isdigit(c) || (c == 0x2E && !usedperiod))
                {
                    m_strdata += c;
                    if(c == 0x2E)
                        usedperiod = true;
                    c = *current++;
                }
                current--;

                m_numdata = (float)atof(m_strdata.c_str());
                m_token = TOKEN_NUMBER;           
                return;
            }
            else if(iswspace(c))
                break;
            std::string msg("Disallowed character '%'");
            msg[22] = c;
            throw LexicalError(m_line, msg.c_str());
        }}
        catch(LexicalError le)
        {
            le.display();
            m_error = true;
            current++;
        };
    }

    // We'll call this legacy code.
    // Used to print the tokens in the first stage of this project.
    /*void RuneAnalyzer::printCurrent()
    {
        if(m_token == TOKEN_NUMBER)
            g_ss << m_line << RuneStrings[m_token] << m_numdata << endl;
        else
            g_ss << m_line << RuneStrings[m_token] << m_numdata << m_strdata.c_str() << endl;
    }*/
}