#include "guicon.h"

namespace af
{
	HANDLE guicon::inConsoleHandle;
	HANDLE guicon::outConsoleHandle;
	WORD guicon::currentColor;
	void (*guicon::inputcallback)(const char *);
	string guicon::inputString;
	map<string, WORD> guicon::colors;
}