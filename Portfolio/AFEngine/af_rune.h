#ifndef _AF_RUNE_SCRIPT_H_
#define _AF_RUNE_SCRIPT_H_

#include "af_rsa.h"

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

namespace af
{
    void tabs(int level);

    enum ValueType
    {
        V_ERROR,
        V_NUM,
        V_FPTR,
        V_ADDR,
        V_STR,
        V_VEC
    };

    struct NativeFDef
    {
        unsigned m_nargs;
        int m_id;
        ValueType m_vtypes[5];
        void *m_fptr;

        // For lookup by compiler
        static map<string, NativeFDef> s_deftable;
        // For lookup by interpreter
        static vector<NativeFDef> s_deflist;
        
        // Registers a function with the two above functions
        static void registerfunction(void *fptr, char *name, char *args);
    };

    // Bytecode commands
    // Begin at 'A' so commands are easily found in bytecode
    enum ByteCode
    {                   // additional information in the bytecode instruction
        BC_ERR,
        BC_JUMP = 0x41, // absolute address
        BC_JF,          // absolute address
        BC_ASS,         // fptr offset of source
        BC_IASS,        // fptr offset of source, index on stack
        // VAR / IVAR must immediately follow ASS / IASS
        BC_PUSH_VAR,    // fptr offset of source
        BC_PUSH_IVAR,   // fptr offset of source, index on stack
        BC_PUSH_VAL,    // 4-byte float value
        BC_PUSH_STR,    // followed by a null-terminated string
        BC_PUSH_VEC,    // numelements
        BC_ADD,
        BC_SUB,
        BC_MUL,
        BC_DIV,
        BC_EQ,          // N
        BC_NEQ,
        BC_LT,
        BC_GT,
        BC_AND,
        BC_OR,          // S
        BC_NEG,
        BC_NOT,
        BC_POP,         
        BC_CALL,        // absolute address (determined at link), numlocals
        BC_NCAL,        // index of the native function
        BC_RET,         // numelements_to_pop
        BC_ABRT         // Z
    };

    enum RuneType
    {
        RT_VAR,
        RT_IVAR,
        RT_STR,
        RT_NUM,
        RT_VEC,
        RT_FUNC,
        RT_IF,
        RT_WHLE,
        RT_RTRN,
        RT_BLCK,
        RT_FDEF,
        RT_BNRY,
        RT_UNRY,
        RT_POP
    };
    
    // base node
    struct runenode
    {
        RuneType m_rtype;
        runenode(RuneType rtype) : m_rtype(rtype) {}
        virtual void print(int level) = 0;
    };

    // Factors are the most complex level of the syntax, there are many variants
    struct varnode : runenode
    {
        string m_name;
        varnode(string name) : m_name(name), runenode(RT_VAR) {}
        void print(int level);
    };
    struct ivarnode : runenode
    {
        string m_name;
        runenode *m_index;
        ivarnode(string name, runenode *index) : m_name(name), m_index(index), runenode(RT_IVAR) {}
        ~ivarnode() { delete m_index; }
        void print(int level);
    };
    struct strnode : runenode
    {
        string m_data;
        strnode(string data) : m_data(data), runenode(RT_STR) {}
        void print(int level);
    };
    struct numnode : runenode
    {
        float m_data;
        numnode(float data) : m_data(data), runenode(RT_NUM) {}
        void print(int level);
    };
    struct vecnode : runenode
    {
        vector<runenode *> m_elements;
        vecnode() : runenode(RT_VEC) {}
        ~vecnode() { for(unsigned i=0; i<m_elements.size(); ++i) delete m_elements[i]; }
        void print(int level);
    };
    struct funcnode : runenode
    {
        string m_name;
        vector<runenode *> m_args;
        funcnode(string name) : m_name(name), runenode(RT_FUNC) {}
        ~funcnode() { for(unsigned i=0; i<m_args.size(); ++i) delete m_args[i]; }
        void print(int level);
    };

    struct ifnode : runenode
    {
        runenode *m_cond;
        runenode *m_if;
        runenode *m_else;
        ifnode(runenode *cond, runenode *ifnode, runenode *elsenode = nullptr)
            : m_cond(cond), m_if(ifnode), m_else(elsenode), runenode(RT_IF)
        {}
        ~ifnode() { delete m_cond; delete m_if; if(m_else) delete m_else; }
        void print(int level);
    };

    struct whlenode : runenode
    {
        runenode *m_cond;
        runenode *m_loop;
        whlenode(runenode *cond) : m_cond(cond), runenode(RT_WHLE) {}
        ~whlenode() { delete m_cond; delete m_loop; }
        void print(int level);
    };

    struct rtrnnode : runenode
    {
        runenode *m_expr;
        rtrnnode(runenode *expr) : m_expr(expr), runenode(RT_RTRN) {}
        ~rtrnnode() { delete m_expr; }
        void print(int level);
    };

    struct blcknode : runenode
    {
        vector<runenode *> m_stmts;
        blcknode() : runenode(RT_BLCK) {}
        ~blcknode() { for(unsigned i=0; i<m_stmts.size(); ++i) delete m_stmts[i]; }
        void print(int level);
    };

    // function definition
    struct fdefnode : runenode
    {
        string m_name;
        blcknode *m_body;
        vector<varnode *> m_args;

        // variables used for linking functions
        unsigned int m_address;
        vector<unsigned> m_references;
        // used for compiling variable references
        map<string, int> m_vars; 

        fdefnode(string name) : m_name(name), m_body(nullptr), runenode(RT_FDEF) {}
        ~fdefnode() { delete m_body; for(unsigned i=0; i<m_args.size(); ++i) delete m_args[i]; }
        void print(int level = 0);
        //void genCode(vector<char> &code);
    };

    struct popnode : runenode
    {
        runenode *m_expr;
        popnode(runenode *expr) : m_expr(expr), runenode(RT_POP) {}
        ~popnode() { delete m_expr; }
        void print(int level);
    };

    // Operators in expressions
    enum BinaryType
    {
        BINARY_ADD,     BINARY_SUB,     BINARY_MUL,     BINARY_DIV,
        BINARY_EQ,      BINARY_NEQ,     BINARY_LT,      BINARY_GT,
        BINARY_AND,     BINARY_OR,      BINARY_ASSIGN,
        BINARY_ERROR
    };

    struct bnrynode : runenode
    {
        BinaryType m_btype;
        runenode *m_left;
        runenode *m_right;
        bnrynode(BinaryType btype, runenode *left, runenode *right)
            : m_btype(btype), m_left(left), m_right(right), runenode(RT_BNRY)
        {}
        ~bnrynode() { delete m_left; delete m_right; }
        void print(int level);
    };

    enum UnaryType
    {
        UNARY_NEG, UNARY_NOT, UNARY_ERROR
    };

    struct unrynode : runenode
    {
        UnaryType m_utype;
        runenode *m_right;
        unrynode(UnaryType utype, runenode *right)
            : m_utype(utype), m_right(right), runenode(RT_UNRY)
        {}
        ~unrynode() { delete m_right; }
        void print(int level);
    };

    class ParserError
    {
        unsigned int line;
        std::string msg;
    public:
        ParserError(unsigned int l, const char *s) : line(l), msg(s) {}
        void display();
    };

    class CompilerError
    {
        std::string msg;
    public:
        CompilerError(const char *s) : msg(s) {}
        void display();
    };

    enum ExpressionType
    {
        EXPR, BOOLEXPR, COMPEXPR, PLUSEXPR, MULTEXPR, FCTR, EXPR_ERROR
    };

    class RuneParser
    {
        RuneAnalyzer m_analyzer;
        map<string, fdefnode *> m_functions;
        fdefnode *m_currentFunction;
        bool m_parsed;

        // Progresses past expected, unimportant tokens
        void expect(RuneToken t);

        // Functions that parse various kinds of expression
        void parseFdef();  // function definitions
        runenode *parseStmt();  // if, blck, whle, rtrn
        runenode *parseExpr(ExpressionType etype = EXPR);  // top level of expressions

    public:
        RuneParser(char *filename);
        ~RuneParser();
        void close();
        bool parse();
        void print();
        //void output();
        map<string, fdefnode *> *getfunctions() { return &m_functions; }
    };

    class RuneCodeGenerator
    {
        fdefnode *m_main;
        map<string, fdefnode *> *m_functions;
        fdefnode *m_currentFunction;
        vector<char> m_bytecode;
        bool m_compiled;

        // Helper functions for compilation
        void linkCode();
        void replaceaddr(unsigned location, unsigned newaddr);
        void emit(ByteCode bc);
        void emit(float bcf);
        void emit(int bci);
        void emit(const char *bcstr);

    public:
        RuneCodeGenerator(map<string, fdefnode *> *functions) : m_functions(functions), m_compiled(false) {}
        ~RuneCodeGenerator()
        {
            /*for_each(m_functions->begin(), m_functions->end(), [&](std::pair<string, fdefnode *> fdefpair)
            {
                delete fdefpair.second;
            });*/ 
            //delete m_main;        
        };
        // Functions available to the runescript compiler user
        bool genCode(runenode *node = nullptr);
        void printCode();
        vector<char> *getCode() { return &m_bytecode; }
    };

    class RuntimeError
    {
        unsigned ip;
        std::string msg;
    public:
        RuntimeError(unsigned _ip, const char *s) : ip(_ip), msg(s) {}
        void display();
    };

#define INC(val)\
    if(val.m_type>V_ADDR)\
    {\
        val.m_refable->m_refcount++;\
    }
#define DEC(val)\
    if(val.m_type>V_ADDR)\
    {\
        val.m_refable->m_refcount--;\
        if(!val.m_refable->m_refcount)\
        {\
            switch(val.m_type)\
            {\
            case V_STR:\
                delete val.m_strval;\
                RefCountable::s_strdeallocs++;\
                break;\
            case V_VEC:\
                delete val.m_vecval;\
                RefCountable::s_vecdeallocs++;\
                break;\
            }\
        }\
    }

    struct RefCountable
    {
        unsigned m_refcount;
        RefCountable() : m_refcount(0) {}

        static unsigned s_vecallocs;
        static unsigned s_vecdeallocs;
        static unsigned s_strallocs;
        static unsigned s_strdeallocs;
        static vector<RefCountable *> s_refcountables;
    };

    struct VecVal;
    struct StrVal;

    struct value
    {
        ValueType m_type;
        union
        {
            float m_numval;
            RefCountable *m_refable;
            VecVal *m_vecval;
            StrVal *m_strval;
            int m_fptr;
            char *m_addr;
        };
    };

#define nullvaluereturn value nullvaluev;\
    nullvaluev.m_type = V_ERROR;\
    nullvaluev.m_fptr = 0x0badf00d;\
    return nullvaluev;

    

    struct VecVal : public RefCountable
    {
        vector<value> v;
        ~VecVal();
    };
   
    struct StrVal : public RefCountable
    {
        string v;
    };

    // Rune / runtime pun.
    class RuneRuntime
    {
        char *m_code;
        char *m_ip;

        vector<value> m_stack;
        int m_fp;

        // returns a stack value from the top of the stack, 1-indexed.
        value &stack(int n) { return m_stack[m_stack.size()-n]; }
        void pop()          { m_stack.pop_back(); }
        void push(value &v) { m_stack.push_back(v); }
        value &getlocal()   { return m_stack[getiarg()+m_fp]; }
        int getiarg();
        float getfarg();
        string getsarg();
    public:
        RuneRuntime(vector<char> *code) : m_code(&(*code)[0]), m_ip(m_code), m_fp(0) {}
        ~RuneRuntime() { }//delete [] m_code; }
        bool run();
    };
    
    // Function pointer type definitions
    typedef value (*fp0)();
    typedef value (*fp1)(value &);
    typedef value (*fp2)(value &, value&);
    typedef value (*fp3)(value &, value&, value&);
    typedef value (*fp4)(value &, value&, value&, value&);
    typedef value (*fp5)(value &, value&, value&, value&, value&);

    // Native random function
    value rnum(value &low, value &high);

    // Native debug point
    value breakvm();

    // Native print functions
    value print(value &v);
#define print1 print
    value print2(value &v1, value &v2);
    value print3(value &v1, value &v2, value &v3);
    value print4(value &v1, value &v2, value &v3, value &v4);
    value print5(value &v1, value &v2, value &v3, value &v4, value &v5);
}

#endif