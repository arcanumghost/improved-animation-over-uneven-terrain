#ifndef _AF_MATRIX_H_
#define _AF_MATRIX_H_

#include <cassert>
#include <algorithm>
#include "af_singl.h"
#include "af_vec.h"

namespace af
{
    // Future implementation for GL-style matrices / matrix stack
    class MatrixStack : public Singleton<MatrixStack>
    {

    };

    // Kinds of initialization
    enum MatrixType { ZERO, IDENTITY };

    // Square matrix class
    template<int s, class T>
    struct matrix
    {
        T m[s*s];

        matrix<s, T>(MatrixType matrixType = ZERO)
        {
            for(int i=0; i<s*s; ++i)
                m[i] = 0;
            switch(matrixType)
            {
            case IDENTITY:
                for(int i = 0; i < s; ++i)
                    m[i*s + i] = 1;
                break;
            default:
            case ZERO:
                break;
            }
		}

		bool operator==(matrix<s, T> &rhs)
		{
			for(int i=0; i<s*s; ++i)
				if(abs(m[i]-rhs[i]) > .0001)
					return false;
			return true;
		}

		bool operator!=(matrix<s, T> &rhs)
		{
			for(int i=0; i<s*s; ++i)
				if(abs(m[i]-rhs[i]) > .0001)
					return true;
			return false;
		}

        T& operator[] (int i)
        {
            assert(i >= 0 && i < s*s);
            return m[i];
        }

        T operator[] (int i) const
        {
            assert(i >= 0 && i < s*s);
            return m[i];
        }

        T& at(int i, int j)
        {
            assert(i >= 0 && i < s && j >= 0 && j < s);
            return m[i*s + j];
        }

        T at(int i, int j) const
        {
            assert(i >= 0 && i < s && j >= 0 && j < s);
            return m[i*s + j];
        }

        matrix<s, T> &operator+=(const matrix<s, T> &rhs)
        {
            for(int i=0; i<s; ++i)
                for(int j=0; j<s; ++j)
                {
                    at(i, j) += rhs.at(i, j);
                }
            return *this;
        }

        matrix<s, T> operator*(const matrix<s, T>& rhs) const
        {
            matrix<s, T> result;
            for(int i=0; i<s; ++i)
                for(int j=0; j<s; ++j)
                {
                    result.at(i, j) = 0;
                    for(int k=0; k<s; ++k)
                        result.at(i,j) += at(i,k)*rhs.at(k,j);
                }
            return result;
        }

        matrix<s, T> &operator+=(const float &rhs)
        {
            for(int i=0; i<s*s; ++i)
                m[i] += rhs;
            return *this;
        }

        matrix<s, T> &operator-=(const float &rhs)
        {
            for(int i=0; i<s*s; ++i)
                m[i] -= rhs;
            return *this;
        }

        matrix<s, T> &operator*=(const float &rhs)
        {
            for(int i=0; i<s*s; ++i)
                m[i] *= rhs;
            return *this;
        }

        matrix<s, T> &operator/=(const float &rhs)
        {
            for(int i=0; i<s*s; ++i)
                m[i] /= rhs;
            return *this;
        }
    };

    // Only matrix ever used. 3x3f should be implemented some day.
    typedef matrix<4, float> matrix4x4f;

    matrix4x4f matrix(float m00, float m01, float m02, float m03,
                       float m10, float m11, float m12, float m13,
                       float m20, float m21, float m22, float m23,
                       float m30, float m31, float m32, float m33);

    bool Inverse(const matrix4x4f &m, matrix4x4f &invOut);

    vec3f xvec(const matrix4x4f &m);
    vec3f yvec(const matrix4x4f &m);
    vec3f zvec(const matrix4x4f &m);
    vec3f pos(const matrix4x4f &m);
}

#endif