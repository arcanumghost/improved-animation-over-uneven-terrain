#ifndef _AF_ANIMBLEND_H_
#define _AF_ANIMBLEND_H_

#include "af_singl.h"
#include "af_anim.h"
#include "af_rune.h"
#include <string>
#include <queue>

namespace af
{
    class AnimBlender : public Singleton<AnimBlender>
    {
        AnimBlender();
    public:
        std::map<std::string, Anim *> m_animdigest;
        
        // Which animations are being currently blended, and individual frame
        // numbers for each
        unsigned m_nanims;
        Anim *m_anims[8];
        unsigned m_frames[8];
        float m_goalweights[8];
        float m_weights[8];
        queue<Anim *> m_animqueues[8];

        // Resulting data, don't allow for more than 100 bones
        matrix4x4f m_bonedata[100];

        // Wrapper for loading Animations
        void registeranim(char *path);
        // Update if you want the next frame of 
        void update();
        static value nplay(value &animname, value &weight);
        static void play(std::string animname, float weight);
        static void initialize();
    };
}

#endif