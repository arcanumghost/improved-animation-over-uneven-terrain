#include "afscreen.h"

namespace af
{
	ScreenObject::ScreenObject(float width, float height, Shader *shader)
		: Object("Screen")
	{
		std::vector<SimpleVertex> mvVector;
		std::vector<uint> iVector;

		SimpleVertex sv;
		sv.position = vec3f(-1.0f, -1.0f, 0);
		sv.texCoord = vec2f(0, 0);
		mvVector.push_back(sv);
		sv.position = vec3f(1.0f, -1.0f, 0);
		sv.texCoord = vec2f(1.0f, 0);
		mvVector.push_back(sv);
		sv.position = vec3f(1.0f, 1.0f, 0);
		sv.texCoord = vec2f(1.0f, 1.0f);
		mvVector.push_back(sv);
		sv.position = vec3f(-1.0f, 1.0f, 0);
		sv.texCoord = vec2f(0, 1.0f);
		mvVector.push_back(sv);

		iVector.push_back(0);
		iVector.push_back(1);
		iVector.push_back(2);
		iVector.push_back(0);
		iVector.push_back(2);
		iVector.push_back(3);

		createRenderable(&mvVector[0].position[0], unsigned(mvVector.size()), sizeof(SimpleVertex), iVector, shader);
		deactivate();
	}

    ScreenObject::~ScreenObject()
    {
        if(m_renderables.size() && m_renderables[0]->m_shader)
            delete m_renderables[0]->m_shader;
    }
}