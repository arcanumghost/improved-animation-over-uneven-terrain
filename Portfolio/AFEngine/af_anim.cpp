#define _CRT_SECURE_NO_WARNINGS

#include "af_anim.h"
#include <queue>
#include <AntTweakBar.h>

namespace af
{
    Anim::Anim()
        : m_frame(0), m_mtype(DUMMY), m_boneslist(nullptr), m_rootanim(false)
    {}

    Anim::Anim(char *path)
        : m_frame(0), m_mtype(DUMMY), m_boneslist(nullptr), m_rootanim(false)
    {
        unsigned pathnamelength = unsigned(strlen(path));
        char *longpath = new char[pathnamelength+5];
        unsigned i=0;
        strcpy(longpath, path);
		strcpy(&longpath[pathnamelength], ".afm");

        LoadAFM(longpath);
        delete [] longpath;
        
        learnfootsteps();
    }

    // This function sets up things that will be useful for determining dynamically where the
    // next 
    void Anim::learnfootsteps()
    {
        maxfootstep.L.footheight = maxfootstep.L.toeheight = FLT_MIN;
        maxfootstep.R.footheight = maxfootstep.R.toeheight = FLT_MIN;

        // walk frames
        unsigned frame;
        std::queue<Anim *> animqueue;
        for(frame=0; frame<m_nframes; ++frame)
        {
            // One footstepframe per frame
            footsteps.push_back(footstepframe());
            
            // Set up the animqueue for this frame
            animqueue.push(this);
            if(m_relRot.size())
                m_m_matrix = m_relRot[frame%m_relRot.size()].mat4(m_relTran[frame%m_relTran.size()], m_relScale[frame%m_relScale.size()]);
            else
                m_m_matrix = matrix4x4f(af::IDENTITY);

            // walk anim structure for this frame
            while(animqueue.size())
            {
                // Send the model matrix on through the children
                for(unsigned i=0; i<animqueue.front()->m_children.size(); ++i)
                {
                    // assign the rot/tran/scale parts of the transformation
                    Anim *tanim = animqueue.front()->m_children[i];
                    if(tanim->m_relRot.size())
                    {
                        tanim->m_rot = tanim->m_relRot[frame%tanim->m_relRot.size()];
                        tanim->m_tran = tanim->m_relTran[frame%tanim->m_relTran.size()];
                        tanim->m_scale = tanim->m_relScale[frame%tanim->m_relScale.size()];

                        /*if(!tanim->m_name.compare("Bip001 R Toe0") || !tanim->m_name.compare("Bip001 L Toe0"))
                            printf("Difference between Foot and Toe: %f\n", tanim->m_tran.dist());*/
                    }
                    else
                    {
                        tanim->m_rot = quat(0, 0, 0, 0);
                        tanim->m_tran = vec3f(0, 0, 0);
                        tanim->m_scale = vec3f(1, 1, 1);
                    }

                    // Create a relative model matrix for the child and use the parent to make a net transformation
                    tanim->m_m_matrix = tanim->m_rot.mat4(tanim->m_tran, tanim->m_scale) * animqueue.front()->m_m_matrix;
                    
                    // Push the child on the queue to be addressed again soon.
                    animqueue.push(tanim);
                }

                // determine lowest points and reasonable tolerance for certain nodes
                // Nodes are "Bip001 R Foot, Bip001 R Toe0, Bip001 L Foot, Bip001 L Toe1"
                if(!animqueue.front()->m_name.compare("Bip001 R Toe0"))
                {
                    footsteps.back().R.toeheight = animqueue.front()->m_m_matrix.m[13];
                    if(minfootstep.R.toeheight > footsteps.back().R.toeheight)
                        minfootstep.R.toeheight = footsteps.back().R.toeheight;
                    if(maxfootstep.R.toeheight < footsteps.back().R.toeheight)
                        maxfootstep.R.toeheight = footsteps.back().R.toeheight;
                }
                else if(!animqueue.front()->m_name.compare("Bip001 L Toe0"))
                {
                    footsteps.back().L.toeheight = animqueue.front()->m_m_matrix.m[13];
                    if(minfootstep.L.toeheight > footsteps.back().L.toeheight)
                        minfootstep.L.toeheight = footsteps.back().L.toeheight;
                    if(maxfootstep.L.toeheight < footsteps.back().L.toeheight)
                        maxfootstep.L.toeheight = footsteps.back().L.toeheight;
                }
                else if(!animqueue.front()->m_name.compare("Bip001 R Foot"))
                {
                    footsteps.back().R.footheight = animqueue.front()->m_m_matrix.m[13];
                    if(minfootstep.R.footheight > footsteps.back().R.footheight)
                        minfootstep.R.footheight = footsteps.back().R.footheight;
                    if(maxfootstep.R.footheight < footsteps.back().R.footheight)
                        maxfootstep.R.footheight = footsteps.back().R.footheight;
                }
                else if(!animqueue.front()->m_name.compare("Bip001 L Foot"))
                {
                    footsteps.back().L.footheight = animqueue.front()->m_m_matrix.m[13];
                    if(minfootstep.L.footheight > footsteps.back().L.footheight)
                        minfootstep.L.footheight = footsteps.back().L.footheight;
                    if(maxfootstep.L.footheight < footsteps.back().L.footheight)
                        maxfootstep.L.footheight = footsteps.back().L.footheight;
                }

                // Pop the old animation
                animqueue.pop();
            }

            /*printf("%d %f %f %f %f\n", frame, footsteps.back().L.footheight, footsteps.back().L.toeheight, 
                footsteps.back().R.footheight, footsteps.back().R.toeheight);*/
        }

        /*printf("Max heights: %f %f %f %f\n", maxfootstep.L.footheight, maxfootstep.L.toeheight, 
            maxfootstep.R.footheight, maxfootstep.R.toeheight);
        printf("Min heights: %f %f %f %f\n", minfootstep.L.footheight, minfootstep.L.toeheight, 
            minfootstep.R.footheight, minfootstep.R.toeheight);//*/

        // walk frames
        for(frame=0;false; )
        {
            // determine and mark frames for L/R Up/Down/Flat

        }
    }

    void Anim::LoadSubAFM(std::fstream &infile, std::vector<Anim *> &models, std::vector<Anim *> *boneslist, matrix4x4f &_zerotran)
    {
        // Header, used for making decisions
        short header;
        
        // variables 
        std::vector<ModelVertex> vertices;
        std::vector<AnimModelVertex> skinnedvertices;
        std::vector<uint> indices;
        
        // Variables for tracking progress with this model list
        unsigned modelsstart = unsigned(infile.tellg())-2;    // -2 for header, already eaten
        unsigned modelslength;
        infile.read((char *)&modelslength, 4);

        matrix4x4f new_zerotran(IDENTITY);

        // Build Models for this list
        while(infile.tellg() < modelsstart + modelslength)
        {
            // Begin a new model
            infile.read((char *)&header, 2);        // Model header
            assert(header==0x4001);
            Anim *m = new Anim();
            m->m_boneslist = boneslist;

            // Variables for tracking progress with this model
            unsigned modelstart = unsigned(infile.tellg())-2;
            unsigned modellength;
            infile.read((char *)&modellength, 4);

            // Build this model
            while(infile.tellg() < modelstart + modellength)
            {
                infile.read((char *)&header, 2);
                switch(header)
                {
                case 0x0005:    // String
                    {
                        // Unused, but parse it and throw it away
                        unsigned int strlength;
                        infile.read((char *)&strlength, 4);
                        char *modelname = new char[strlength-6+1];
                        infile.read(modelname, strlength-6);
                        modelname[strlength-6] = 0;
                        m->m_name = modelname;
                        delete []  modelname;

                        // Import the type
                        infile.read((char *)&m->m_mtype, 4);
                        if(m->m_mtype == BONE)   // Bones
                            boneslist->push_back(m);

                        // Import the zero position
                        infile.read((char *)&m->m_zerotran, sizeof(matrix4x4f));
                        new_zerotran = m->m_zerotran*_zerotran;
                        Inverse(new_zerotran, m->m_zerotran);
                        break;
					}
				case 0x5000:    // Matrix List
					{
						unsigned int animlength;
						infile.read((char *)&animlength, 4);
						vector<matrix4x4f> tmatrices;
						tmatrices.resize((animlength-6)/sizeof(matrix4x4f));
						infile.read((char *)&tmatrices[0], animlength-6);
						// Big problem if you break here
						/*m->m_relTranMatrix.resize((animlength-6)/sizeof(matrix4x4f));
						infile.read((char *)&m->m_relTranMatrix[0], animlength-6);*/
						break;
					}
				case 0x5001:    // Tran Anim List
					{
						unsigned int animlength;
						infile.read((char *)&animlength, 4);
						animlength -= 6;
						animlength /= sizeof(quat) + 2*sizeof(vec3f);
						m->m_relRot.resize(animlength);
						m->m_relTran.resize(animlength);
						m->m_relScale.resize(animlength);
						infile.read((char *)&m->m_relRot[0], animlength*sizeof(quat));
						infile.read((char *)&m->m_relTran[0], animlength*sizeof(vec3f));
						infile.read((char *)&m->m_relScale[0], animlength*sizeof(vec3f));
						break;
					}
                case 0x4002:    // Mesh
                    {
                        unsigned int meshlength;
                        infile.read((char *)&meshlength, 4);
                        unsigned int matid;         infile.read((char *)&matid, 4);
                        unsigned int vertexmask;    infile.read((char *)&vertexmask, 4);

                        /*if(vertexmask != (VERTEX_DIFFTEXCOORD | VERTEX_NORMAL | VERTEX_POSITION))
                            exit(-1);*/

                        infile.read((char *)&header, 2);
                        unsigned int vertexlistsize;    infile.read((char *)&vertexlistsize, 4);

                        if(m->m_mtype == SKIN)
                        {
                            skinnedvertices.resize((vertexlistsize-6)/48);
                            infile.read((char *)&skinnedvertices[0], vertexlistsize-6);
                        }
                        else
                        {
                            vertices.resize((vertexlistsize-6)/32);
                            infile.read((char *)&vertices[0], vertexlistsize-6);
                        }

                        infile.read((char *)&header, 2);
                        unsigned int indexlistsize;    infile.read((char *)&indexlistsize, 4);

                        indices.resize((indexlistsize-6)/4);
                        infile.read((char *)&indices[0], indexlistsize-6);
                    }
                    break;
                case 0x4000:    // Model list
                    LoadSubAFM(infile, m->m_children, boneslist, new_zerotran);
                    break;
                default:
                    throw "Unrecognized";
                }
            }

            // Push it on 
            models.push_back(m);
        }
    }

    void Anim::LoadAFM(char *path)
    {
        m_updateTime = false;
        m_time = 0.0f;

        // load the obj file
        std::fstream infile(path, std::ios::binary | std::ios::in);
        
        if(infile.fail())
            throw "Didn't open file";

        // Clear / check header
        unsigned short header;              infile.read((char *)&header, 2);
        unsigned int length;                infile.read((char *)&length, 4);
        unsigned short version;             infile.read((char *)&version, 2);
        infile.read((char *)&m_nframes, 4); m_nframes*=2; // Double the length for 60fps
        m_frame = m_nframes-1;
        BSphere sphere;                     infile.read((char *)&sphere, 16);

        if(header != 0xAFFF || version < 0x0101)
            exit(-1);

        // Collect materials
        infile.read((char *)&header, 2);
        infile.read((char *)&length, 4);
        unsigned int position=6;
		std::vector<char> texturePathBuf;
		std::vector<wchar_t> texturePathBufW;
        while(position < length)
        {
            unsigned int matlength;
            unsigned int texturemask;
            infile.read((char *)&header, 2);
            infile.read((char *)&matlength, 4);
            position += matlength;
            unsigned matposition = 10;
            infile.read((char *)&texturemask, 4);
            while(matposition < matlength)
            {
                // Read texture filename
                unsigned int textureNameLength = 0;
                infile.read((char *)&header, 2);
                infile.read((char *)&textureNameLength, 4);
                matposition += textureNameLength;
                textureNameLength-=6;
				texturePathBuf.resize( length+1 );
                char *texturePath = &texturePathBuf[0];
                infile.read(texturePath, textureNameLength);
                texturePath[textureNameLength] = 0;

				texturePathBufW.resize( textureNameLength+1 );
                wchar_t *wTexturePath = &texturePathBufW[0];
                mbstowcs(wTexturePath, texturePath, textureNameLength+1);
            }
        }

        // Load models, start by simply passing the nframes, models, and textures
        infile.read((char *)&header, 2);            // Model List header
        assert(header==0x4000);

        // Register the name of this object
        string pathname(path);
        pathname = pathname.substr(0, pathname.size()-4);

        m_boneslist = new std::vector<Anim *>;
        m_rootanim = true;

        matrix4x4f _zerotran(IDENTITY);
        LoadSubAFM(infile, m_children, m_boneslist, _zerotran);

        // Close file and leave
        infile.close();
    }

    void Anim::update(unsigned frame, framedata &fd)
    {
        matrix4x4f transformedMatrix;

        // WOLF_DEBUG
        // if(m_relTranMatrix.size())
        if(m_relTran.size())
		{
			fd.m_rot = m_relRot[(frame/2)%m_relRot.size()];
            fd.m_tran = m_relTran[(frame/2)%m_relTran.size()];
			fd.m_scale = m_relScale[(frame/2)%m_relScale.size()];
		}
        else
		{
			fd.m_rot = quat(0, 0, 0, 0);
			fd.m_tran = vec3f(0, 0, 0);
			fd.m_scale = vec3f(1, 1, 1);
		}
        fd.m_name = m_name;

        //* Cancel out the XZ translation at the skeleton level. It will be used for character speed elsewhere.
        if(!strcmp(m_name.c_str(), "Bip001"))
        {
            // If the frame is 0, interpolate the XZ translation
            if(!frame)
            {
                vec3f ttran = m_relTran[1] - m_relTran[0];
                vec3f ttran2 = m_relTran[m_relTran.size() - 1] - m_relTran[m_relTran.size() - 2];
                fd.m_tran.x = (ttran.x+ttran2.x)/2.0f;
                fd.m_tran.z = (ttran.z+ttran2.z)/2.0f;
            }
            // Otherwise, just use the relative XZ translation from the last frame on even frames.
            else if(!(frame%2))
            {
                vec3f ttran = m_relTran[(frame/2)%m_relTran.size()] - m_relTran[(frame/2)%m_relTran.size() - 1];
                fd.m_tran.x = ttran.x;
                fd.m_tran.z = ttran.z;
            }
            // remove 
            else
            {
                fd.m_tran.x = fd.m_tran.z = 0.0f;
            }
        }

        for(int i=0; i<m_children.size(); ++i)
        {
            if(fd.m_children.size() == i)
                fd.m_children.push_back(new framedata());

            m_children[i]->update(frame, *fd.m_children[i]);
        }
        /*std::for_each(m_children.begin(), m_children.end(), [&](Anim *m)
        {
            m->update(frame);
        });*/
    }

    // TODO move to framedata
    void Anim::updateanim(const matrix4x4f &modelMat )
    {
        matrix4x4f transformedMatrix;

        //transformedMatrix = m_m_matrix*modelMat;
		
        /*if(m_mtype == BONE)
            transformedMatrix = m_m_matrix*modelMat;
        else if(m_relTranMatrix.size())
            transformedMatrix = m_relTranMatrix[0]*modelMat;
        else
            transformedMatrix = modelMat;*/


        /*matrix4x4f modelInv;
        if(!Inverse(transformedMatrix, modelInv))   
            DebugBreak();

        m_vp_matrix = vp_Mat;*/
        m_m_matrix = m_rot.mat4(m_tran, m_scale)*modelMat;

        std::for_each(m_children.begin(), m_children.end(), [&](Anim *m)
        {
            m->updateanim(m_m_matrix);
        });
    }

    const framedata *const Anim::work()
    {
        if(++m_frame >= m_nframes)
            m_frame = 0;

        // Debug frame
        //m_frame = 47;

        update(m_frame, m_workingframe);

        return &m_workingframe;
    }

    Anim::~Anim()
    {
        if(m_rootanim)
            delete m_boneslist;

        for(int i=0; i<m_children.size(); ++i)
        {
            delete m_children[i];
        }
    }
}