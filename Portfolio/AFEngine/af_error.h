#ifndef _AF_ERROR_H_
#define _AF_ERROR_H_

#include <exception>

namespace af
{
    // Unused
    class afexception : public std::exception
    {
    public:
        char *what();
    };
}

#endif