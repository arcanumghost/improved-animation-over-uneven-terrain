// AFEngine.cpp : Defines the entry point for the application.
//
#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <cassert>
#include <crtdbg.h>
#include <iostream>
#include <sstream>

// 
#include "AFEngine.h"

// Debugging includes
#include "guicon.h"
#include "af_error.h"
#include "af_test.h"

// Engine includes
#include "af_rndrr.h"
#include "af_blend.h"
#include "af_time.h"
#include "af_cam.h"
#include "af_frstm.h"
#include "af_input.h"

// Object includes
#include "af_model.h"
#include "af_axis.h"
#include "af_bsp.h"
#include "af_cube.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
HWND				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

// Frame cycle calls
void Input( HWND );
void Update( float, af::Axis &, af::Model &, af::Model & ); //, af::BSP &, af::Cube & );
void Render( HDC, HWND );

// Global objects for camera and frustum
af::Camera g_camera;
af::Frustum g_frustum;

/*
af::value pause()
{
    g_paused = !g_paused;
    af::value v;
    v.m_type = af::V_ERROR;
    v.m_fptr = 0x0badf00d;
    return v;
}*/

int APIENTRY _tWinMain(HINSTANCE hInstance,
                       HINSTANCE hPrevInstance,
                       LPTSTR    lpCmdLine,
                       int       nCmdShow)
{
    // Set up Memory allocation debugging
#ifdef _DEBUG
    af::guicon::init();
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif  // _DEBUG
    // Uncomment this line and put the memory allocation # for where there is a leak
	//_CrtSetBreakAlloc( 8259 ); 

	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_AFENGINE, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
    HWND hWnd = InitInstance (hInstance, nCmdShow);
    if(!hWnd)
	{
        MessageBox( NULL, TEXT( "Failed to Initialize Window" ), 
            TEXT( "Windows Error" ), MB_OK | MB_ICONERROR);
	}
	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_AFENGINE));
    
    // Initialize the renderer
    RECT clientRect;
    GetClientRect(hWnd, &clientRect); 
    float aspectRatio = 
        clientRect.right / static_cast< float >( clientRect.bottom );
    g_frustum.init( 45.0f, aspectRatio, 10.0f, 3000.0f );
    af::Renderer::initialize(hWnd, clientRect);
    af::AnimBlender::initialize();

    // Define default shader and axis that use it.
    af::Shader *defaultshader = af::Renderer::getInstance().createShader("shaders\\default");
    af::Axis axis(50.0f, defaultshader);
    
    // Define model shader and the model that uses it.
    af::Shader *modelshader = af::Renderer::getInstance().createShader("shaders\\model");
    af::Shader *animshader = af::Renderer::getInstance().createShader("shaders\\animmodel");

    // Two models to view
    // WOLF_DEBUG
    //* Wolf set
    af::Model model = af::Model("grywalk.afm", modelshader, animshader);
	model.m_translation.x = 100.0f;

	af::Model level = af::Model("level.afm", modelshader, animshader);
    
    af::AnimBlender::getInstance().registeranim("grywalk");
    af::AnimBlender::getInstance().registeranim("gryidle");
	af::AnimBlender::getInstance().registeranim("gryjump");//*/

    /* Zax set
    af::Model model = af::Model("zax.afm", modelshader, animshader);
    
    af::AnimBlender::getInstance().registeranim("jump_out");
    af::AnimBlender::getInstance().registeranim("jump_up");
    af::AnimBlender::getInstance().registeranim("idle_stand");
    af::AnimBlender::getInstance().registeranim("idle_stretch");
    af::AnimBlender::getInstance().registeranim("walk_frwd");
    af::AnimBlender::getInstance().registeranim("idle_look_around");
    af::AnimBlender::getInstance().registeranim("turn_left");
    af::AnimBlender::getInstance().registeranim("gun_front_shoot");
    af::AnimBlender::getInstance().registeranim("gun_down_shoot");
    af::AnimBlender::getInstance().registeranim("gun_up_shoot");//*/

    // Set the camera position
    g_camera.setPosition(af::vec3f(0, 0, 100));

    // Set the center for the rotating light
    // rotLightCenter = cube.m_center;

    // render function
    af::SystemClocks nextFrameTime = 0;
    float time = 0.0f;

    // Center the mouse so there are no adverse initial rotation effects
    SetCursorPos( 400, 400 );

    // Codegen
    af::RuneParser parser("scripts\\controls.rs");
    if(parser.parse())
        parser.print();
    else
    {
        MessageBox( NULL, TEXT( "Failed to parse program, check output.txt for error" ), 
            TEXT( "RuneParser Error" ), MB_OK | MB_ICONERROR);
        parser.close();
        return -1;
    }
    
    af::RuneCodeGenerator codegen(parser.getfunctions());
    if(codegen.genCode())
        codegen.printCode();
    else
    {
        MessageBox( NULL, TEXT( "Failed to compile program, check output.txt for error" ), 
            TEXT( "RuneCompiler Error" ), MB_OK | MB_ICONERROR);
        parser.close();
        return -1;
    }

    af::RuneRuntime runtime(codegen.getCode());

	// Main message loop:
    bool quitgame = false;
    while(!quitgame)
    {
	    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	    {
            if( msg.message == WM_QUIT || (msg.message == WM_KEYDOWN && msg.wParam == VK_ESCAPE))
                quitgame = true;

		    if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		    {
			    TranslateMessage(&msg);
			    DispatchMessage(&msg);
		    }
	    }
        else
        {
            // Code replaced by controls.rs runtime
            if(!runtime.run())
            {
                parser.close();
                return -1;
            }

            // Todo FIX INPUT think of a way to get this standardized for different needs
            /*if(GetFocus() == hWnd)
                af::InputController::update( model, g_camera );*/

            // Update Game Objects
            Update( time, axis, model, level );
             
            // Render
            af::Renderer::getInstance().render();

            // Spin lock operations, once every frame
            {
                const static float SECONDS_PER_FRAME = 1.0f/60.0f;
                const static af::SystemClocks CLOCKS_PER_FRAME = af::SecondsToClocks( SECONDS_PER_FRAME );

                af::SystemClocks now = af::GetAbsoluteTimeClocks();
                while(now < nextFrameTime)
                    now = af::GetAbsoluteTimeClocks();

                nextFrameTime = now + CLOCKS_PER_FRAME;
                //if(!g_paused)
                time += SECONDS_PER_FRAME;
            }
        }
    }

    // clean up renderer and renderable objects
    af::Renderer::deleteInstance();
    
    // clean up shaders
    delete defaultshader;
    delete modelshader;
    delete animshader;

	return (int) msg.wParam;
}

void Update( float time, af::Axis &axis, af::Model &model, af::Model &level ) //, af::BSP &level, af::Cube &cube )
{
    // update camera and frustum based on camera
    g_camera.update();
    g_frustum.update(g_camera);
    
    // Set up model rotations and translations:
    //af::vec3f translation(500, 50, -600);
    af::vec3f translation(0,0,0);
	af::matrix4x4f modelMatrix(af::IDENTITY);
	af::matrix4x4f yrotMatrix(af::IDENTITY);
	af::matrix4x4f zrotMatrix(af::IDENTITY);
    af::matrix4x4f modelInverse(af::IDENTITY);
    af::matrix4x4f modelInversetest(af::IDENTITY);

    //modelInverse[0] = modelInverse[10] = modelMatrix[0] = modelMatrix[10] = -1;
    // WOLF_DEBUG
	// modelMatrix[13] = model.m_translation.y;
	model.m_verticalspeed -= 7;
	if(model.m_translation.y <= 35 && model.m_translation.x < 35.0f && model.m_translation.x > -35.0f
		&& model.m_translation.z < 35.0f && model.m_translation.z > -35.0f && model.m_verticalspeed < 0)
	{
		model.m_translation.y = 35;
		model.m_verticalspeed = 0;
		model.m_jumping = false;

		af::AnimBlender &blender = af::AnimBlender::getInstance();
		blender.m_goalweights[2] = 0.0f;
	}

	if(model.m_translation.y <= 0.0f && model.m_verticalspeed < 0)
	{
		model.m_verticalspeed = 0;
		model.m_translation.y = 0;
		model.m_jumping = false;

		af::AnimBlender &blender = af::AnimBlender::getInstance();
		blender.m_goalweights[2] = 0.0f;
	}
	model.m_translation.y += model.m_verticalspeed*(1.0f/60.0f);
	if(model.m_translation.y < 35)
	{
		if(model.m_translation.x <= 36.0f && model.m_translation.x > 35.0f && model.m_translation.z > -35.0f && model.m_translation.z < 35.0f)
			model.m_translation.x = 36.0f;
		else if(model.m_translation.x >= -36.0f && model.m_translation.x < -35.0f && model.m_translation.z > -35.0f && model.m_translation.z < 35.0f)
			model.m_translation.x = -36.0f;
		else if(model.m_translation.z <= 36.0f && model.m_translation.z > 35.0f && model.m_translation.x > -35.0f && model.m_translation.x < 35.0f)
			model.m_translation.z = 36.0f;
		else if(model.m_translation.z >= -36.0f && model.m_translation.z < -35.0f && model.m_translation.x > -35.0f && model.m_translation.x < 35.0f)
			model.m_translation.z = -36.0f;
	}
	


	if(model.m_translation.x < -300.0f)
		model.m_translation.x = -300.0f;
	else if(model.m_translation.x > 300.0f)
		model.m_translation.x = 300.0f;

	if(model.m_translation.z < -270.0f)
		model.m_translation.z = -270.0f;
	else if(model.m_translation.z > 270.0f)
		model.m_translation.z = 270.0f;

	

	modelMatrix[12] = model.m_translation.x;
    modelMatrix[13] = model.m_translation.y+20;
    modelMatrix[14] = model.m_translation.z;

	//cout << model.m_translation.x << " " << model.m_translation.y << " " << model.m_translation.z << endl;

    // WOLF_DEBUG
    // yrotMatrix[0] =   yrotMatrix[10] = cos(model.m_rotation.y);
    // yrotMatrix[8] = -(yrotMatrix[2]  = sin(model.m_rotation.y));
    yrotMatrix[0] =   yrotMatrix[10] = -cos(model.m_rotation.y);
    yrotMatrix[8] = -(yrotMatrix[2]  = -sin(model.m_rotation.y));
	zrotMatrix[5] = zrotMatrix[0] = 0;
	zrotMatrix[1] = -(zrotMatrix[4] = 1);
    modelInverse[12] = -(translation.x*modelInverse[0] + translation.x*modelInverse[1] + translation.x*modelInverse[ 2]);
    modelInverse[13] = -(translation.y*modelInverse[4] + translation.y*modelInverse[5] + translation.y*modelInverse[ 6]); 
    modelInverse[14] = -(translation.z*modelInverse[8] + translation.z*modelInverse[9] + translation.z*modelInverse[10]);
    af::Inverse(modelMatrix, modelInversetest);

    af::matrix4x4f badmatrix = yrotMatrix*modelMatrix;

    // update model
	level.update(time, g_camera.camView()*g_frustum.getProjectionMatrix(), af::matrix4x4f(af::IDENTITY), g_camera.lightPosition() );
	//level.deactivate();

    af::AnimBlender::getInstance().update();

	model.update(time, g_camera.camView()*g_frustum.getProjectionMatrix(), zrotMatrix*yrotMatrix*modelMatrix, g_camera.lightPosition(), true );

	//model.update(time, g_camera.camView()*g_frustum.getProjectionMatrix(), zrotMatrix*yrotMatrix*modelMatrix, g_camera.lightPosition(), true );

    // update axes
    axis.update( g_camera.camView()*g_frustum.getProjectionMatrix(), af::vec3f(0.0f, 0.0f, 0.0f) );
}









//// Boiler Plate, this stuff never changes

//
//  FUNCTION: MyRegisterClass()
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_AFENGINE));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= nullptr;
	wcex.lpszMenuName	= nullptr;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
HWND InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      200, 200, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return hWnd;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}