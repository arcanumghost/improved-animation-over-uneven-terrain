#include "afline.h"

namespace af
{
    Line::Line(Shader *shader)
        : Object("Line")
    {
        std::vector<DefaultVertex> mvVector;
        std::vector<uint> iVector;

        DefaultVertex rv = {vec3f(0,0,0), vec3f(1,0,0)};
        mvVector.push_back(rv);
        rv.position = vec3f(0,5000, 0);
        rv.color = vec3f(0,1,0);
        mvVector.push_back(rv);

        iVector.push_back(0);
        iVector.push_back(1);

        // set up vectors
        createRenderable(&mvVector[0].position[0], unsigned(mvVector.size()), sizeof(DefaultVertex), iVector, shader);

        m_mvpMatrix = matrix4x4f(af::IDENTITY);
        m_renderables[0]->setUniformVariable(0, &m_mvpMatrix);

        // Use lines mode for this
        m_renderables[0]->setRenderMode( AF_LINES );
    }

    Line::~Line() {}

    void Line::update(matrix4x4f &vpMatrix, vec3f p1, vec3f p2)
    {
        std::vector<DefaultVertex> mvVector;
        std::vector<uint> iVector;

        DefaultVertex rv = {p1, vec3f(1,0,0)};
        mvVector.push_back(rv);
        rv.position = p2;
        rv.color = vec3f(0,1,0);
        mvVector.push_back(rv);

        updateVertices(&mvVector[0].position[0], unsigned(mvVector.size()), sizeof(DefaultVertex));

        m_mvpMatrix = vpMatrix;
        m_renderables[0]->setUniformVariable(0, &m_mvpMatrix);
    }
}