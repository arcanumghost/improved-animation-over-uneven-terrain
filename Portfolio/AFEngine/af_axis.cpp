#include "af_axis.h"

namespace af
{
    // Create a colorful axis
    Axis::Axis(float radius, Shader *shader)
        : Object("Axis")
    {
		std::vector<DefaultVertex> mvVector;
        std::vector<uint> iVector;

        // 6 positions
        for(int i=0; i<6; ++i)
        {
            vec3f   pos, 
                    color;

            int axis = i/2;
            int dir  = (i%2)*2-1;

            pos[axis] = radius*float(dir);
            color[axis] = 1.0f;
            color[(axis+1)%3] = dir<0;

            DefaultVertex rv = { pos, color };

            mvVector.push_back(rv);
            iVector.push_back(i);
        }

        // set up vectors
        createRenderable(&mvVector[0].position[0], unsigned(mvVector.size()), sizeof(DefaultVertex), iVector, shader);

        // Use lines mode for this
        m_renderables[0]->setRenderMode( AF_LINES );
    }

    Axis::~Axis() {}
    
    // Update position and the viewprojection matrix
    void Axis::update( matrix4x4f &vpMatrix, vec3f position )
    {
        matrix4x4f tran(IDENTITY);
        tran[12] = position.x;
        tran[13] = position.y;
        tran[14] = position.z;

        m_mvpMatrix = tran*vpMatrix;

        m_renderables[0]->setUniformVariable(0, &m_mvpMatrix);
    }
}