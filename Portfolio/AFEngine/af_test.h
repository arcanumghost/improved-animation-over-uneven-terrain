#ifndef _AF_TESTCASE_H_
#define _AF_TESTCASE_H_

#include "af_error.h"
#include "guicon.h"

#include <functional>
#include <iostream>

namespace af
{
    // Performs a test case of each kind to verify test cases are working.
    void checktests();

    // Tests the function test and displays a boilerplate header / success
    void testcase(char *testname, std::function<bool ()> test);

    // Increment for each test and pass. Doesn't really solve the problem I hoped to solve in retrospect.
    void inctest();
    void incpass();

    // Tests error throwing functionality, takes in the error type as a template argument. Succeeds
    // only when catching that exception.
    template<class T>
    void testerrorcase(char *testname, std::function<bool ()> test)
    {
#ifdef _WIN32
        guicon::color(0x000F);
#endif

        inctest();

        bool passed;

        std::cout << testname << ": ... ";

        try // Success here is failure
        {
            test();
            passed = false;
        }
        catch(T &e) // Success is here
        {
            std::cout << " \"" << e.what() << "\" successfully caught ";
            passed = true;
        }

        if(passed)
            incpass();

#ifdef _WIN32
        guicon::color( passed ? FOREGROUND_GREEN : FOREGROUND_RED);
#endif
        std::cout << (passed ? " OK\n" : " Failed\n");
#ifdef _WIN32
        guicon::color(0x000F);
#endif
    }

    // Simply prints the passed / total tests.
    void report();
}

#endif