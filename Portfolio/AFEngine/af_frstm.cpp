#include "af_frstm.h"
#include "af_utils.h"
#include <cmath>

namespace af
{
    // Initialize frustum with elements for a projection matrix
    void Frustum::init(float _angle, float _ratio, float _nearD, float _farD)
    {
        // set projection matrix variables
        angle = DEGREES_TO_RADIANS*_angle*.5f;
        ratio = _ratio; 
        zNear = _nearD;
        zFar = _farD;
        
        // set variables for frustum updates
        tang = (float)tan(angle);
        nHeight = zNear*tang;
        nWidth = nHeight*ratio;
        fHeight = zFar*tang;
        fWidth = fHeight*ratio;
        
        // set projection matrix
        for(int i=0; i<16; ++i)
            m_projMatrix.m[i] = 0.0f;
        m_projMatrix.m[0] = 1/(tan(angle)*ratio);
        m_projMatrix.m[5] = 1/tan(angle);
        m_projMatrix.m[10] = (zFar+zNear)/(zNear-zFar);
        m_projMatrix.m[11] = -1;
        m_projMatrix.m[14] = (2*zFar*zNear)/(zNear-zFar);

        m_lightprojMatrix = m_projMatrix;
        m_lightprojMatrix.m[0] = 1/(tan(angle)*1.0f);
    }

    // Update the culling planes based on the current camera orientation
    void Frustum::update(Camera& c)
    {
        // modelview basis
        matrix4x4f MV = c.camView();
        vec3f nc, fc, 
            xDir(MV[0], MV[4], MV[8]),
            yDir(MV[1], MV[5], MV[9]),
            zDir(MV[2], MV[6], MV[10]);

        // calculate near and far
        nc = c.camPosition() - zDir*zNear;
        fc = c.camPosition() - zDir*zFar;

        // calculate frustum corners
        ntl = nc + yDir*nHeight - xDir*nWidth;
        ntr = nc + yDir*nHeight + xDir*nWidth;
        nbl = nc - yDir*nHeight - xDir*nWidth;
        nbr = nc - yDir*nHeight + xDir*nWidth;
        ftl = fc + yDir*fHeight - xDir*fWidth;
        ftr = fc + yDir*fHeight + xDir*fWidth;
        fbl = fc - yDir*fHeight - xDir*fWidth;
        fbr = fc - yDir*fHeight + xDir*fWidth;

        // create planes based on frustum corners
        m_planes[0].set(ntr, ntl, ftl);
        m_planes[1].set(nbl, nbr, fbr);
        m_planes[2].set(ntl, nbl, fbl);
        m_planes[3].set(nbr, ntr, fbr);
        m_planes[4].set(ntl, ntr, nbr);
        m_planes[5].set(ftr, ftl, fbl);
    }

    // Returns projection matrix
    matrix4x4f& Frustum::getProjectionMatrix( )
    {
        return m_projMatrix;
    }    
    
    // Returns light projection matrix
    matrix4x4f& Frustum::getLightProjectionMatrix( )
    {
        return m_lightprojMatrix;
    }

    // Determines whether or not a spherical bound is within the frustum or not
    bool Frustum::cullGeometry(vec3f& center, float radius)
    {
        // test 6 bounds, must be within all 6 to be drawn
        for(int i=0; i<6; ++i)
        {
            float dist = (m_planes[i].distance + dotproduct(m_planes[i].normal, center));
            if ( dist > radius)
                return true;
        }
        return false;
    }
}