#ifndef _AF_ANIM_H_ 
#define _AF_ANIM_H_

#include "af_model.h"
#include "af_quat.h"
#include "afblendnode.h"

namespace af
{
    struct footstepframe
    {
        struct ffdata 
        { 
            ffdata() : footheight(FLT_MAX), toeheight(FLT_MAX) {}

            float footheight;
            float toeheight;
        };

        ffdata L, R;
    };

    class Anim : public blendnode
    {
    public:
        std::vector<Anim *> m_children;
        // Relative matrix for transforming this object from its parent
        std::vector<quat> m_relRot;
		std::vector<vec3f> m_relTran;
		std::vector<vec3f> m_relScale;
		//std::vector<matrix4x4f> m_relTranMatrix;
        vec3f m_light;
        std::vector<Anim *> *m_boneslist;
        bool m_updateTime;
        float m_time;
        unsigned m_nframes;
        unsigned m_frame;
        ModelType m_mtype;

        bool m_rootanim;

        matrix4x4f m_zerotran;
        matrix4x4f m_m_matrix;
		
        std::string m_name;

        // One anim set per frame will be keeping a running total of the current
        // transformations
        quat m_rot;
        vec3f m_tran;
		vec3f m_scale;

        matrix4x4f m_vp_matrix;

        matrix4x4f m_bonedata[100];

        void LoadAFM(char *path);
        void LoadSubAFM(std::fstream &infile, std::vector<Anim *> &models, std::vector<Anim *> *boneslist, matrix4x4f &_zerotran);

        Anim();

        Anim(char *path);
        virtual ~Anim();

        const framedata *const work();

        void update(unsigned frame, framedata &fd);
        void updateanim(const matrix4x4f &modelMat);    // TODO move this to the framedata
        void negateTimeUpdate() { m_updateTime = !m_updateTime; }

        // Figure out the footsteps
        void learnfootsteps();
        vector<footstepframe> footsteps;
        footstepframe minfootstep, maxfootstep;
    };
}

#endif