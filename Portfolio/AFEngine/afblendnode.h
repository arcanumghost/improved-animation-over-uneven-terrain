#pragma once

#include <vector>
#include "af_vec.h"
#include "af_quat.h"

namespace af
{
    struct framedata
    {
        std::vector<framedata *> m_children;
        af::quat m_rot;
        af::vec3f m_tran;
        af::vec3f m_scale;
        std::string m_name;

        // For keeping track of temporary values
        af::matrix4x4f m_matrix;

        void comp()
        {
            m_matrix = m_rot.mat4(m_tran, m_scale);
        }

        ~framedata()
        {
            for(unsigned i=0; i<m_children.size(); ++i)
                delete m_children[i];
            m_children.clear();
        }
    };

    class blendnode
    {
    public:
        // This framedata is the root of the framedata for this blendnode
        framedata m_workingframe;
        blendnode *source;
        virtual const framedata *const work() = 0;

        virtual ~blendnode() {}
    };
}