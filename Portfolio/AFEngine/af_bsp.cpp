#define _CRT_SECURE_NO_WARNINGS
#define QUAKE3_BSP_TESSELATION_USE_STOCK_ACCESSORS_AND_OPERATORS

#include "af_bsp.h"
#include "Quake3BSPTesselation.h"
#include <fstream>
#include <sys/stat.h>
#include "af_rndrr.h"
#include <windows.h>
#include <sstream>

namespace af
{
    // YZ flip helper, because BSP axes are wrong
    template<typename T>
    void yzFlip(T& y, T& z)
    {
        T t = y;
        y = z;
        z = -t;
    }

    // helper to load lumps
    template<typename T>
    void loadLump(std::vector<T>& v, BSPLump& lump, std::ifstream& in)
    {
        v.resize(lump.length / sizeof(T));
        in.seekg(lump.offset);
        in.read(reinterpret_cast<char*>(v.data()), lump.length);
    }

    // Loads a bsp file in full
    BSP::BSP(char *filename, Shader *lightMapShader, Shader *defaultShader)
		: Object("BSPLevel")
    {
		// Save shader and resolve uniform addresses
        m_lightmapShader = lightMapShader;
        m_defaultShader = defaultShader;
        m_shadowMap = Renderer::getInstance().createTexture();
        m_shadowMap->texture = 1;

        // Load and check BSP Header
        std::ifstream in;
        in.open(filename, std::ios::binary);
        in.read(reinterpret_cast<char*>(&m_bspHeader), sizeof(BSPHeader));
        if(!m_bspHeader.valid())
            return;

        // Load Lumps
        in.read(reinterpret_cast<char*>(&m_bspLumps), sizeof(BSPLump)*I_MAXLUMPS);

        // Load textures
        loadLump(m_bspTextures, m_bspLumps[I_TEXTURES], in);
        m_textures = new Texture*[m_bspLumps[I_TEXTURES].length / sizeof(BSPTexture)];

        // Loads a texture from file
        for(uint i=0; i<(m_bspLumps[I_TEXTURES].length / sizeof(BSPTexture)); ++i)
        {
            // Factorate a texture
            m_textures[i] = Renderer::getInstance().createTexture();

            // Create the file name
            wchar_t *wTexturePath = new wchar_t[95];
            char *texturePath = new char[95];
            strcpy(texturePath, "pak0_stripped\\");
            strcpy(&(texturePath[14]), m_bspTextures[i].strName);
            int length = int(strlen(m_bspTextures[i].strName));

            // Try .tga
            strcpy(&(texturePath[length+14]), ".tga\0");
            struct stat buffer;
            if(!stat(texturePath,&buffer))
            {
                mbstowcs(wTexturePath, texturePath, 95);
                m_textures[i]->loadFromFile(wTexturePath, true);
            }
            else
            {
                // Try .jpg
                strcpy(&(texturePath[length+14]), ".jpg\0");
                if(!stat(texturePath,&buffer))
                {
                    mbstowcs(wTexturePath, texturePath, 95);
                    m_textures[i]->loadFromFile(wTexturePath, true);
                }
                // Default case
                else
                {
                    m_textures[i]->loadFromFile(TEXT("default.png"), true);
                }
            }

            // delete temporary allocated memory
            delete [] wTexturePath;
            delete [] texturePath;
        }

        // Load faces
        loadLump(m_bspFaces, m_bspLumps[I_FACES], in);
        m_lastFaceIterations = new unsigned int[m_bspLumps[I_FACES].length / sizeof(BSPFace)];

        // Load BSPNodes
        loadLump(m_bspNodes, m_bspLumps[I_NODES], in);
        for(uint i=0; i<(m_bspLumps[I_NODES].length / sizeof(BSPNode)); ++i)
        {
            yzFlip(m_bspNodes[i].mins[1], m_bspNodes[i].mins[2]);
            yzFlip(m_bspNodes[i].maxs[1], m_bspNodes[i].maxs[2]);
        }

        // Load BSPPlanes
        loadLump(m_bspPlanes, m_bspLumps[I_PLANES], in);
        for(uint i=0; i < (m_bspLumps[I_PLANES].length / sizeof(BSPPlane)); ++i)
            yzFlip(m_bspPlanes[i].normal.y, m_bspPlanes[i].normal.z);

        // Load BSPLeafs
        loadLump(m_bspLeafs, m_bspLumps[I_LEAFS], in);
        for(uint i=0; i<(m_bspLumps[I_LEAFS].length / sizeof(BSPLeaf)); ++i)
        {
            yzFlip(m_bspLeafs[i].mins[1], m_bspLeafs[i].mins[2]);
            yzFlip(m_bspLeafs[i].maxs[1], m_bspLeafs[i].maxs[2]);
        }
       
        // Load BSPLeafFaces
        loadLump(m_bspLeafFaces, m_bspLumps[I_LEAFFACES], in);

        // Load light maps
        loadLump(m_bspLightmaps, m_bspLumps[I_LIGHTMAPS], in);
        unsigned char *lightMapData = reinterpret_cast<unsigned char*>(m_bspLightmaps.data());
        for(int i=0; i<m_bspLumps[I_LIGHTMAPS].length; ++i)
            lightMapData[i] = unsigned char(255*pow(lightMapData[i]/255.0f, 1.0f/GAMMA));

        // create textures for the lightmaps
        m_lightmaps = new Texture*[m_bspLumps[I_LIGHTMAPS].length / sizeof(BSPLightmap)];
        for(uint i=0; i<(m_bspLumps[I_LIGHTMAPS].length / sizeof(BSPLightmap)); ++i)
        {
            m_lightmaps[i] = Renderer::getInstance().createTexture();

            // Add the alpha channel
            unsigned char data[128*128*4];
            for(uint j=0; j<(128*128); ++j)
            {
                data[j*4+0] = m_bspLightmaps[i].imageBits[j/128][j%128][0];
                data[j*4+1] = m_bspLightmaps[i].imageBits[j/128][j%128][1];
                data[j*4+2] = m_bspLightmaps[i].imageBits[j/128][j%128][2];
                data[j*4+3] = char(255);
            }

            // load from memory
            m_lightmaps[i]->loadFromMemory(128, 128, (uint*)&data, false);
        }

        // Load vertices
        loadLump(m_bspVertices, m_bspLumps[I_VERTICES], in);

        // Load indices
        loadLump(m_bspIndices, m_bspLumps[I_INDICES], in);

        // Load leaf clusters
        in.seekg(m_bspLumps[I_VISDATA].offset);
        in.read(reinterpret_cast<char*>(&m_bspVisData), 8);
        m_bspVisData.pVecs = new char[m_bspVisData.numOfVectors*m_bspVisData.vectorSize];
        in.read(m_bspVisData.pVecs, m_bspVisData.numOfVectors*m_bspVisData.vectorSize);

        // Close the file now that we're done loading
        in.close();

        // Load Bezier Patches
        jeffwofford::TesselateBezierPatches< BSPFace, BSPInternalVertex, uint >
            ( m_bspFaces, m_bspVertices, m_bspVertices, m_bspIndices );

        // This YZ flip must remain unsigned, so YZFlip is not used.
        for( unsigned int i=0; i<m_bspIndices.size(); i+=3)
        {
            unsigned int t = m_bspIndices[i+1];
            m_bspIndices[i+1] = m_bspIndices[i+2];
            m_bspIndices[i+2] = t;
        }

        // Make VBOs/IBOs
        // Prep the vertices
        for(unsigned int i=0; i<m_bspVertices.size(); ++i)
        {
            yzFlip(m_bspVertices[i].vPosition.y, m_bspVertices[i].vPosition.z);
            yzFlip(m_bspVertices[i].vNormal.y, m_bspVertices[i].vNormal.z);
        }

        // Assemble a Renderable-compatible list of vertices.
        std::vector<BSPVertex> bspRenderableVertices;
        bspRenderableVertices.reserve(m_bspVertices.size());
        for(uint i=0; i<m_bspVertices.size(); ++i)
        {
            BSPVertex rv = {m_bspVertices[i].vPosition,
                            m_bspVertices[i].vTextureCoord,
                            m_bspVertices[i].vLightmapCoord,
                            m_bspVertices[i].vNormal};
            bspRenderableVertices.push_back(rv);
        }

        // Create shadow vertex buffer
        std::vector<af::SMVertex> smvBuffer;
        for(uint i=0; i<bspRenderableVertices.size(); ++i)
        {
            SMVertex smv;
            smv.position = bspRenderableVertices[i].position;
            smvBuffer.push_back(smv);
        }

        // create the renderable and set it up for custom rendering
        m_renderables.push_back(Renderer::getInstance().createRenderable(&bspRenderableVertices[0].position[0], unsigned(bspRenderableVertices.size()), sizeof(BSPVertex), m_bspIndices, m_defaultShader));
        //createShadowRenderable(&smvBuffer[0].position[0], unsigned(smvBuffer.size()), sizeof(SMVertex), m_bspIndices, Renderable::s_shadowShader);
        m_renderables[0]->deactivate();
        //Renderer::getInstance().addObject(this);
    }

    // update frustum, light, and mvp matrix
    void BSP::update(Frustum &frustum, Camera &c )
    {
        updateCluster( c );

        m_frustum = &frustum;
        m_lightPos = c.lightPosition();

        m_mvpMatrix = c.camView()*frustum.getProjectionMatrix();
        m_lvpMatrix = c.lightView()*frustum.getLightProjectionMatrix();
    }

    // object render
    void BSP::render()
    {
        draw();
    }

    // helper for extracting bounding boxes as vec3f
    vec3f vec3itovec3f(int *vec)
    {
        return vec3f( (float)vec[0], (float)vec[1], (float)vec[2] );
    }

    // draw iteration keeps track of the current frame, used to prevent double
    // draws on the same face
    static unsigned int drawIteration = 0;
    // draw the bsp tree
    void BSP::draw( bool cullBSP, int nodeIndex )
    {
        // iterate on the tree root
        if(!nodeIndex)
            ++drawIteration;

        // check visibility of a node
        vec3f minPoint = (nodeIndex < 0) ? vec3itovec3f(m_bspLeafs[~nodeIndex].mins) : vec3itovec3f(m_bspNodes[nodeIndex].mins);
        vec3f maxPoint = (nodeIndex < 0) ? vec3itovec3f(m_bspLeafs[~nodeIndex].maxs) : vec3itovec3f(m_bspNodes[nodeIndex].maxs);
        vec3f center = (minPoint + maxPoint) / 2.0f;
        if((nodeIndex >= 0 || isClusterVisible(&m_bspVisData, m_currentCluster, m_bspLeafs[~nodeIndex].cluster)) 
            && !m_frustum->cullGeometry(center, (minPoint-center).dist()))
        {
            // Draw this leaf node
            if(nodeIndex < 0)
            {
                int leafIndex = m_bspLeafs[~nodeIndex].leafFace;
                for( int i=0; i<m_bspLeafs[~nodeIndex].n_leafFace; ++i)
                    drawFace(m_bspLeafFaces[leafIndex+i].faceIndex);

            }
            // Visit this node
            else
            {
                draw( cullBSP, m_bspNodes[nodeIndex].front );
                draw( cullBSP, m_bspNodes[nodeIndex].back );
            }
        }
    }

    // Update which cluster the pvs culling position is in
    void BSP::updateCluster(Camera& c)
    {
        int node = 0;

        while(node >= 0)
        {
            vec3f p = c.pvsPosition();
            BSPNode *n = &m_bspNodes[node];
            BSPPlane *pl = &(m_bspPlanes[m_bspNodes[node].plane]);
            
            node = dotproduct(m_bspPlanes[m_bspNodes[node].plane].normal, c.pvsPosition()) - m_bspPlanes[m_bspNodes[node].plane].dist < 0 ?
                m_bspNodes[node].back :
                m_bspNodes[node].front;
        }

        if(m_bspLeafs[~node].contains(c.pvsPosition()))
            m_currentCluster = m_bspLeafs[~node].cluster;
        else
            m_currentCluster = -1;
    }

    // Draw a face by setting up the textures and uniform variables, then render elements
    void BSP::drawFace( int i )
    {
        if(m_lastFaceIterations[i] == drawIteration)
            return;
        m_lastFaceIterations[i] = drawIteration;

        m_textureIndex = 0;
        m_lightmapIndex = 1;

        if(m_bspFaces[i].lightmapID >= 0)
            m_renderables[0]->m_shader = m_lightmapShader;
        else
            m_renderables[0]->m_shader = m_defaultShader;

        m_renderables[0]->setUniformVariable(0, &m_mvpMatrix);
        //m_renderables[0]->setUniformVariable(1, &m_lvpMatrix);
        m_renderables[0]->setUniformVariable(1, &m_lightPos);
        
        m_renderables[0]->addTexture(m_textures[m_bspFaces[i].textureID]);
        if(m_bspFaces[i].lightmapID >= 0)
            m_renderables[0]->addTexture(m_lightmaps[m_bspFaces[i].lightmapID]);
        //m_renderables[0]->addTexture(m_shadowMap);

        m_renderables[0]->renderElements(m_bspFaces[i].startVertIndex, m_bspFaces[i].numOfVerts,
                                     m_bspFaces[i].startIndex, m_bspFaces[i].numOfIndices);

        m_renderables[0]->m_textures.clear();
    }

    void BSP::objectShadowRender( )
    {
        m_shadowRenderables[0]->setUniformVariable( 0, &m_lvpMatrix.m);
        m_shadowRenderables[0]->setUniformVariable( 1, &m_lightPos);

        for(uint i = 0; i<m_bspFaces.size(); ++i)
        {
            m_shadowRenderables[0]->renderElements(m_bspFaces[i].startVertIndex, m_bspFaces[i].numOfVerts,
                                               m_bspFaces[i].startIndex, m_bspFaces[i].numOfIndices);
        }
    }

    // Clean up all those allocated textures and visibility data
    BSP::~BSP()
    {
        m_bspTextures.clear();
        for(unsigned int i=0; i<m_bspLumps[I_TEXTURES].length / sizeof(BSPTexture); ++i)
            delete m_textures[i];
        delete [] m_textures;
        for(unsigned int i=0; i<m_bspLumps[I_LIGHTMAPS].length / sizeof(BSPLightmap); ++i)
            delete m_lightmaps[i];
        delete [] m_lightmaps;
        delete [] m_lastFaceIterations;
        delete [] m_bspVisData.pVecs;
        delete m_shadowMap;
    }
}