#include "af_rndrr.h"
#include "af_obj.h"

namespace af
{
    //std::vector<Renderable *> Renderer::s_renderables;
    //std::map<std::string, unsigned int> Renderer::s_
    std::vector<Object *> Renderer::s_objects;
    std::map<std::string, unsigned> Renderer::s_objectmap;
    std::vector<Object *> Renderer::s_shadowObjects;

    // Initialize from AFengine.ini
    void Renderer::initialize( HWND hWnd, RECT &clientRect )
    {
        // Parse only field in AFengine.ini
        char buffer[25];
        char filePath[200];
        GetFullPathNameA("AFengine.ini", 200, filePath, NULL);
        GetPrivateProfileStringA("Renderer", "API", "GL", buffer, 25, filePath);

        // Create a GL or DX renderer
        if(strcmp(buffer, "GL")==0)
            af::createRendererGL(hWnd, clientRect);    // Should break with advanced DX effects
        else
            setInstance(af::createRendererDX(hWnd, clientRect));    // Should break with AFImporter

        getInstance().myBar = TwNewBar("Height Constant");
    }

    // render renderables by default if active, and render objects using their objectRender function
    void Renderer::render()
    {
        for(unsigned i=0; i<s_objects.size(); ++i)
            if(s_objects[i]->active())
                s_objects[i]->render();
    }

    // Add a renderable / object
    /*void Renderer::addRenderable(Renderable *pRenderable)
    {
        s_renderables.push_back(pRenderable);
    }*/
    void Renderer::addObject(Object *pObject)
    {
		//printf("Object %s added\n", pObject->m_name.c_str());
        if(pObject->name().length() > 0)
            s_objectmap[pObject->name()] = unsigned(s_objects.size());
        s_objects.push_back(pObject);
    }
    void Renderer::addShadow(Object *pObject)
    {
        s_shadowObjects.push_back(pObject);
    }

    // destroy renderable objects generated (recursively destroys textures)
    Renderer::~Renderer()
    {
        /*std::for_each(s_renderables.begin(), s_renderables.end(), [&](Renderable *pRenderable)
            { delete pRenderable; });*/
    }
}