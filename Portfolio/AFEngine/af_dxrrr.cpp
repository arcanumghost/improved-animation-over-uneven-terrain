#include "af_rndrr.h"
#include "af_obj.h"

namespace af
{
    struct BufferVertex
    {
        vec3f position;
        vec2f texCoord;
    };

    // DX version of the renderer
    class DXRenderer : public Renderer
    {
        // All DX overarching variables
        IDXGISwapChain *m_swapchain;
        ID3D11Device *m_device;
        ID3D11DeviceContext *m_context;
        ID3D11RenderTargetView *m_rtview;
        ID3D11DepthStencilView *m_dsview;
        ID3D11RasterizerState *m_raststate;
        D3D11_VIEWPORT m_viewport;

        // Shadowmap variables
        ID3D11Texture2D *m_smtexture;
        ID3D11RenderTargetView *m_smrtview;
        ID3D11ShaderResourceView *m_smresourceview;
        ID3D11DepthStencilView *m_smdsview;
        D3D11_VIEWPORT m_smviewport;

        // Post-processing variables
        ID3D11Texture2D *m_pptexture;
        ID3D11RenderTargetView *m_pprtview;
        Shader *m_ppshader;
        ID3D11Buffer *m_ppIBO;
        ID3D11Buffer *m_ppVBO;
        ID3D11ShaderResourceView *m_ppresourceview;

    public:
        // Set up DX Renderer
        DXRenderer(HWND hWnd, RECT &clientRect)
        {
            DXGI_SWAP_CHAIN_DESC sd =
            {
                { clientRect.right, clientRect.bottom, { 60, 1 }, DXGI_FORMAT_R8G8B8A8_UNORM, 
                    DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED, DXGI_MODE_SCALING_UNSPECIFIED },
                { 1, 0 },
                DXGI_USAGE_RENDER_TARGET_OUTPUT,
                1,
                hWnd,
                true,
                DXGI_SWAP_EFFECT_DISCARD,
                0
            };

            D3D_FEATURE_LEVEL featurelevels[] = 
            {
                D3D_FEATURE_LEVEL_11_0,
                D3D_FEATURE_LEVEL_10_1,
                D3D_FEATURE_LEVEL_10_0,
            };

            D3D11_TEXTURE2D_DESC desc =
            {
                clientRect.right, clientRect.bottom,
                1, 1,
                DXGI_FORMAT_D32_FLOAT,
                { 1, 0 },
                D3D11_USAGE_DEFAULT,
                D3D11_BIND_DEPTH_STENCIL,
                0, 0
            };

            D3D11_TEXTURE2D_DESC smdesc =
            {
                1024, 1024,
                1, 1,
                DXGI_FORMAT_D32_FLOAT,
                { 1, 0 },
                D3D11_USAGE_DEFAULT,
                D3D11_BIND_DEPTH_STENCIL,
                0, 0
            };

            D3D_FEATURE_LEVEL featurelevelpicked;

            unsigned int createdeviceflags = 0;
            #ifdef _DEBUG
                createdeviceflags |= D3D11_CREATE_DEVICE_DEBUG;
            #endif

            HV(D3D11CreateDeviceAndSwapChain(0, D3D_DRIVER_TYPE_HARDWARE, 0, createdeviceflags, featurelevels, sizeof(featurelevels)/sizeof(D3D_FEATURE_LEVEL), D3D11_SDK_VERSION, &sd, &m_swapchain, &m_device, &featurelevelpicked, &m_context));

            // Set up backbuffer and depth stencil views
            ID3D11Texture2D *backbuffer;
            HV(m_swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void **)&backbuffer));
            HV(m_device->CreateRenderTargetView(backbuffer, 0, &m_rtview));
            HV(backbuffer->Release());

            ID3D11Texture2D *targetTex = 0;
            HV(m_device->CreateTexture2D(&desc, 0, &targetTex));
            HV(m_device->CreateDepthStencilView(targetTex, 0, &m_dsview));
            targetTex->Release();
            // m_context->OMSetRenderTargets(1, &m_rtview, m_dsview);

            // Set up shadowmap variables
            ID3D11Texture2D *smTargetTex = 0;
            HV(m_device->CreateTexture2D(&smdesc, 0, &smTargetTex));
            HV(m_device->CreateDepthStencilView(smTargetTex, 0, &m_smdsview));
            smTargetTex->Release();

            m_smtexture = 0;
            smdesc.Format = DXGI_FORMAT_R32_FLOAT;
            smdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
            HV(m_device->CreateTexture2D(&smdesc, 0, &m_smtexture));
            HV(m_device->CreateRenderTargetView(m_smtexture, 0, &m_smrtview));
            HV(m_device->CreateShaderResourceView(m_smtexture, NULL, &m_smresourceview));

            // Set up post-processing variables
            m_pptexture = 0;
            D3D11_TEXTURE2D_DESC ppdesc =
            {
                clientRect.right, clientRect.bottom,
                1, 1,
                DXGI_FORMAT_R8G8B8A8_UNORM,
                { 1, 0 },
                D3D11_USAGE_DEFAULT,
                D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET,
                0, 0
            };
            HV(m_device->CreateTexture2D(&ppdesc, 0, &m_pptexture));
            HV(m_device->CreateRenderTargetView(m_pptexture, 0, &m_pprtview));
            HV(m_device->CreateShaderResourceView(m_pptexture, NULL, &m_ppresourceview));

            // Create post-processing shader and renderable
            m_ppshader = createShader("shaders\\postproc");

            std::vector<BufferVertex> vBuffer;
            vBuffer.resize(4);
            vBuffer[0].position = vec3f(-1,-1,0);   vBuffer[0].texCoord = vec2f(0.0f, 1.0f);
            vBuffer[1].position = vec3f( 1,-1,0);   vBuffer[1].texCoord = vec2f(1.0f, 1.0f);
            vBuffer[2].position = vec3f(-1, 1,0);   vBuffer[2].texCoord = vec2f(0.0f, 0.0f);
            vBuffer[3].position = vec3f( 1, 1,0);   vBuffer[3].texCoord = vec2f(1.0f, 0.0f);
            std::vector<uint> iBuffer;
            iBuffer.push_back(0);   iBuffer.push_back(1);   iBuffer.push_back(2);
            iBuffer.push_back(2);   iBuffer.push_back(1);   iBuffer.push_back(3);

            D3D11_SUBRESOURCE_DATA srd = { &vBuffer[0], 0, 0 };
            D3D11_SUBRESOURCE_DATA srdi = { &iBuffer[0], 0, 0 };

            HV(m_device->CreateBuffer(&CD3D11_BUFFER_DESC(unsigned(iBuffer.size())*sizeof(uint), D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_IMMUTABLE), &srdi, &m_ppIBO));
            HV(m_device->CreateBuffer(&CD3D11_BUFFER_DESC(unsigned(vBuffer.size())*sizeof(BufferVertex), D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_IMMUTABLE), &srd, &m_ppVBO));
            
            // Set the viewport
            D3D11_RASTERIZER_DESC rasterizerdesc = { D3D11_FILL_SOLID, D3D11_CULL_BACK, true, 0, 0, 0, true, false, false, false };
            HV(m_device->CreateRasterizerState(&rasterizerdesc, &m_raststate));
            m_context->RSSetState(m_raststate);

            D3D11_VIEWPORT viewport = {0, 0, (FLOAT)clientRect.right, (FLOAT)clientRect.bottom, 0, 1};
            m_viewport = viewport;
            m_smviewport = viewport;
            m_smviewport.Width = m_smviewport.Height = 1024;
        }

        // Release DX variables
        ~DXRenderer()
        {
            m_raststate->Release();
            m_dsview->Release();
            m_rtview->Release();
            m_context->Release();
            m_device->Release();
            m_swapchain->Release();

            m_smdsview->Release();
            m_smresourceview->Release();
            m_smrtview->Release();
            m_smtexture->Release();

            m_ppIBO->Release();
            m_ppVBO->Release();
            m_ppresourceview->Release();
            m_pprtview->Release();
            m_pptexture->Release();
            delete m_ppshader;
        }

        void renderShadowmap()
        {
            std::for_each(s_shadowObjects.begin(), s_shadowObjects.end(), [&](Object *o)
            {
                o->objectShadowRender();
            });
        }

        // Makes the shadowmap resource view available to 
        // should be abstracted when shadowmap functionality exists in GL
        ID3D11ShaderResourceView *getShadowMap() 
        {
            return m_smresourceview;
        }

        // Clear screen render, and present
        void render()
        {
            // Set up targets and viewport for depth texture.
            m_context->OMSetRenderTargets(1, &m_smrtview, m_smdsview);
            m_context->RSSetViewports(1, &m_smviewport);
            m_raststate->Release();
            D3D11_RASTERIZER_DESC rasterizerdesc = { D3D11_FILL_SOLID, D3D11_CULL_BACK, true, 0, 0, 0, true, false, false, false };
            HV(m_device->CreateRasterizerState(&rasterizerdesc, &m_raststate));
            m_context->RSSetState(m_raststate);

            // Clear the shadowmap and shadowmap depth stencil view
            float cleardepthcolor[] = {10000.0f, 0, 0, 0};
            m_context->ClearRenderTargetView(m_smrtview, cleardepthcolor);
            m_context->ClearDepthStencilView(m_smdsview, D3D11_CLEAR_DEPTH, 1.0f, 0);

            // Render shadowmap
            renderShadowmap();

            // Set up rendering for the post-processing render target
            // Main rendering pass
            m_context->OMSetRenderTargets(1, &m_pprtview, m_dsview);
            m_context->RSSetViewports(1, &m_viewport);
            m_raststate->Release();
            D3D11_RASTERIZER_DESC rasterizerdesc2 = { D3D11_FILL_SOLID, D3D11_CULL_BACK, true, 0, 0, 0, true, false, false, false };
            HV(m_device->CreateRasterizerState(&rasterizerdesc2, &m_raststate));
            m_context->RSSetState(m_raststate);

            float clearcolor[] = {0.1f, 0.5f, 0.0f, 1.0f};
            m_context->ClearRenderTargetView(m_pprtview, clearcolor);
            m_context->ClearDepthStencilView(m_dsview, D3D11_CLEAR_DEPTH, 1.0f, 0);

            Renderer::render();

            m_context->OMSetRenderTargets(1, &m_rtview, m_dsview);

            uint stride = m_ppshader->stride();
            uint offset = 0;

            m_context->IASetVertexBuffers(0, 1, &m_ppVBO, &stride, &offset);
            m_context->IASetIndexBuffer(m_ppIBO, DXGI_FORMAT_R32_UINT, 0);
            m_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
            
            ((DXShaderUniform *)m_ppshader->m_uniforms[0])->m_effectvariable->AsShaderResource()->SetResource(m_ppresourceview);
            //((DXShaderUniform *)m_ppshader->m_uniforms[0])->m_effectvariable->AsShaderResource()->SetResource(m_smresourceview);
            ((DXShader *)m_ppshader)->makeActiveShader(m_context);

            m_context->DrawIndexed(6, 0, 0);

            ((DXShaderUniform *)m_ppshader->m_uniforms[0])->m_effectvariable->AsShaderResource()->SetResource(NULL);
            m_context->IASetInputLayout(NULL);
            ((DXShader *)m_ppshader)->m_pass->Apply(0, m_context);

            m_swapchain->Present( 0, 0 );
        }

        // create a DX renderable
        Renderable *createRenderable(float *vBuffer, uint nVertices, uint vertexSize, std::vector<uint> &iBuffer, Shader *shader)
        {
            DXRenderable *pdxrenderable = new DXRenderable(m_device, m_context);
            pdxrenderable->init(vBuffer, nVertices, vertexSize, iBuffer, shader);
            
            //addRenderable(pdxrenderable);

            return pdxrenderable;
        }

        // create a DX shader
        Shader *createShader(std::string filename)
        {
            std::wstring wfilename(filename.length(), L'');
            std::copy(filename.begin(), filename.end(), wfilename.begin());
            return new DXShader(wfilename.append(L".fx").c_str(), m_device);
        }

        // create a dx texture
        Texture *createTexture()
        {
            Texture *t = new DXTexture();
            ((DXTexture *)t)->setDevice(m_device);
            return t;
        }

        // create a dx texture from memory
        Texture *createTexture(uint width, uint height, uint *pRGBATexels, bool useAlpha)
        {
            Texture *t = new DXTexture();
            ((DXTexture *)t)->setDevice(m_device);
            t->loadFromMemory(width, height, pRGBATexels, useAlpha);
            return t;
        }
    };

    // Create a DXRenderer for Renderer
    Renderer *createRendererDX(HWND hWnd, RECT &clientRect)
    {
        return new DXRenderer(hWnd, clientRect);
    }
}