/****************************************
 * Quaternion class
 * By Will Perone
 * Original: 12-09-2003  
 * Revised:  27-09-2003
 *           22-11-2003
 *           10-12-2003
 *           15-01-2004
 *           16-04-2004
 *           29-07-2011   added corrections from website,
 *           22-12-2011   added correction to *= operator, thanks Steve Rogers
 * 
 * Notes:  
 * if |q|=1 then q is a unit quaternion
 * if q=(0,v) then q is a pure quaternion 
 * if |q|=1 then q conjugate = q inverse
 * if |q|=1 then q= [cos(angle), u*sin(angle)] where u is a unit vector 
 * q and -q represent the same rotation 
 * q*q.conjugate = (q.length_squared, 0) 
 * ln(cos(theta),sin(theta)*v)= ln(e^(theta*v))= (0, theta*v)
 ****************************************/

#ifndef _AF_QUATERNION_H_
#define _AF_QUATERNION_H_

#include "af_matrx.h"
#include "af_vec.h"
#include "assert.h" 

namespace af
{
    struct quat
    {
	    union {
		    struct {
				vec3f v; //!< the imaginary components
				float s; //!< the real component
		    };
		    struct { float elem[4]; }; //! the raw elements of the quaternion
	    };


	    //! constructors
	    quat() : s(0), v(0,0,0) {}
	    quat(float real, float x, float y, float z): s(real), v(x,y,z) {}
	    quat(float real, const vec3f &i): s(real), v(i) {}

	    //! from 3 euler angles
	    quat(float theta_z, float theta_y, float theta_x)
	    {
		    float cos_z_2 = cosf(0.5f*theta_z);
		    float cos_y_2 = cosf(0.5f*theta_y);
		    float cos_x_2 = cosf(0.5f*theta_x);

		    float sin_z_2 = sinf(0.5f*theta_z);
		    float sin_y_2 = sinf(0.5f*theta_y);
		    float sin_x_2 = sinf(0.5f*theta_x);

		    // and now compute quaternion
		    s   = cos_z_2*cos_y_2*cos_x_2 + sin_z_2*sin_y_2*sin_x_2;
		    v.x = cos_z_2*cos_y_2*sin_x_2 - sin_z_2*sin_y_2*cos_x_2;
		    v.y = cos_z_2*sin_y_2*cos_x_2 + sin_z_2*cos_y_2*sin_x_2;
		    v.z = sin_z_2*cos_y_2*cos_x_2 - cos_z_2*sin_y_2*sin_x_2;

	    }
	
	    //! from 3 euler angles 
	    quat(const vec3f &angles)
	    {	
		    float cos_z_2 = cosf(0.5f*angles.z);
		    float cos_y_2 = cosf(0.5f*angles.y);
		    float cos_x_2 = cosf(0.5f*angles.x);

		    float sin_z_2 = sinf(0.5f*angles.z);
		    float sin_y_2 = sinf(0.5f*angles.y);
		    float sin_x_2 = sinf(0.5f*angles.x);

		    // and now compute quaternion
		    s   = cos_z_2*cos_y_2*cos_x_2 + sin_z_2*sin_y_2*sin_x_2;
		    v.x = cos_z_2*cos_y_2*sin_x_2 - sin_z_2*sin_y_2*cos_x_2;
		    v.y = cos_z_2*sin_y_2*cos_x_2 + sin_z_2*cos_y_2*sin_x_2;
		    v.z = sin_z_2*cos_y_2*cos_x_2 - cos_z_2*sin_y_2*sin_x_2;		
	    } 

        // Construct a quaternion using the 3x3 portion of a matrix4x4f
        // This math is sourced from software.intel.com/file/37726
    private:
        float recipsqrt(float x)
        {
            long i;
            float y, r;

            y = x * 0.5f;
            i = *(long *)( &x );
            i = 0x5f3759df - ( i >> 1 );
            r = *(float *)( &i );
            r = r * ( 1.5f - r * r * y );
            return r;
        }
    public:
        quat(const matrix4x4f &mat)
        {
            const float *m = mat.m;
            float tr = m[0] + m[5] + m[10];

            float m00(m[0]), m01(m[1]), m02(m[2]),
                  m10(m[4]), m11(m[5]), m12(m[6]),
                  m20(m[8]), m21(m[9]), m22(m[10]);

			/*if(m22==m11 || m11==m00 || m00==m22)
				DebugBreak();*/

            if (tr > 0) { 
				float S = float(sqrt(tr+1.0) * 2); // S=4*s 
				//if(S==0)DebugBreak();
              s = 0.25f * S;
              v.x = (m21 - m12) / S;
              v.y = (m02 - m20) / S; 
              v.z = (m10 - m01) / S; 
            } else if ((m00 > m11)&(m00 > m22)) { 
				float S = float(sqrt(1.0 + m00 - m11 - m22) * 2); // S=4*qx 
				//if(S==0)DebugBreak();
              s = (m21 - m12) / S;
              v.x = 0.25f * S;
              v.y = (m01 + m10) / S; 
              v.z = (m02 + m20) / S; 
            } else if (m11 > m22) { 
				float S = float(sqrt(1.0 + m11 - m00 - m22) * 2); // S=4*qy
				//if(S==0)DebugBreak();
              s = (m02 - m20) / S;
              v.x = (m01 + m10) / S; 
              v.y = 0.25f * S;
              v.z = (m12 + m21) / S; 
			} else { 
				float S = float(sqrt(1.0 + m22 - m00 - m11) * 2); // S=4*qz
				//if(S==0)DebugBreak();
              s = (m10 - m01) / S;
              v.x = (m02 + m20) / S;
              v.y = (m12 + m21) / S;
              v.z = 0.25f * S;
            }

            if(length() != normalized().length())
                normalize();
        }
        /*quat(const matrix4x4f &mat)
        {
            const float *m = mat.m;

            if(m[0] + m[5] + m[10] > 0.0f)
            {
                float t0 = m[0] + m[5] + m[10] + 1.0f;
                float s0 = recipsqrt(t0) * .5f;

                s = s0*t0;
                v.x = (m[1] - m[4])*s0;
                v.y = (m[8] - m[2])*s0;
                v.z = (m[6] - m[9])*s0;
            }
            else if(m[0] > m[5] && m[0] > m[10])
            {
                float t0 = m[0] - m[5] - m[10] + 1.0f;
                float s0 = recipsqrt(t0) * .5f;

                s = s0*t0;
                v.x = (m[1] + m[4])*s0;
                v.y = (m[8] + m[2])*s0;
                v.z = (m[6] - m[9])*s0;
            }
            else if(m[5] > m[10])
            {
                float t0 = - m[0] + m[5] - m[10] + 1.0f;
                float s0 = recipsqrt(t0) * .5f;

                s = s0*t0;
                v.x = (m[1] + m[4])*s0;
                v.y = (m[8] - m[2])*s0;
                v.z = (m[6] + m[9])*s0;
            }
            else
            {
                float t0 = - m[0] - m[5] + m[10] + 1.0f;
                float s0 = recipsqrt(t0) * .5f;

                s = s0*t0;
                v.x = (m[1] - m[4])*s0;
                v.y = (m[8] + m[2])*s0;
                v.z = (m[6] + m[9])*s0;
            }
        }*/

        /*quat(const matrix4x4f &mat)
        {
            const float *m = mat.m;

            if(m[0] + m[5] + m[10] > 0.0f)
            {
                float t0 = m[0] + m[5] + m[10] + 1.0f;
                float s0 = recipsqrt(t0) * .5f;

                s = s0*t0;
                v.x = (m[4] - m[1])*s0;
                v.y = (m[2] - m[8])*s0;
                v.z = (m[9] - m[6])*s0;
            }
            else if(m[0] > m[5] && m[0] > m[10])
            {
                float t0 = m[0] - m[5] - m[10] + 1.0f;
                float s0 = recipsqrt(t0) * .5f;

                s = s0*t0;
                v.x = (m[4] + m[1])*s0;
                v.y = (m[2] + m[8])*s0;
                v.z = (m[9] - m[6])*s0;
            }
            else if(m[5] > m[10])
            {
                float t0 = - m[0] + m[5] - m[10] + 1.0f;
                float s0 = recipsqrt(t0) * .5f;

                s = s0*t0;
                v.x = (m[4] + m[1])*s0;
                v.y = (m[2] - m[8])*s0;
                v.z = (m[9] + m[6])*s0;
            }
            else
            {
                float t0 = - m[0] - m[5] + m[10] + 1.0f;
                float s0 = recipsqrt(t0) * .5f;

                s = s0*t0;
                v.x = (m[4] - m[1])*s0;
                v.y = (m[2] + m[8])*s0;
                v.z = (m[9] + m[6])*s0;
            }
        }*/
		
	    // basic operations
	    quat &operator =(const quat &q)		
	    { s= q.s; v= q.v; return *this; }

	    const quat operator +(const quat &q) const	
	    { return quat(s+q.s, v+q.v); }

	    const quat operator -(const quat &q) const	
	    { return quat(s-q.s, v-q.v); }

	    const quat operator *(const quat &q) const	
	    {	
            return quat(s*q.s - dotproduct(v, q.v),
				      v.y*q.v.z - v.z*q.v.y + s*q.v.x + v.x*q.s,
				      v.z*q.v.x - v.x*q.v.z + s*q.v.y + v.y*q.s,
				      v.x*q.v.y - v.y*q.v.x + s*q.v.z + v.z*q.s);
	    }

	    const quat operator /(const quat &q) const	
	    {
		    quat p(q); 
		    p.invert(); 
		    return *this * p;
	    }

	    const quat operator *(float scale) const
	    { return quat(s*scale,v*scale); }

	    const quat operator /(float scale) const
	    { return quat(s/scale,v/scale); }

	    const quat operator -() const
	    { return quat(-s, -v); }
	
	    const quat &operator +=(const quat &q)		
	    { v+=q.v; s+=q.s; return *this; }

	    const quat &operator -=(const quat &q)		
	    { v-=q.v; s-=q.s; return *this; }

	    const quat &operator *=(const quat &q)		
	    {			
		    float x= v.x, y= v.y, z= v.z, sn= s*q.s - dotproduct(v,q.v);
		    v.x= y*q.v.z - z*q.v.y + s*q.v.x + x*q.s;
		    v.y= z*q.v.x - x*q.v.z + s*q.v.y + y*q.s;
		    v.z= x*q.v.y - y*q.v.x + s*q.v.z + z*q.s;
		    s= sn;
		    return *this;
	    }
	
	    const quat &operator *= (float scale)			
	    { v*=scale; s*=scale; return *this; }

	    const quat &operator /= (float scale)			
	    { v/=scale; s/=scale; return *this; }
	

	    //! gets the length of this quaternion
	    float length() const
	    { return (float)sqrt(s*s + dotproduct(v,v)); }

	    //! gets the squared length of this quaternion
	    float length_squared() const
	    { return (float)(s*s + v.dist()); }

	    //! normalizes this quaternion
	    void normalize()
	    { *this/=length(); }

	    //! returns the normalized version of this quaternion
	    quat normalized() const
	    { return  *this/length(); }

	    //! computes the conjugate of this quaternion
	    void conjugate()
	    { v=-v; }

	    //! inverts this quaternion
	    void invert()
	    { conjugate(); *this/=length_squared(); }
	
	    //! returns the logarithm of a quaternion = v*a where q = [cos(a),v*sin(a)]
	    quat log() const
	    {
		    float a = (float)acos(s);
		    float sina = (float)sin(a);
		    quat ret;

		    ret.s = 0;
		    if (sina > 0)
		    {
			    ret.v.x = a*v.x/sina;
			    ret.v.y = a*v.y/sina;
			    ret.v.z = a*v.z/sina;
		    } else {
			    ret.v.x= ret.v.y= ret.v.z= 0;
		    }
		    return ret;
	    }

	    //! returns e^quaternion = exp(v*a) = [cos(a),vsin(a)]
	    quat exp() const
	    {
		    float a = (float)v.dist();
		    float sina = (float)sin(a);
		    float cosa = (float)cos(a);
		    quat ret;

		    ret.s = cosa;
		    if (a > 0)
		    {
			    ret.v.x = sina * v.x / a;
			    ret.v.y = sina * v.y / a;
			    ret.v.z = sina * v.z / a;
		    } else {
			    ret.v.x = ret.v.y = ret.v.z = 0;
		    }
		    return ret;
	    }

	    //! casting to a 4x4 isomorphic matrix for right multiplication with vector
	    matrix4x4f mat4(vec3f tran, vec3f scale) const
		{	
			/*if(scale.x < 0 || scale.y < 0 || scale.z < 0)
				DebugBreak();*/
			//scale = vec3f(1.0, 1.0, 1.0);
			matrix4x4f ts = matrix(scale.x, 0, 0, 0,
						  0, scale.y, 0, 0,
						  0, 0, scale.z, 0,
						  0, 0, 0, 1);
		    matrix4x4f tm = matrix((1-2*(v.y*v.y+v.z*v.z)), 2*(v.x*v.y-s*v.z), 2*(v.x*v.z+s*v.y), 0,  
				          2*(v.x*v.y+s*v.z), (1-2*(v.x*v.x+v.z*v.z)), 2*(v.y*v.z-s*v.x), 0,
				          2*(v.x*v.z-s*v.y), 2*(v.y*v.z+s*v.x), (1-2*(v.x*v.x+v.y*v.y)), 0,
                          tran.x, tran.y, tran.z, 1);
			return ts*tm;
	    }
	
	    //! casting to 3x3 rotation matrix
	    /*operator matrix3() const
	    {
		    Assert(length() > 0.9999 && length() < 1.0001, "quaternion is not normalized");		
		    return matrix3(1-2*(v.y*v.y+v.z*v.z), 2*(v.x*v.y-s*v.z),   2*(v.x*v.z+s*v.y),   
				    2*(v.x*v.y+s*v.z),   1-2*(v.x*v.x+v.z*v.z), 2*(v.y*v.z-s*v.x),   
				    2*(v.x*v.z-s*v.y),   2*(v.y*v.z+s*v.x),   1-2*(v.x*v.x+v.y*v.y));
	    }*/

	    //! computes the dot product of 2 quaternions
	    static inline float dot(const quat &q1, const quat &q2) 
	    { return dotproduct(q1.v,q2.v) + q1.s*q2.s; }

	    //! linear quaternion interpolation
	    static quat lerp(const quat &q1, const quat &q2, float t) 
	    { return (q1*(1-t) + q2*t).normalized(); }

	    //! spherical linear interpolation
	    static quat slerp(const quat &q1, const quat &q2, float t) 
	    {
		    quat q3;
		    float dot = quat::dot(q1, q2);

		    /*	dot = cos(theta)
			    if (dot < 0), q1 and q2 are more than 90 degrees apart,
			    so we can invert one to reduce spinning	*/
		    if (dot < 0)
		    {
			    dot = -dot;
			    q3 = -q2;
		    } else q3 = q2;
		
		    if (dot < 0.95f)
		    {
			    float angle = acosf(dot);
			    return (q1*sinf(angle*(1-t)) + q3*sinf(angle*t))/sinf(angle);
		    } else // if the angle is small, use linear interpolation								
			    return lerp(q1,q3,t);		
	    }

	    //! This version of slerp, used by squad, does not check for theta > 90.
	    static quat slerpNoInvert(const quat &q1, const quat &q2, float t) 
	    {
		    float dot = quat::dot(q1, q2);

		    if (dot > -0.95f && dot < 0.95f)
		    {
			    float angle = acosf(dot);			
			    return (q1*sinf(angle*(1-t)) + q2*sinf(angle*t))/sinf(angle);
		    } else  // if the angle is small, use linear interpolation								
			    return lerp(q1,q2,t);			
	    }

	    //! spherical cubic interpolation
	    static quat squad(const quat &q1,const quat &q2,const quat &a,const quat &b,float t)
	    {
		    quat c= slerpNoInvert(q1,q2,t),
			           d= slerpNoInvert(a,b,t);		
		    return slerpNoInvert(c,d,2*t*(1-t));
	    }

	    //! Shoemake-Bezier interpolation using De Castlejau algorithm
	    static quat bezier(const quat &q1,const quat &q2,const quat &a,const quat &b,float t)
	    {
		    // level 1
		    quat q11= slerpNoInvert(q1,a,t),
				    q12= slerpNoInvert(a,b,t),
				    q13= slerpNoInvert(b,q2,t);		
		    // level 2 and 3
		    return slerpNoInvert(slerpNoInvert(q11,q12,t), slerpNoInvert(q12,q13,t), t);
	    }

	    //! Given 3 quaternions, qn-1,qn and qn+1, calculate a control point to be used in spline interpolation
	    static quat spline(const quat &qnm1,const quat &qn,const quat &qnp1)
	    {
		    quat qni(qn.s, -qn.v);	
		    return qn * (( (qni*qnm1).log()+(qni*qnp1).log() )/-4).exp();
	    }

	    //! converts from a normalized axis - angle pair rotation to a quaternion
	    static inline quat from_axis_angle(const vec3f &axis, float angle)
	    { return quat(cosf(angle/2), axis*sinf(angle/2)); }

	    //! returns the axis and angle of this unit quaternion
	    void to_axis_angle(vec3f &axis, float &angle) const
	    {
		    angle = acosf(s);

		    // pre-compute to save time
		    float sinf_theta_inv = 1.0f/sinf(angle);

		    // now the vector
		    axis.x = v.x*sinf_theta_inv;
		    axis.y = v.y*sinf_theta_inv;
		    axis.z = v.z*sinf_theta_inv;

		    // multiply by 2
		    angle*=2;
	    }

	    //! rotates v by this quaternion (quaternion must be unit)
	    vec3f rotate(const vec3f &v)
	    {   
		    quat Vquat(0, v);
		    quat conjugate(*this);
		    conjugate.conjugate();
		    return (*this * Vquat * conjugate).v;
	    }

	    //! returns the euler angles from a rotation quaternion
	    vec3f euler_angles(bool homogenous=true) const
	    {
		    float sqw = s*s;    
		    float sqx = v.x*v.x;    
		    float sqy = v.y*v.y;    
		    float sqz = v.z*v.z;    

		    vec3f euler;
		    if (homogenous) {
			    euler.x = atan2f(2.f * (v.x*v.y + v.z*s), sqx - sqy - sqz + sqw);    		
			    euler.y = asinf(-2.f * (v.x*v.z - v.y*s));
			    euler.z = atan2f(2.f * (v.y*v.z + v.x*s), -sqx - sqy + sqz + sqw);    
		    } else {
			    euler.x = atan2f(2.f * (v.z*v.y + v.x*s), 1 - 2*(sqx + sqy));
			    euler.y = asinf(-2.f * (v.x*v.z - v.y*s));
			    euler.z = atan2f(2.f * (v.x*v.y + v.z*s), 1 - 2*(sqy + sqz));
		    }
		    return euler;
	    }
    };
}

#endif