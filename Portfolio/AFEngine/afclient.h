#pragma once

#include "afpacket.h"

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind/bind.hpp>

using namespace boost::asio::ip;

namespace af
{
	class client
	{
		void handle_send_to(const boost::system::error_code &error,	size_t bytes);
		void handle_receive_from(const boost::system::error_code &error, size_t bytes);

	protected:
		// Use to initialize a client
		client(boost::asio::io_service &_service, const char *ipaddr, short port);
		
		// Override in the subclass if desired.
		virtual void send();
		virtual void receive(size_t bytes) = 0;

	public:
		// Used to send a message to the server
		void start_send();
		void start_receive();

		boost::asio::io_service &service;
		udp::socket sock;
		udp::endpoint serveraddr;
		static packet packetout;
		static packet::packetdata packetin;
	};
}