#include "af_rndrr.h"
#include "af_input.h"
#include "af_blend.h"

namespace af
{
    value Renderer::getIndex(value &name)
    {
        value v;    

        v.m_type = V_NUM;   
        
        if(!s_objectmap.count(name.m_strval->v))
            v.m_numval = -1;
        else
            v.m_numval = float(s_objectmap[name.m_strval->v]);
        
        return v;
    }

    value Renderer::active(value &objindex)
    {
        value v;

        v.m_type = V_NUM;
        v.m_numval = s_objects[unsigned(objindex.m_numval)]->active();

        return v;
    }

    value Renderer::activate(value &objindex)
    {
        s_objects[unsigned(objindex.m_numval)]->activate();

        nullvaluereturn;
    }

    value Renderer::deactivate(value &objindex)
    {
        s_objects[unsigned(objindex.m_numval)]->deactivate();

        nullvaluereturn;
    }
    
    value AnimBlender::nplay(value &animname, value &weight)
    {
        AnimBlender &blender = AnimBlender::getInstance(); 
        if(blender.m_nanims < 8)
        {
            blender.m_anims[blender.m_nanims] = blender.m_animdigest[animname.m_strval->v];
            blender.m_goalweights[blender.m_nanims] = weight.m_numval;
            blender.m_frames[blender.m_nanims] = 0;
            blender.m_nanims++;
        }

        nullvaluereturn;
    }

    value InputController::keystate(value &key)
    {
        value v;
        v.m_type = V_NUM;
        v.m_numval = IsKeyDown(int(key.m_numval));
        return v;
    }
    
    value InputController::keydown(value &key)
    {
        value v;
        v.m_type = V_NUM;
        v.m_numval = IsKeyDown(int(key.m_numval)) && !prevStates[int(key.m_numval)];
        return v;
    }

    value InputController::keyup(value &key)
    {
        value v;
        v.m_type = V_NUM;
        v.m_numval = !IsKeyDown(int(key.m_numval)) && prevStates[int(key.m_numval)];
        return v;
    }
}