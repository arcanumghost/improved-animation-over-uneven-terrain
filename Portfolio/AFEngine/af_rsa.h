#ifndef _AF_RUNE_SCRIPT_ANALYZER_H_
#define _AF_RUNE_SCRIPT_ANALYZER_H_

#include <string>

using namespace std;

namespace af
{
    enum RuneToken 
    {
        TOKEN_END_OF_FILE = 0,      // EOF
        TOKEN_IDENTIFIER,           // Identifier
        TOKEN_ASSIGN,               // Assignment
        TOKEN_STRING,               // Literals
        TOKEN_NUMBER,
        TOKEN_VEC_BEGIN,
        TOKEN_VEC_END,
        TOKEN_ADD,                  // Operators
        TOKEN_SUB,
        TOKEN_MUL,
        TOKEN_DIV,
        TOKEN_EQ,
        TOKEN_NEQ,
        TOKEN_LT,
        TOKEN_GT,
        TOKEN_AND,
        TOKEN_OR,
        TOKEN_NOT,
        TOKEN_BLOCK_BEGIN,          // Syntax flow
        TOKEN_BLOCK_END,
        TOKEN_GROUP_BEGIN,
        TOKEN_GROUP_END,
        TOKEN_SEMICOLON,            
        TOKEN_SEPARATOR,
        TOKEN_IF,                   // Control statements
        TOKEN_ELSE,
        TOKEN_RETURN,
        TOKEN_WHILE,
        TOKEN_MAIN,                 // main function identifier
        TOKEN_ERROR
    };

    class LexicalError
    {
        unsigned int line;
        std::string msg;
    public:
        LexicalError(unsigned int l, const char *s) : line(l), msg(s) {}
        void display();
    };

    class RuneAnalyzer
    {
        RuneToken m_token;
        unsigned int m_line;
        unsigned char c, *current, *code;
        string m_strdata;
        float m_numdata;
        bool m_error;
           
    public:
        RuneAnalyzer(char *filename);

        ~RuneAnalyzer()
        {
            delete [] code;
        }
        
        // Generates next m_token
        void next();
        void printCurrent();

        RuneToken token() { return m_token; }
        unsigned int line() { return m_line; }
        string strdata() { return m_strdata; }
        float numdata() { return m_numdata; }
        bool error() { return m_error; }
    };
}

#endif