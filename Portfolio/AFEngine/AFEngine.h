#pragma once

/*	I'm not really sure why this file is here, but I'm going to use it to write
 *	some notes about the engine and where it needs to go.
 *	
 *	These are some comment tags that I'll be using to standardize work notes
 *      - Should be in all-caps as shown here
 * 
 *	TODO - Features/functionality that need to be completed eventually
 *  BUG  - Bugs are here and need to be fixed
 *  CODE - Places where code organization needs changes
 *  DOC  - Documentation needs
 *  NOTE - Implementation notes, but for myself, not release. Replace with DOC
 *  REF  - A reference to something external
 *  FIX  -
 *  HACK - An implementation that works, but is hacky and ought to be fixed
 */

// CODE This file should not exist, figure out what it is, remove it as a dependency
#include "resource.h"
