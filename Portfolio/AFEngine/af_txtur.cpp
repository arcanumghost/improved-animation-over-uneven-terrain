#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include <sstream>
#include <iostream>

#include "af_gfx.h"
#include "af_txtur.h"
#include "IL/il.h"
#include "IL/ilu.h"

#pragma comment ( lib, "DevIL.lib" )
#pragma comment ( lib, "ILU.lib" )

namespace af
{
	Texture::Texture()
        : m_flippedNormalY( false )
	{
		texture = 0;
	}

    Texture::~Texture() {}

	bool Texture::isLoaded() const
	{
		if( texture == 0)
			return false;
		else
			return true;
	}
	
	std::wstring Texture::getFilePath() const
	{
		return textureFileName;
	}
	
	uint Texture::getWidth() const
	{
		REQUIRES( isLoaded() );
		return textureWidth;
	}

	uint Texture::getHeight() const
	{
		REQUIRES( isLoaded() );
		return textureHeight;
	}

	bool Texture::hasAlpha() const
	{
		REQUIRES( isLoaded() );
		return alpha;
	}

	void Texture::loadFromFile( const wchar_t *szTexturePath, bool flipOrigin, bool flippedNormalY )
	{
		REQUIRES( !isLoaded() );
		REQUIRES( szTexturePath );
		REQUIRES( sizeof( szTexturePath ) > 0 );

		ilInit();

		ILuint idImage = ilGenImage();
		ilBindImage( idImage );
		ilLoadImage( szTexturePath );

		// Check if Image has loaded
		//
        ILenum err = ilGetError();
 		if( err != IL_NO_ERROR )
        {
            std::wstringstream stringBuilder; 
			stringBuilder << L"Texture \"" << szTexturePath << L"\" failed to load.\n";
            char writeablestring[256];
            size_t nconverted;
            wcstombs_s(&nconverted, writeablestring, stringBuilder.str().c_str(), 256);
            //std::cout << writeablestring;
            do
	        {
                //const wchar_t *errormessage = iluErrorString(err);
			    stringBuilder << err << L": \n";// << errormessage;
                wcstombs_s(&nconverted, writeablestring, stringBuilder.str().c_str(), 256);
                //std::cout << writeablestring;
		    } while((err = ilGetError()) != IL_NO_ERROR);
            //MessageBox( NULL, stringBuilder.str().c_str(), L"Texture Load Error", MB_OK | MB_ICONERROR );
        }

        if( flipOrigin )
        {
            ilEnable( IL_ORIGIN_SET );
            ilOriginFunc( IL_ORIGIN_UPPER_LEFT );
        }
        else
        {
            ilEnable( IL_ORIGIN_SET );
            ilOriginFunc( IL_ORIGIN_LOWER_LEFT );
        }

		textureFileName = szTexturePath;

		ILuint width = ilGetInteger( IL_IMAGE_WIDTH );
		ILuint height = ilGetInteger( IL_IMAGE_HEIGHT );
		ILuint type = ilGetInteger( IL_IMAGE_TYPE );
		type = IL_UNSIGNED_BYTE;
		ILuint nChannels = ilGetInteger( IL_IMAGE_CHANNELS );

 		uint *dataBuffer = new uint[ width*height ];
 		ilCopyPixels( 0, 0, 0, width, height, 1, IL_RGBA, type, dataBuffer );

        // flip normals along the y axis for some normal textures, basically negates green
        if(flippedNormalY)
            for(unsigned int i=0; i < width*height; ++i)
                dataBuffer[i] = (dataBuffer[i] & 0xFFFF00FF) | ((~dataBuffer[i]) & 0x0000FF00);

        // virtual load from memory for DX / GL
		loadFromMemory( width, height, dataBuffer, ( nChannels == 4 ) ? true : false );

 		// Delete the DevIL image.
 		//
 		ilDeleteImage( idImage );

		delete [] dataBuffer;
		
		PROMISES( isLoaded() );
		PROMISES( getFilePath() == szTexturePath );
	}

    // helper function for determining binary dimensions
	bool Texture::isPowerOfTwo( uint x ) const
	{
		if( x == 0 )
			return false;

		return ( x != 0 ) && ( x & ( x - 1 )) == 0;
	}

    // get the texture uint for GL textures
	uint Texture::getTextureUint() const
	{
		return texture;
	}

    // GL variant of load from memory
	void GLTexture::loadFromMemory( uint width, uint height, uint *pRGBATexels, bool useAlpha )
	{
		REQUIRES( !isLoaded() );
		REQUIRES( width > 0 );
		REQUIRES( height > 0 );
		REQUIRES( pRGBATexels );

		alpha = useAlpha;

		// Creates an OpenGL texture object
		//
		glGenTextures( 1, &texture );
		assert( texture );

		// Configure the texture object.
		//
		glBindTexture( GL_TEXTURE_2D, texture );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        
        glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

        textureHeight = height;
        textureWidth = width;

		// Load the texture object from memory.
		//
		glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pRGBATexels );

        //glGenerateMipmap( GL_TEXTURE_2D );

		PROMISES( isLoaded() );
	}

    // Delete the GL references to the texture
	GLTexture::~GLTexture()
	{
		glDeleteTextures( 1, (const GLuint*)( &texture ));
	}
    
    // MASK and AverageColor are used to mipmap four pixels down to one
#define MASK(x) ((x & 0xFCFCFCFC) >> 2)
    uint AverageColor( uint a, uint b, uint c, uint d)
    {
        return MASK(a) + MASK(b) + MASK(c) + MASK(d);
    }

    // DX variant of load from memory
    void DXTexture::loadFromMemory( uint width, uint height, uint *pRGBATexels, bool useAlpha )
    {
        // Set texture dimensions
        textureHeight = height;
        textureWidth = width;
        
        // Presently, mipmaps are generated for square textures
        // Count the number of mipmaps needed
        int nMipMaps = 1;
        if(isPowerOfTwo(width) && isPowerOfTwo(height))
            while(width > 1 && height > 1)
            {
                width = max(width / 2, 1);
                height = max(height / 2, 1);
                ++nMipMaps;
            }

        // Create the texture description
        D3D11_TEXTURE2D_DESC desc;
        desc.Width = textureWidth;
        desc.Height = textureHeight;
        desc.MipLevels = nMipMaps;  // 0 for auto-generated mipmaps TODO
        desc.ArraySize = 1;
        desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        desc.SampleDesc.Quality = (desc.SampleDesc.Count = 1)-1;
        desc.Usage = D3D11_USAGE_DEFAULT;
        desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
        desc.CPUAccessFlags = 0;
        desc.MiscFlags = 0;
        ID3D11Texture2D *pTexture = nullptr;

        // Create sub-resource data and mipmaps
        uint **mipmaps = new uint *[nMipMaps];
        mipmaps[0] = pRGBATexels;
        width = textureWidth;
        height = textureHeight;
        int pastwidth, pastheight;
        D3D11_SUBRESOURCE_DATA *data = new D3D11_SUBRESOURCE_DATA[nMipMaps];
        for(int i=0; i<nMipMaps; ++i)
        {
            // generate the mipmap
            if(i)
            {
                mipmaps[i] = new uint[width*height];
                for(int y=0; y<pastheight; y+=2)
                    for(int x=0; x<pastwidth; x+=2)
                        mipmaps[i][width*(y/2) + (x/2)] = AverageColor(mipmaps[i-1][pastwidth*y + x], mipmaps[i-1][pastwidth*(y+1)+x],
                            mipmaps[i-1][pastwidth*y + (x+1)], mipmaps[i-1][pastwidth*(y+1)+(x+1)]);
            }
            data[i].pSysMem = mipmaps[i];
            data[i].SysMemPitch = width*sizeof(uint);
            data[i].SysMemSlicePitch = 0;
            pastwidth = width;
            pastheight = height;
            width = max( width / 2, 1);
            height = max( height / 2, 1);
        }

        // Create the texture
        HV(m_device->CreateTexture2D( &desc, data, &pTexture));
        HV(m_device->CreateShaderResourceView(pTexture, NULL, &m_tex));

        // Release temporary data
        pTexture->Release();
        for(int i=1; i<nMipMaps; ++i)
            delete [] mipmaps[i];
        delete [] mipmaps;
        delete [] data;

        // Set for isLoaded()
        texture = true;
    }

    // Release the texture resource view
    DXTexture::~DXTexture()
    {
        m_tex->Release();
    }
}