#include "af_rndrr.h"
#include "afscreen.h"
#include "af_input.h"

namespace af
{
	void checkError()
	{
		GLenum errorcode = glGetError();
		if(errorcode != GL_NO_ERROR)
			DebugBreak();
	}

    class GLRenderer : public Renderer
    {
        // variables necessary for rendering
        HGLRC m_renderingContext;
        HDC m_hDC;
		GLuint *m_texturebuffers, *m_framebuffers, *m_depthbuffers;
		GLuint m_depthdepthbuffer, m_depthframebuffer, m_depthtexturebuffer;
		GLuint m_atexturebuffer, m_aframebuffer, m_adepthbuffer;

		GLuint m_ssaotexturebuffer, m_ssaoframebuffer, m_ssaodepthbuffer;
		GLuint m_blurtexturebuffer, m_blurframebuffer, m_blurdepthbuffer;

		vec3f SSAOonly;

		vec2i *m_texturesizes;
		vec3f *m_texturedeltas;
		int nbuffers;
		float width, height;
		ScreenObject *m_screenobj, *m_screenobjfinal, *m_adaptivescreenobj;

    public:
		void GLRenderer::negateSSAOOnly(keyeventtype &e)
		{
			SSAOonly.x = 1.0f - SSAOonly.x;
		}

        GLRenderer(HWND hWnd, RECT &clientRect)
			: SSAOonly(1.0f, 1.0f, 1.0f)
        {
            m_hDC = GetDC( hWnd );

            // Set the window pixel format
	        //
	        PIXELFORMATDESCRIPTOR pixelFormatDescriptor = {0};

	        pixelFormatDescriptor.nSize = sizeof( pixelFormatDescriptor );
	        pixelFormatDescriptor.nVersion = 1;

			blah.registerInputFunction(keyeventtype(KEY_PRESS, 'O'), &GLRenderer::negateSSAOOnly, this);

	        pixelFormatDescriptor.dwFlags = 
		        PFD_DRAW_TO_WINDOW | 
		        PFD_SUPPORT_OPENGL | 
		        PFD_DOUBLEBUFFER;
	        pixelFormatDescriptor.dwLayerMask = PFD_MAIN_PLANE;
	        pixelFormatDescriptor.iPixelType = PFD_TYPE_RGBA;
	        pixelFormatDescriptor.cColorBits = 32;
	        pixelFormatDescriptor.cDepthBits = 16;	

	        int pixelFormat = ChoosePixelFormat( m_hDC, &pixelFormatDescriptor );
	        assert (pixelFormat != 0);
	        SetPixelFormat( m_hDC, pixelFormat, &pixelFormatDescriptor );

            // set up the rendering context
            m_renderingContext = wglCreateContext( m_hDC );
            wglMakeCurrent( m_hDC, m_renderingContext );

            // set up culling modes
	        glEnable( GL_CULL_FACE );
	        glCullFace( GL_BACK );
            //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // Useful for seeing polygon draw order
            glEnable( GL_DEPTH_TEST );

			int x = clientRect.right;
			int y = clientRect.bottom;
			nbuffers = 3;

			do
			{
				nbuffers++;
				x/=2;
				y/=2;
			}while(x > 1 && y > 1);
            
            // create a viewport
            glViewport( 0, 0, clientRect.right, clientRect.bottom );

            glewInit();

			//*
			m_texturebuffers = new GLuint[nbuffers];
			m_depthbuffers = new GLuint[nbuffers];
			m_framebuffers = new GLuint[nbuffers];
			m_texturesizes = new vec2i[nbuffers];
			m_texturedeltas = new vec3f[nbuffers];

			// Set up texture
			glGenTextures(nbuffers, m_texturebuffers);
			glGenFramebuffers(nbuffers, m_framebuffers);
			glGenRenderbuffers(nbuffers, m_depthbuffers);

			glGenTextures(1, &m_atexturebuffer);
			glGenFramebuffers(1, &m_aframebuffer);
			glGenRenderbuffers(1, &m_adepthbuffer);

			glGenTextures(1, &m_depthtexturebuffer);
			glGenFramebuffers(1, &m_depthframebuffer);
			glGenRenderbuffers(1, &m_depthdepthbuffer);

			glBindTexture(GL_TEXTURE_2D, m_atexturebuffer);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 1, 1, 0, GL_RGBA, GL_FLOAT, nullptr);

			// Create and attach frame buffer to texture
			glBindFramebuffer(GL_FRAMEBUFFER, m_aframebuffer);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_atexturebuffer, 0);

			// Create and set up render buffer (depth)
			glBindRenderbuffer(GL_RENDERBUFFER, m_adepthbuffer);
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 1, 1);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_adepthbuffer);

			x = clientRect.right;
			y = clientRect.bottom;

			checkError();
			
			for(int i=0; i<nbuffers; ++i)
			{
				if(!i)
				{
					glBindTexture(GL_TEXTURE_2D, m_depthtexturebuffer);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);														
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);														
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);													
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, x, y, 0, GL_RGBA, GL_FLOAT, nullptr);		

					// Create and attach frame buffer to texture
					glBindFramebuffer(GL_FRAMEBUFFER, m_depthframebuffer);
					glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_depthtexturebuffer, 0);

					glBindRenderbuffer(GL_RENDERBUFFER, m_depthdepthbuffer);
					glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, x, y);
					glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_depthdepthbuffer);

					GLenum status;
					status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
					if(status == GL_FRAMEBUFFER_COMPLETE)
						cout << "good " << i << endl;
				}

				/*if(i==2)
				{
					glBindTexture(GL_TEXTURE_2D, m_)
				}*/
				
				glBindTexture(GL_TEXTURE_2D, m_texturebuffers[i]);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, x, y, 0, GL_RGBA, GL_FLOAT, nullptr);

				// Create and attach frame buffer to texture
				glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffers[i]);
				glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_texturebuffers[i], 0);

				/*if(!i)
				{
					// Bind a depth buffer texture to the frame buffer																	 checkError();
					glBindTexture(GL_TEXTURE_2D, m_depthtexturebuffer);																	 checkError();
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);														 checkError();
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);														 checkError();
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);													 checkError();
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);													 checkError();
					glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);												 checkError();
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);									 checkError();
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);													 checkError();
					glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, x, y, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, nullptr);		 checkError();

					glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_COMPONENT, GL_TEXTURE_2D, m_depthtexturebuffer, 0);
				}*/
				{
					// Create and set up render buffer (depth)
					glBindRenderbuffer(GL_RENDERBUFFER, m_depthbuffers[i]);
					glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, x, y);
					glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_depthbuffers[i]);
				}
				
				m_texturesizes[i].x = x;
				m_texturesizes[i].y = y;
				m_texturedeltas[i].x = (1.0f/float(x))/2.0f;
				m_texturedeltas[i].y = (1.0f/float(y))/2.0f;
				if(x > 1)
					x /= 2;
				if(y > 1)
					y /= 2;
				
				// Check the success of setting up a second render target
				GLenum status;
				status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
				if(status == GL_FRAMEBUFFER_COMPLETE)
					cout << "good " << i << endl;
			}

			width = float(clientRect.right);
			height = float(clientRect.bottom);

			glBindFramebuffer( GL_FRAMEBUFFER, 0);//*/

            // Set up AntTweakBar
            TwInit(TW_OPENGL, NULL);
            TwWindowSize(clientRect.right, clientRect.bottom);
        }

		void setupscreenobj()
		{
			Shader *screenshader = createShader("shaders\\screen");
			m_screenobj = new ScreenObject(width, height, screenshader);
			m_screenobj->m_renderables[0]->m_othertextureids.push_back(1);
            //delete screenshader;

			Shader *screenshaderadaptive = createShader("shaders\\adaptive");
			m_adaptivescreenobj = new ScreenObject(1, 1, screenshaderadaptive);
			m_adaptivescreenobj->m_renderables[0]->m_othertextureids.push_back(1);
			m_adaptivescreenobj->m_renderables[0]->m_othertextureids.push_back(1);
            //delete screenshaderadaptive;

			Shader *screenshaderfinal = createShader("shaders\\hdr");
			m_screenobjfinal = new ScreenObject(width, height, screenshaderfinal);
			m_screenobjfinal->m_renderables[0]->m_othertextureids.push_back(1);
			m_screenobjfinal->m_renderables[0]->m_othertextureids.push_back(1);
			m_screenobjfinal->m_renderables[0]->m_othertextureids.push_back(1);
			//m_screenobj->m_renderables[0]->setUniformVariable(0, &m_texturebuffers);
            //delete screenshaderfinal;
		}

        ~GLRenderer()
        {
            //delete m_screenobj->m_renderables[0]->m_shader;
            //delete m_screenobj->m_renderables[0]->m_shader;
            //delete m_screenobj->m_renderables[0]->m_shader;

            delete m_screenobj;
            delete m_adaptivescreenobj;
            delete m_screenobjfinal;
            delete Renderable::s_shadowShader;

            delete [] m_texturebuffers;
            delete [] m_depthbuffers;
            delete [] m_framebuffers;
            delete [] m_texturesizes;
            delete [] m_texturedeltas;

            wglMakeCurrent( NULL, NULL );
            wglDeleteContext( m_renderingContext );
            m_renderingContext = 0;	
        }

		void renderShadowmap()
		{
			std::for_each(Renderer::getInstance().s_objects.begin(), Renderer::getInstance().s_objects.end(), [&](Object *o)
			{
				if(o->active())
				{
					//printf("Shadow render %s\n", o->m_name.c_str());
					o->shadowRender();
				}
			});
		}   

        // clear screens, swap buffer, and ask the base renderer to render objects
        void render()
        {
			//glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffers[0]);

            // Clear Window
            glClearColor(.4f, .4f, .6f, 1.0f);
            glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT );

			//glViewport( 0, 0, m_texturesizes[0].x, m_texturesizes[0].y );

			Renderer::render();

			//glBindFramebuffer(GL_FRAMEBUFFER, m_depthframebuffer);

			/* Code for Rendering only until post processing is separated.
			glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT );

			renderShadowmap();

			glBindFramebuffer(GL_FRAMEBUFFER, 0);//*/

			/*
			glBindTexture(GL_TEXTURE_2D, m_texturebuffers);
			glDisable(GL_BLEND);

			glMatrixMode( GL_PROJECTION );
			glLoadIdentity();
			glOrtho(0.0f, width, 0.0f, height, -1.0, 1.0);
			glMatrixMode( GL_MODELVIEW );
			glLoadIdentity();

			/*glBegin(GL_TRIANGLE_STRIP);
			{
				glTexCoord2f(0.0f, 0.0f);
				glVertex2f(0.0f, 0.0f);
				glTexCoord2f(1.0f, 0.0f);
				glVertex2f(width, 0.0f);
				glTexCoord2f(0.0f, 1.0f);
				glVertex2f(0.0f, height);
				glTexCoord2f(1.0f, 1.0f);
				glVertex2f(width, height);
			}
			glEnd();*/

			//*
			/*for(int i=1; i<nbuffers; ++i)
			{
				glBindFramebuffer(GL_FRAMEBUFFER, m_framebuffers[i]);

				// Clear Window
				glClearColor(.4f, .4f, .6f, 1.0f);
				glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT );

				m_screenobj->m_renderables[0]->m_othertextureids[0] = m_texturebuffers[i-1];
				m_screenobj->m_renderables[0]->setUniformVariable(0, &m_texturedeltas[i]);

				glViewport( 0, 0, m_texturesizes[i].x, m_texturesizes[i].y );

				m_screenobj->render();

				/* Commentable code for visibly drawing buffers in between.
				glBindFramebuffer(GL_FRAMEBUFFER, 0);

				// Clear Window
				glClearColor(.4f, .4f, .6f, 1.0f);
				glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT );

				glViewport( 0, 0, m_texturesizes[0].x, m_texturesizes[0].y );

				m_screenobj->render();

				SwapBuffers( m_hDC );///
			}//*/


			// Adaptive rendering effect
			/*glBindFramebuffer(GL_FRAMEBUFFER, m_aframebuffer);
			glClear( GL_DEPTH_BUFFER_BIT );

			m_adaptivescreenobj->m_renderables[0]->m_othertextureids[0] = m_atexturebuffer;
			m_adaptivescreenobj->m_renderables[0]->m_othertextureids[1] = m_texturebuffers[nbuffers-1];

			m_adaptivescreenobj->render();


			//* Commentable out final draw to screen
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			
			glClearColor(.4f, .4f, .6f, 1.0f);
			glClear( GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT );

			m_screenobjfinal->m_renderables[0]->setUniformVariable(0, &m_projmat);
			m_screenobjfinal->m_renderables[0]->setUniformVariable(1, &SSAOonly);
			m_screenobjfinal->m_renderables[0]->m_othertextureids[0] = m_texturebuffers[0];//
			m_screenobjfinal->m_renderables[0]->m_othertextureids[1] = m_atexturebuffer;//m_texturebuffers[nbuffers-1];
			m_screenobjfinal->m_renderables[0]->m_othertextureids[2] = m_depthtexturebuffer;//

			glViewport( 0, 0, m_texturesizes[0].x, m_texturesizes[0].y );

			m_screenobjfinal->render();//*/
#ifdef _DEBUG
            TwRefreshBar(Renderer::getInstance().myBar);
            TwDraw();
#endif
			SwapBuffers(m_hDC);
        }

        // create a gl renderable object
        Renderable* createRenderable( float *vBuffer, uint nVertices, uint vertexSize, std::vector<uint> &iBuffer, Shader *shader)
        {
            GLRenderable* pglrenderable = new GLRenderable();
            pglrenderable->init(vBuffer, nVertices, vertexSize, iBuffer, shader);

            //addRenderable(pglrenderable);

            return pglrenderable;
        }

        // create a gl shader
        Shader* createShader(std::string filename)
        {
            return new GLShader(std::string(filename).append(".vp").c_str(),  std::string(filename).append(".fp").c_str());
        }

        // create an unloaded gl texture
        Texture *createTexture()
        {
            return new GLTexture();
        }

        // create a gl texture from memory (loads the texture)
        Texture *createTexture( uint width, uint height, uint *pRGBATexels, bool useAlpha )
        {
            Texture *t = new GLTexture();
            t->loadFromMemory(width, height, pRGBATexels, useAlpha);
            return t;
        }
    };

    // creates a GLRenderer for Renderer
    void createRendererGL(HWND hWnd, RECT &clientRect)
    {
        Renderer::setInstance(new GLRenderer(hWnd, clientRect));
		((GLRenderer&)(Renderer::getInstance())).setupscreenobj();
    }
}