#pragma once

// Maybe this file is a silly idea.
// I can put VK_ in front of stuff just fine
namespace af
{
	namespace Keys
	{	
		enum KeyStroke
		{
			CTRL = VK_CONTROL,
			LEFT = VK_LEFT,
			UP = VK_UP,
			RIGHT = VK_RIGHT,
			DOWN = VK_DOWN
		};
	}
}