#pragma once

#include "af_time.h"

namespace af
{
#define MAXPACKETSIZE 8192
	
	enum PacketType
	{
		GUARANTEED, NONGUARANTEED
	};
	
	struct packet
	{
		SystemClocks timestamp;
		unsigned datalength;
		union packetdata
		{
			struct packetheader
			{
				long long sequenceid;
				PacketType ptype;
			} header;
			char raw[MAXPACKETSIZE];
		} data;

		packet();

		// Pushes a new message, returns false on failure (proper response would be to send the current packet,
		// renew, and attempt another pushmsg call)
		bool pushmsg(void *pdata, unsigned size, PacketType ptype);

		// Refreshes this packet for new messages
		void renew();
	};
}