#ifndef _AF_MODEL_H_
#define _AF_MODEL_H_

#include <vector>
#include <map>
#include <fstream>
#include "af_txtur.h"
#include "af_vec.h"
#include "af_matrx.h"
#include "af_obj.h"
#include "afevent.h"
#include "afblendnode.h"
#include "af_axis.h"

namespace af
{
    enum TextureType
    {
        TEXTURE_DIFFUSE     = 0x0001,
        TEXTURE_BUMP        = 0x0002
    };

    enum VertexType
    {
        VERTEX_POSITION             = 0x0001,
        VERTEX_DIFFTEXCOORD         = 0x0002,
        VERTEX_NORMAL               = 0x0004,
        VERTEX_COLOR                = 0x0008,
        VERTEX_TANGENT              = 0x0010,
        VERTEX_LIGHTMAPTEXCOORD     = 0x0020,
        VERTEX_BONEWEIGHT           = 0x0040,
        VERTEX_BONEINDEX            = 0x0080
    };
    
    enum ModelType
    {
        DUMMY,
        MESH,
        SKIN,
        BONE
    };

    // Model Vertex not yet used, RenderableVertex is the base for Mesh renderables
    struct ModelVertex
    {
        vec3f position;
        vec2f texCoord;
        vec3f normal;
    };

    struct AnimModelVertex
    {
        vec3f position;
        vec2f texCoord;
        vec3f normal;
        vec3f weights;
        char boneindices[4];
    };

    // Bounding Sphere
    struct BSphere
    {
        vec3f center;
        float radius;
    };

	class Anim;

    // Models may have multiple meshes and have a bounding sphere used for centering and initial zoom.
    struct Model : public Object
    {
    private:
        // Keep track of meshes, light, and mvpmatrix
        // Child meshes
        std::vector<Model *> m_children;
        // Relative matrix for transforming this object from its parent
        std::vector<matrix4x4f> m_relTranMatrix;
        vec3f m_light;
        std::vector<Model *> *m_boneslist;
        bool m_updateTime;
        float m_time;
        ModelType m_mtype;

        matrix4x4f m_zerotran;
        matrix4x4f m_m_matrix;
        matrix4x4f m_vp_matrix;
        matrix4x4f m_anothertemporarymatrix;

        // Why is this a nested vector? I can't think of a reason for 2D vectors here...
        // NOTE the nested vectors are because of multiple meshes / draw calls, and each mesh / 
        // draw call needs a matrix for each bone and textures for different effects (bump/height)
        std::vector<std::vector<matrix4x4f>> m_meshbonetrans;
        std::vector<std::vector<Texture *>> m_textures;

        BSphere *m_sphere;

        void LoadOBJ(char *path, Shader *_shader);
        void LoadAFM(char *path, Shader *_shader, Shader *_animshader);
        void LoadSubAFM(std::fstream &infile, std::vector<Model *> &models, std::vector<Model *> *boneslist, 
            std::vector<std::vector<Texture *>> &textures, Shader *_shader, Shader *_animshader, matrix4x4f &_zerotran,
            BSphere *sphere);

        void applyframedata(framedata &fd, const matrix4x4f &modelMat);

        Model();

    public:
        vec3f m_translation;
        vec3f m_velocity;
        vec3f m_rotation_goal;
		vec3f m_rotation;
        bool m_useblender;
        bool m_jumping;
        bool m_rootmodel;
		float m_verticalspeed;
        blendnode *m_blendtree;


        std::vector<Axis *> m_boneaxes;
        Shader *m_defaultshader;

        Model(char *path, Shader *_shader, Shader *_animshader = nullptr);
        ~Model();

        void render();
		void shadowRender();
        void update(float time, const matrix4x4f &vp_Mat, const matrix4x4f &modelMat, const vec3f &light, bool useblender = false, Anim *animation = nullptr);
        void negateTimeUpdate() { m_updateTime = !m_updateTime; }

        // turn on and off rendering
        void activate();
        void deactivate();

        void blend();

        void moveXBox(xboxeventdata &e);
    };
}

#endif