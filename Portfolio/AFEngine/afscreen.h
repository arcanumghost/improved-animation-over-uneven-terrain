#pragma once

#include "targetver.h"

#ifdef _WIN32
#include <Windows.h>
#endif

#include "af_gfx.h"
#include "af_obj.h"

namespace af
{
	struct SimpleVertex
	{
		vec3f position;
		vec2f texCoord;
	};

	class ScreenObject : public Object
	{
	public:
		ScreenObject(float width, float height, Shader *shader);
        virtual ~ScreenObject();
	};
}