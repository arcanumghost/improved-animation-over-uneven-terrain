// Copyright 2010 Jeff Wofford.
//
// This file provides the template function TesselateBezierPatches().
// TesselateBezierPatches() can be used to convert Quake 3 BSP faces that are of
// "patch" type to tesselated, renderable triangle faces of "polygon" type.
//
// There is no accompanying cpp file.

#include <cassert>
#include <vector>

namespace jeffwofford
{


#ifdef QUAKE3_BSP_TESSELATION_USE_STOCK_ACCESSORS_AND_OPERATORS

	// These stock functions work without modification on Quake 3 BSP Vertex and Face structs
	// that are identical to those in Quake3Format.htm. 
	//
	// If you are using those structs 
	// without modification (except for the name of the struct--you can change that),
	// then #define QUAKE3_BSP_TESSELATION_USE_STOCK_ACCESSORS_AND_OPERATORS
	// before including this file.
	//
	// If you have changed those structs in some way, such as renaming member data
	// for stylistic reasons, then you will need to provide your own
	// implementations of these functions.

	template< typename VertexT >
	VertexT operator*( const VertexT& lhs, float rhs )
	{
		VertexT vert = lhs;

		vert.vLightmapCoord[0] *= rhs;
		vert.vLightmapCoord[1] *= rhs;
		vert.vPosition[0] *= rhs;
		vert.vPosition[1] *= rhs;
		vert.vPosition[2] *= rhs;
		vert.vTextureCoord[0] *= rhs;
		vert.vTextureCoord[1] *= rhs;
		vert.vNormal[0] *= rhs;
		vert.vNormal[1] *= rhs;
		vert.vNormal[2] *= rhs;

		return vert;
	}

	template< typename VertexT >
	VertexT operator+( const VertexT& lhs, const VertexT& rhs)
	{
		VertexT vert = lhs;

		vert.vLightmapCoord[0]	+= rhs.vLightmapCoord[0];
		vert.vLightmapCoord[1]	+= rhs.vLightmapCoord[1];
		vert.vPosition[0]		+= rhs.vPosition[0];
		vert.vPosition[1]		+= rhs.vPosition[1];
		vert.vPosition[2]		+= rhs.vPosition[2];
		vert.vTextureCoord[0]	+= rhs.vTextureCoord[0];
		vert.vTextureCoord[1]	+= rhs.vTextureCoord[1];
		vert.vNormal[0]			+= rhs.vNormal[0];
		vert.vNormal[1]			+= rhs.vNormal[1];
		vert.vNormal[2]			+= rhs.vNormal[2];

		return vert;
	}

	template< typename BSPFaceT >
	int GetNumPatchesX( const BSPFaceT& face )
	{
		return face.size[ 0 ];
	}

	template< typename BSPFaceT >
	int GetNumPatchesY( const BSPFaceT& face )
	{
		return face.size[ 1 ];
	}

	template< typename BSPFaceT >
	int GetBaseIndex( const BSPFaceT& face )
	{
		return face.startVertIndex;
	}

	template< typename BSPFaceT >
	bool IsBezierPatchFace( const BSPFaceT& face )
	{
		return face.type == 2;						// MAGIC: 2 is defined by the Quake 3 BSP format as the type for patch faces.
	}

	template< typename BSPFaceT >
	void SetupPolygonBSPFace( 
							 BSPFaceT& face, 
							 unsigned int iStartVertex, 
							 unsigned int nVertices, 
							 unsigned int iStartIndex,
							 unsigned int nIndices )
	{
		face.type = 1;								// MAGIC: 1 is defined by the Quake 3 BSP format as the type for polygon faces.
		face.startVertIndex = iStartVertex;
		face.numOfVerts = nVertices;
		face.startIndex = iStartIndex;
		face.numOfIndices = nIndices;
	}

#endif	// QUAKE3_BSP_TESSELATION_USE_STOCK_ACCESSORS_AND_OPERATORS

	// Declaration of function used by TesselateBezierPatches(). No need to call this yourself.
	//
	template< typename VertexT, typename IndexT >
	void TesselateBezierPatch( 
							  const std::vector< VertexT >& inControlPoints,
							  std::vector< VertexT >& outTriangleVertices,
							  std::vector< IndexT >& outTriangleIndices,
							  IndexT baseVertexIndex,
							  IndexT degreeOfTesselation
							  );


	// FUNCTION: void TesselateBezierPatches( ... )
	//
	// Given a set of BSP Faces, a set of vertices, and a degree of tesselation,
	// this function converts all the faces that are of Bezier patch type to a set of renderable
	// triangles. These triangles are stored in terms of new vertices (which are appended to the end of outTriangleVertices),
	// new indices (which are appended to the end of outTriangleIndices),
	// and new or modified faces.
	//
	// Each Bezier patch within the face is divided into a number of subdivisions as determined by degreeOfTesselation.
	// For each subdivision of the patch, a pair of triangles is emitted in the form of new vertices 
	// added to outTriangleVertices and new indices added to outTriangleIndices.
	//
	// outTriangleVertices and inPatchVertices *may *be the same container. 
	// In that case, in effect, the new patch vertices will be appended to the end of inPatchVertices.
	//
	// If pOutPatchFaces is NULL, then each patch-type face in inPatchFaces is modified in-place to reference the 
	// necessary vertex and index data. This is a "destructive" process in the sense that although the original 
	// patch vertices are still stored in inPatchVertices, they are no longer referenced by any face. It would be better
	// to remove them entirely, but this is not done for you.
	//
	// If pOutPatchFaces is non-NULL, then for each patch-type face in inPatchFaces, a new face is appended to the end of *pOutPatchFaces.
	// This new face points to the required renderable triangle vertices and indices, is given "polygon" type, but is otherwise
	// identical to the patch face that begat it.
	//
	// If pOutPatchFaces is non-NULL, then inPatchFaces and *pOutPatchFaces *may *be the same container. 
	// In that case, in effect, the new patch faces will be appended to the end of inPatchFaces.
	//
	// The following support functions must exist:
	//		BSPFaceT::BSPFaceT( const BSPFaceT& );		// Copy constructor for BSPFaceT
	//		VertexT operator+( const VertexT&, const VertexT& );
	//		VertexT operator*( const VertexT&, float );
	//		int GetNumPatchesX( const BSPFaceT& );		// Returning the number of bezier patches within the face in the X direction.
	//		int GetNumPatchesY( const BSPFaceT& );		// Returning the number of bezier patches within the face in the Y direction.
	//		int GetBaseIndex( const SFace& );			// Returns the index of the base index for the face control points within inPatchVertices.
	//		bool IsBezierPatchFace( const SFace& );		// Returning true if the given face in fact contains bezier patches.
	//		void SetupPolygonBSPFace( 
	//			CLevelBSP::SFace& face, 
	//			unsigned int iStartVertex, 
	//			unsigned int nVertices, 
	//			unsigned int iStartIndex,
	//			unsigned int nIndices );				// Sets the face to have polygon type and the specified vertex and index offset/length values.
	//
	// IndexT is expected to be an ordinal type implicitably castable from size_t.
	//
	template< typename BSPFaceT, typename VertexT, typename IndexT >
	void TesselateBezierPatches( 
								std::vector< BSPFaceT >& inPatchFaces,
								const std::vector< VertexT >& inPatchVertices,
								std::vector< VertexT >& outTriangleVertices, 
								std::vector< IndexT >& outTriangleIndices,
								std::vector< BSPFaceT > *pOutPatchFaces = 0,
								IndexT degreeOfTesselation = 5 )
	{
		assert( !inPatchFaces.empty() );			// Gimme some faces!
		assert( !inPatchVertices.empty() );			// Gimme some vertices!

		// Prepare a container for the control points for each Bezier patch.
		// For efficiency, these control points are actually pointers to existing vertices.
		//
		std::vector< VertexT > vecControlPoints( 9 );		// MAGIC: 9 is mathematically fundamental. A Bezier patch, by definition, has 9 control points.

		const size_t nInPatchFaces = inPatchFaces.size();

		for( int iFace = 0; iFace < nInPatchFaces; ++iFace )
		{
			BSPFaceT& face = inPatchFaces[ iFace ];

			// Is this face in fact a Bezier patch? This is the only kind we process.
			//
			if( IsBezierPatchFace( face ))
			{
				const IndexT iStartVertex = IndexT(outTriangleVertices.size());
				const int iStartIndex = int(outTriangleIndices.size());

				// How many Bezier control points are in each direction?
				//
				int sizeX = GetNumPatchesX( face );
				int sizeY = GetNumPatchesY( face );

				assert( sizeX > 0 );
				assert( sizeY > 0 );

				int baseFaceIndex = GetBaseIndex( face );
				assert( baseFaceIndex >= 0 );

				// For each patch in the face...
				//
				for( int y = 0; y < sizeY - 1; y += 2 )
				{
					for( int x = 0; x < sizeX - 1; x += 2 )
					{
						// Build the control points for this patch.
						//
						for( int i = 0; i < 3; ++i )
						{
							// For row y in the patch matrix and row i in the control point matrix,
							// calculate the corresponding offset into the vertex array.
							//
							const int patchVertexRowOffset = sizeX  *( i + y );

							for( int j = 0; j < 3; ++j )
							{
								vecControlPoints[ j + i  *3 ] = 
									inPatchVertices[ baseFaceIndex + x + j + patchVertexRowOffset ];
							}
						}

						// Use the control points for this individual patch to generate triangle vertices.
						//
						TesselateBezierPatch( 
							vecControlPoints, 
							outTriangleVertices, 
							outTriangleIndices, 
							iStartVertex,
							degreeOfTesselation );
					}
				}

				// Update the face (or make a new face) with the correct vertex and index rendering information, along
				// with other information from the original face.
				//
				BSPFaceT *pPolygonFace = 0;

				if( pOutPatchFaces != 0 )
				{
					pOutPatchFaces->push_back( face );		// Make a new copy of the face.
					pPolygonFace = &( pOutPatchFaces->back() );
				}
				else
				{
					pPolygonFace = &face;
				}

				int nVertices = int(outTriangleVertices.size() - iStartVertex);
				int nIndices = int(outTriangleIndices.size() - iStartIndex);

				assert( nVertices > 0 );
				assert( nIndices > 0 );

				SetupPolygonBSPFace( *pPolygonFace, iStartVertex, nVertices, iStartIndex, nIndices );

			}	// END Processing a Bezier patch face.
		}
	}

	// Given a Bezier patch defined by a set of control points, and given a degree of tesselation,
	// this function generates vertices and indices that represent the triangles that may be used to render the patch.
	// The patch is divided into a number of subdivisions as determined by degreeOfTesselation.
	// For each subdivision of the patch, a pair of triangles is emitted to the index vector.
	//
	// The following support functions must exist:
	//		VertexT operator+( const VertexT&, const VertexT& );
	//		VertexT operator*( const VertexT&, float );
	// 
	// IndexT is expected to be an ordinal type implicitably castable from size_t.
	//
	template< typename VertexT, typename IndexT >
	void TesselateBezierPatch( 
							  const std::vector< VertexT >& inControlPoints,
							  std::vector< VertexT >& outTriangleVertices,
							  std::vector< IndexT >& outTriangleIndices,
							  IndexT baseVertexIndex,
							  IndexT degreeOfTesselation
							  )
	{
		assert( inControlPoints.size() == 9 );

		// Add another patch's worth of vertices to the triangle array.
		//
		const IndexT outStartVertex = IndexT(outTriangleVertices.size());

		const size_t nTriangleVertsPerPatch = ( degreeOfTesselation + 1 )  *( degreeOfTesselation + 1 );
		outTriangleVertices.resize( outTriangleVertices.size() + nTriangleVertsPerPatch );

		// Get a pointer to the actual vertices that are used for this patch (the ones we just added), 
		// so that indexing operations are simpler (they can be indexed off of 0 rather than iStartingVertex.
		//
		VertexT *const arrVerticesForThisPatch = &( outTriangleVertices.front()) + outStartVertex;

		//
		// Generate vertices for this patch.
		//

		// Tessellate the first column into the first row
		//
		for( IndexT iVertex = 0; iVertex <= degreeOfTesselation; ++iVertex )
		{
			float a = static_cast< float >( iVertex )/ degreeOfTesselation;
			float b = 1.0f - a;

			arrVerticesForThisPatch[ iVertex ] = 
				inControlPoints[0]  *(b  *b) + 
				inControlPoints[3]  *(2.0f  *b  *a) + 
				inControlPoints[6]  *(a  *a);
		}

		// Tessellate the rest of the rows based on tessellated CPs
		//
		for( IndexT iVertex = 1; iVertex <= degreeOfTesselation; ++iVertex )
		{
			float a = static_cast< float >( iVertex )/ degreeOfTesselation;
			float b = 1.0f - a;


			// Generate new control points based on tessellation of control point rows
			//
			VertexT arrGeneratedControlPoints[ 3 ];
			for( IndexT j = 0; j < 3; ++j )
			{
				int k = 3  *j;
				arrGeneratedControlPoints[ j ] =
					inControlPoints[k + 0]  *(b  *b) +
					inControlPoints[k + 1]  *(2.0f  *b  *a) +
					inControlPoints[k + 2]  *(a  *a);
			}

			// Generate vertices by using the generated control points
			//
			for( IndexT j = 0; j <= degreeOfTesselation; ++j )
			{
				float a = static_cast< float >( j )/ degreeOfTesselation;
				float b = 1.0f - a;

				arrVerticesForThisPatch[ iVertex  *(degreeOfTesselation+1) + j ] =
					arrGeneratedControlPoints[0]  *(b  *b) +
					arrGeneratedControlPoints[1]  *(2.0f  *b  *a) +
					arrGeneratedControlPoints[2]  *(a  *a);
			}
		}

		//
		// Generate indices for this patch.
		//

		baseVertexIndex = outStartVertex - baseVertexIndex;

		IndexT vertsPerEdge = degreeOfTesselation + 1;
		for( IndexT i = 0; i < degreeOfTesselation; ++i )
		{
			IndexT rowOffset = i  *vertsPerEdge;

			for( IndexT j = 0; j < degreeOfTesselation; ++j )
			{
				// Triangle indices clockwise
				//
				IndexT newOffset = baseVertexIndex + j + rowOffset;

				outTriangleIndices.push_back( newOffset );									// TL
				outTriangleIndices.push_back( newOffset + 1 );								// TR
				outTriangleIndices.push_back( newOffset + 1 + degreeOfTesselation + 1 );	// BR

				outTriangleIndices.push_back( newOffset );									// TL
				outTriangleIndices.push_back( newOffset + 1 + degreeOfTesselation + 1 );	// BR
				outTriangleIndices.push_back( newOffset + degreeOfTesselation + 1 );		// BL
			}
		}
	}

}		// END namespace jeffwofford