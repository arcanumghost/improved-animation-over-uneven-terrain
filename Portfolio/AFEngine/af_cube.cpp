#include "af_cube.h"
#include "af_rndrr.h"

namespace af
{
    // helper function, initializes with position and zeroes the rest
    CubeVertex RenderableVertexPos(vec3f position)
    {
        CubeVertex rv;

        rv.position = position;
        for(int i=0; i<4; ++i)
            rv.tangent[i] = 0;

        return rv;
    }

    // object constructor
    Cube::Cube(vec3f position, float sidelength, wchar_t *basetexturename, Shader *shader)
		: Object("Cube")
    {
		// reserve space for the biffers
        std::vector<CubeVertex> vBuffer;
        vBuffer.reserve(24);
        std::vector<uint> iBuffer;
        iBuffer.reserve(36);

        // create base vertices, texture coordinates, and indicies
        for(uint i = 0; i < 6; ++i)
        {
            int dir = i/2;
            int side = i%2;

            vBuffer.push_back(RenderableVertexPos(sidelength*(vec3f(float(dir==0?side:!side), float(i==3), float(side||i==2))-vec3f(.5f,.5f,.5f))));
            vBuffer.push_back(RenderableVertexPos(sidelength*(vec3f(float(side),              float(i==3), float(side==dir||side==(dir-1)))-vec3f(.5f,.5f,.5f))));
            vBuffer.push_back(RenderableVertexPos(sidelength*(vec3f(float(dir==0?side:!side), float(i!=2), float(side&&i!=3))-vec3f(.5f,.5f,.5f))));
            vBuffer.push_back(RenderableVertexPos(sidelength*(vec3f(float(side),              float(i!=2), float(i==0||i==5))-vec3f(.5f,.5f,.5f))));

            vBuffer[i*4  ].texCoord = vec2f(0.0f,0.0f);
            vBuffer[i*4+1].texCoord = vec2f(1.0f,0.0f);
            vBuffer[i*4+2].texCoord = vec2f(0.0f,1.0f);
            vBuffer[i*4+3].texCoord = vec2f(1.0f,1.0f);

            iBuffer.push_back(i*4);
            iBuffer.push_back(i*4+1);
            iBuffer.push_back(i*4+2);
            iBuffer.push_back(i*4+2);
            iBuffer.push_back(i*4+1);
            iBuffer.push_back(i*4+3);
        }
            
        // create the renderable and set up shadow renderable
        std::vector<af::SMVertex> smvBuffer;
        setUpVertices(position, vBuffer, iBuffer, unsigned(vBuffer.size()), unsigned(iBuffer.size()));
        for(uint i = 0; i < vBuffer.size(); ++i)
        {
            SMVertex smv;
            smv.position = vBuffer[i].position;
            smvBuffer.push_back(smv);
        }
        createRenderable(&vBuffer[0].position[0], unsigned(vBuffer.size()), sizeof(CubeVertex), iBuffer, shader);
        //createShadowRenderable(&smvBuffer[0].position[0], unsigned(smvBuffer.size()), sizeof(SMVertex), iBuffer, Renderable::s_shadowShader);

        // Load textures
        for(int i=0; i<3; ++i)
        {
            Texture *t = Renderer::getInstance().createTexture();
            std::wstring ws(basetexturename);

            bool flipNormalY = false;

            switch(i)
            {
            case 0: ws.append(L"Diffuse.bmp"); break;
            case 1: ws.append(L"Normal.bmp"); flipNormalY = true; break;
            case 2: ws.append(L"Depth.bmp"); break;
            }

            t->loadFromFile(ws.c_str(), false, flipNormalY);
            m_renderables[0]->addTexture(t);
        }

        // Initialize a texture for the shadowmap, it will
        // be used to determine shadows and associated with
        // the light's depth texture.
        m_renderables[0]->addTexture(Renderer::getInstance().createTexture());
        m_renderables[0]->m_textures[3]->texture = true;
    }

    // sets up vertices for the cube
    void Cube::setUpVertices( vec3f position, std::vector<CubeVertex> &vBuffer, std::vector<uint> &iBuffer, int nVertices, int nIndices)
    {
        // rotate and translate vertices
        float xRot = 0.0f;
        float yRot = 1.0f;
        float zRot = 0.0f;
        for(int i=0; i<nVertices; ++i)
        {
            vec3f t = vBuffer[i].position;

            float siny = sin(yRot);
            float cosy = cos(yRot);

            af::vec3f t2(siny*t.x - cosy*t.z, t.y, siny*t.z + cosy*t.x);

            vBuffer[i].position = t2 + position;
        }

        // find the center for light rotation
        m_center = (vBuffer[0].position+vBuffer[6].position)/2.0f;

        // calculate tangent space variables for the buffer
        for(int i=0; i<nIndices; i+=3)
        {
            af::vec3f p0(vBuffer[iBuffer[i  ]].position),
                      p1(vBuffer[iBuffer[i+1]].position),
                      p2(vBuffer[iBuffer[i+2]].position);
            af::vec2f c0(vBuffer[iBuffer[i  ]].texCoord),
                      c1(vBuffer[iBuffer[i+1]].texCoord),
                      c2(vBuffer[iBuffer[i+2]].texCoord);

            p1 = p1-p0;
            p2 = p2-p0;
            c1 = c1-c0;
            c2 = c2-c0;

            float m = 1/(c1[0]*c2[1] - c1[1]*c2[0]);
            af::vec3f T(m*(p1[0]*c2[1] - p2[0]*c1[1]), m*(p1[1]*c2[1] - p2[1]*c1[1]), m*(p1[2]*c2[1] - p2[2]*c1[1])),
                      B(m*(p2[0]*c1[0] - p1[0]*c2[0]), m*(p2[1]*c1[0] - p1[1]*c2[0]), m*(p2[2]*c1[0] - p1[2]*c2[0])),
                      N(crossproduct(p1, p2));
            normalize(N);
        
            T = T - dotproduct(N, T)*N;
            normalize(T);

            B = B - dotproduct(N, B)*N - dotproduct(T, B)*T;
            normalize(B);

            vBuffer[iBuffer[i]].normal = vBuffer[iBuffer[i+1]].normal =  vBuffer[iBuffer[i+2]].normal = N;
            vBuffer[iBuffer[i]].tangent[0] = vBuffer[iBuffer[i+1]].tangent[0] = vBuffer[iBuffer[i+2]].tangent[0] = T.x;
            vBuffer[iBuffer[i]].tangent[1] = vBuffer[iBuffer[i+1]].tangent[1] = vBuffer[iBuffer[i+2]].tangent[1] = T.y;
            vBuffer[iBuffer[i]].tangent[2] = vBuffer[iBuffer[i+1]].tangent[2] = vBuffer[iBuffer[i+2]].tangent[2] = T.z;
            vBuffer[iBuffer[i]].tangent[3] = vBuffer[iBuffer[i+1]].tangent[3] = vBuffer[iBuffer[i+2]].tangent[3] = dotproduct(crossproduct(N, T), B);
        }
    }

    //  update cube variables
    void Cube::update( matrix4x4f &mvp_matrix, matrix4x4f &lvp_matrix, vec3f &lightPos, vec3f &lightPos2, vec3f &camPos )
    {
        m_mvpMatrix = mvp_matrix;
        m_lvpMatrix = lvp_matrix;
        m_lightPos = lightPos;
        m_lightPos2 = lightPos2;
        m_camPos = camPos;

        m_renderables[0]->setUniformVariable( 0, &m_mvpMatrix.m);
        m_renderables[0]->setUniformVariable( 1, &m_lvpMatrix.m);
        m_renderables[0]->setUniformVariable( 2, &m_lightPos);
        m_renderables[0]->setUniformVariable( 3, &m_lightPos2);
        m_renderables[0]->setUniformVariable( 4, &m_camPos);
        //m_shadowRenderables[0]->setUniformVariable( 0, &m_lvpMatrix.m);
        //m_shadowRenderables[0]->setUniformVariable( 1, &m_lightPos2);
    }

    void Cube::objectShadowRender()
    {
        //m_shadowRenderables[0]->render();
    }
}