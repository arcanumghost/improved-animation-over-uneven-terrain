// Borrowed /modified from spacehax, Alex Welsh 2011
#ifndef XBOX_CONTROLLER_H
#define XBOX_CONTROLLER_H

#include "targetver.h"
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <crtdbg.h>
#include <sstream>
#include <cassert>

#include "af_vec.h"
#include "XInput.h"

#pragma comment( lib, "XInput.lib" )

#define LSTICK_YPOS 0
#define LSTICK_XPOS 1
#define LSTICK_YNEG 2
#define LSTICK_XNEG 3
#define RIGHT_TRIGGER 4
#define LEFT_TRIGGER 5

namespace af
{
	static int FRAMES_TO_VIBRATE = 10;

	class XBOX_Controller
	{
	public: 
		XBOX_Controller( int playerNumber );
		// REQUIRES( playerNumber > 0 && playerNumber < 5 );

		bool IsConnected();

		void Vibrate( int leftVal = 0, int rightVal = 0 );

		bool IsDown( unsigned short button );
		bool DidGoDown( unsigned short button );
		bool DidGoUp( unsigned short button );

		bool AltIsDown( unsigned short dir );
		bool AltGoDown( unsigned short dir );
		bool AltGoUp( unsigned short dir );

		vec2f GetLJoyNormal();
        vec2f GetLJoyCoordinates();
		vec2f GetRJoyNormal();
        vec2f GetRJoyCoordinates();
		float GetLJoyAngle();
		float GetRJoyAngle();

		void Update();

	private:
		static const unsigned int MAX_BUTTONS = 14;
		static const unsigned int MAX_ALT_BUTTON = 6;
		XINPUT_STATE m_controllerState;
		int m_controllerNum;
		int m_timeVibrate;

		int Lx, Ly, Rx, Ry;

		bool m_buttonStates[ MAX_BUTTONS ];
		bool m_lastButtonStates[ MAX_BUTTONS ];

		bool m_altButtonStates[ MAX_ALT_BUTTON ];
		bool m_lastAltButtonStates[ MAX_ALT_BUTTON ];

		int GetButton( unsigned short button );
		unsigned short GetRealButton( unsigned short button );

		XINPUT_STATE GetState();
	};
}

#endif