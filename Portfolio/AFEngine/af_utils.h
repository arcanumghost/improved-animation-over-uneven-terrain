// Utility 
#ifndef _AF_UTILS_H_
#define _AF_UTILS_H_

#include <cassert>

namespace af
{
    // Common constants
    const float PI = 3.14159f;
    const float HALF_PI = PI/2.0f;
    const float DEGREES_TO_RADIANS = PI/180.0f;
    const float RADIANS_TO_DEGREES = 180.0f/PI;

    // Sometimes typing uint is more convenient
    typedef unsigned int uint;

    // clamp used in some locations
    template<typename T>
    inline void clamp(T& v, const T& lo, const T& hi)
    {   
        v = v < lo ? lo : ( v > hi ? hi : v );  
    }

    // Mostly used in textures, artifacts from first assignment TODO cleanup
    #define REQUIRES assert
    #define PROMISES assert
}

#endif