#ifndef _AF_VEC_H_
#define _AF_VEC_H_

#include <cmath>

namespace af
{
    // vec3
    template<typename T>
    struct vec3
    {
        T x, y, z;

        explicit vec3(T _x = 0, T _y = 0, T _z = 0)
            : x(_x), y(_y), z(_z)
        {}

        vec3<T>& operator=(const vec3<T>& rhs)
        {
            if(this != &rhs)
            {
                x = rhs.x;
                y = rhs.y;
                z = rhs.z;
            }
            return *this;
        }

        vec3<T> operator-() const
        {
            vec3<T> result(-x, -y, -z);
            return result;
        }

        vec3<T> operator+(const vec3<T>& rhs) const
        {
            vec3<T> result = *this;
            return result+=rhs;
        }
        vec3<T> operator-(const vec3<T>& rhs) const
        {
            vec3<T> result = *this;
            return result-=rhs;
        }
        vec3<T> operator*(const T& rhs) const
        {
            vec3<T> result = *this;
            return result*=rhs;
        }
        vec3<T> operator/(const T& rhs) const
        {
            vec3<T> result = *this;
            return result/=rhs;
        }
        friend vec3<T> operator*(const T& lhs, const vec3<T>& rhs)
        {
            return rhs*lhs;
        }

        vec3<T>& operator+=( const vec3<T>& rhs )
        {
            x += rhs.x;
            y += rhs.y;
            z += rhs.z;
            return *this;
        }
        vec3<T>& operator-=( const vec3<T>& rhs )
        {
            x -= rhs.x;
            y -= rhs.y;
            z -= rhs.z;
            return *this;
        }
        vec3<T>& operator*=( const T& rhs )
        {
            x *= rhs;
            y *= rhs;
            z *= rhs;
            return *this;
        }
        vec3<T>& operator/=( const T& rhs )
        {
            x /= rhs;
            y /= rhs;
            z /= rhs;
            return *this;
		}

		bool operator!=( const vec3<T>& rhs )
		{
			return !(x == rhs.x && y == rhs.y && z == rhs.z);
		}
		bool operator==( const vec3<T>& rhs )
		{
			return (x == rhs.x && y == rhs.y && z == rhs.z);
		}

        float dist() const   
        {   
            return std::sqrt(x*x + y*y + z*z);    
        }

        vec3<T>& normalized() const
        {
            if(dist() == 0.0f)
                return vec3(1,1,1)/vec3(1,1,1).dist();
            return *this/dist();
        }
    
        T& operator[](unsigned int i)
        {
            return (&x)[i];
        }

        T operator[](unsigned int i) const
        {
            return (&x)[i];
        }
    };

    // vec3 definitions
    typedef vec3<float> vec3f;
    typedef vec3<int> vec3i;

    // common vec3f functions
    float dotproduct(const vec3f&, const vec3f&);
    vec3f crossproduct(vec3f&, vec3f&);
    void normalize(vec3f&);

    // vec2
    template<typename T>
    struct vec2
    {
        T x, y;

        explicit vec2(T _x = 0, T _y = 0)
            : x(_x), y(_y)
        {}

        vec2<T>& operator=(const vec2<T>& rhs)
        {
            if(this != &rhs)
            {
                x = rhs.x;
                y = rhs.y;
            }
            return *this;
        }

        vec2<T> operator+(const vec2<T>& rhs) const
        {
            vec2<T> result = *this;
            return result+=rhs;
        }
        vec2<T> operator-(const vec2<T>& rhs) const
        {
            vec2<T> result = *this;
            return result-=rhs;
        }
        vec2<T> operator*(const T& rhs) const
        {
            vec2<T> result = *this;
            return result*=rhs;
        }
        vec2<T> operator/(const T& rhs) const
        {
            vec2<T> result = *this;
            return result/=rhs;
        }
        friend vec2<T> operator*(const T& lhs, const vec2<T>& rhs)
        {
            return rhs*lhs;
        }

        vec2<T>& operator+=( const vec2<T>& rhs )
        {
            x += rhs.x;
            y += rhs.y;
            return *this;
        }
        vec2<T>& operator-=( const vec2<T>& rhs )
        {
            x -= rhs.x;
            y -= rhs.y;
            return *this;
        }
        vec2<T>& operator*=( const T& rhs )
        {
            x *= rhs;
            y *= rhs;
            return *this;
        }
        vec2<T>& operator/=( const T& rhs )
        {
            x /= rhs;
            y /= rhs;
            return *this;
        }

        float dist()    
        {   
            return std::sqrt(x*x + y*y);    
        }
    
        T& operator[](unsigned int i)
        {
            return (&x)[i];
        }

        T operator[](unsigned int i) const
        {
            return (&x)[i];
        }
    };

    // vec2 definitions
    typedef vec2<float> vec2f;
    typedef vec2<int> vec2i;

    // common vec2 functions
    void normalize(vec2f&);
}

#endif