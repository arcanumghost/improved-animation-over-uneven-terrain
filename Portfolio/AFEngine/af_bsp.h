#ifndef _AF_BSP_H_
#define _AF_BSP_H_

#include "targetver.h"

#ifdef _WIN32
#include <Windows.h>
#endif

#include <vector>

#include "af_gfx.h"
#include "af_vec.h"
#include "af_txtur.h"
#include "af_frstm.h"
#include "af_matrx.h"
#include "af_obj.h"

namespace af
{
    // Future BSPVertex, at present all objects use RenderableVertex
    struct BSPVertex
    {
        vec3f position;
        vec2f texCoord;
        vec2f lightmapTexCoord;
        vec3f normal;
    };

    const float GAMMA = 4.0f;

    enum LUMPS
    {
        I_ENTITIES = 0,
        I_TEXTURES,
        I_PLANES,
        I_NODES,
        I_LEAFS,
        I_LEAFFACES,
        I_LEAFBRUSHES,
        I_MODELS,
        I_BRUSHES,
        I_BRUSHSIDES,
        I_VERTICES,
        I_INDICES,
        I_SHADERS,
        I_FACES,
        I_LIGHTMAPS,
        I_LIGHTVOLUMES,
        I_VISDATA,
        I_MAXLUMPS
    };

    struct BSPLump
    {
        int offset;
        int length;
    };

    // BSPHeader checking
    const char BSPID[4] = {'I', 'B', 'S', 'P'};
    struct BSPHeader
    {
        char strID[4];
        int version;

        bool valid() { return memcmp( strID, BSPID, 4 ) == 0 && version == 46; }
    };

    // Vertex used internally by BSP Tree
    struct BSPInternalVertex
    {
        vec3f vPosition;
        vec2f vTextureCoord;
        vec2f vLightmapCoord;
        vec3f vNormal;
        unsigned char color[4];
    };

    struct BSPLeafFace
    {
        int faceIndex;
    };

    struct BSPPlane
    {
        vec3f normal;
        float dist;
    };

    struct BSPNode
    {
        int plane;
        int front;
        int back;
        int mins[3];
        int maxs[3];
    };

    struct BSPLeaf
    {
        int cluster;
        int area;
        int mins[3];
        int maxs[3];
        int leafFace;
        int n_leafFace;
        int leafBrush;
        int n_leafBrushes;

        // determine if this leaf contains the pvs culling position
        bool contains(const vec3f& position)
        {
            for(int i=0; i<3; ++i)
                if(mins[i] >= position[i] == maxs[i] >= position[i])
                    return false;
            return true;
        }
    };

    struct BSPFace
    {
        int textureID;
        int effect;
        int type;
        int startVertIndex;
        int numOfVerts;
        int startIndex;
        int numOfIndices;
        int lightmapID;
        int lMapCorner[2];
        int lMapSize[2];
        vec3f lMapPos;
        vec3f lMapVecs[2];
        vec3f vNormal;
        int size[2];
    };

    struct BSPTexture
    {
        char strName[64];
        int flags;
        int textureType;
    };

    struct BSPVisData
    {
        int numOfVectors;    
        int vectorSize;      
        char *pVecs;         
    };

    // Determine if the test cluster is visible from the current cluster
    inline int isClusterVisible(BSPVisData *pPVS, int current, int test)
    {
        if(!pPVS->pVecs || current < 0) return 1;

        char vector = pPVS->pVecs[(current*pPVS->vectorSize) + (test/8)];
        int result = vector & (1 << (test & 7));

        return ( result );
    }

    struct BSPLightmap
    {
        unsigned char imageBits[128][128][3];
    };

    // Quake 3 BSP file
    class BSP : public Object
    {
		// massive list of BSP variables, mostly used in loading
        BSPHeader m_bspHeader;
        BSPLump m_bspLumps[I_MAXLUMPS];
        std::vector<BSPTexture> m_bspTextures;
        Texture **m_textures;
        Texture **m_lightmaps;
        std::vector<BSPFace> m_bspFaces;
        std::vector<BSPLeaf> m_bspLeafs;
        std::vector<BSPLeafFace> m_bspLeafFaces;
        std::vector<BSPNode> m_bspNodes;
        std::vector<BSPPlane> m_bspPlanes;
        std::vector<BSPInternalVertex> m_bspVertices;
        std::vector<BSPLightmap> m_bspLightmaps;
        std::vector<uint> m_bspIndices;
        BSPVisData m_bspVisData;
        int m_currentCluster;

        // Keep track of two shaders that can be swapped out for different faces
        Shader *m_defaultShader;
        Shader *m_lightmapShader;

        // Keep track of frustum for node collision
        Frustum *m_frustum;

        // Keep track of variables used in rendering
        matrix4x4f m_mvpMatrix;
        matrix4x4f m_lvpMatrix;
        vec3f m_lightPos;
        uint m_textureIndex;
        uint m_lightmapIndex;

        // list of face iterations, checked so drawing doesn't happen twice
        unsigned int *m_lastFaceIterations;
        
    public:
        BSP( char *filename, Shader *lightMapShader, Shader *defaultShader);
        ~BSP();

        // update and draw functions
        void draw( bool cullBSP = true, int nodeIndex = 0);
        void updateCluster(Camera&);
        void drawFace( int );
        void update( Frustum& frustum, Camera& c );

        // Render the object, overridden from Object base
        Texture *m_shadowMap;
        void render( );
        void objectShadowRender( );
    };
}

#endif
