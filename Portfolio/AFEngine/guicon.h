﻿#pragma once

#ifdef _WIN32

#include <windows.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include <iostream>
#include <fstream>
#include <queue>
#include <map>
#include <string>

#ifndef _USE_OLD_IOSTREAMS
using namespace std;
#endif

// maximum number of lines the output console should have
static const WORD MAX_CONSOLE_LINES = 32767;
static const WORD MAX_CONSOLE_COLUMNS = 80;

namespace af
{
#define BLACK 0
#define DARKBLUE 1
#define GREEN 2
#define DARKRED 4
#define TEAL DARKBLUE|GREEN
#define VIOLET DARKRED|DARKBLUE
#define DARKYELLOW DARKRED|GREEN
#define GRAY DARKBLUE|GREEN|DARKRED

#define RED 8|DARKRED
#define PINK 8|VIOLET
#define BRIGHTGREEN 8|GREEN
#define TURQUOISE 8|TEAL
#define BLUE 8|DARKBLUE
#define LIGHTGRAY 8|BLACK
#define YELLOW 8|DARKYELLOW
#define WHITE 8|GRAY

#define CL_VERTICAL	  '³'
#define CL_HORIZONTAL 'Ä'
#define CL_TLEFT	  'Ã'
#define CL_TRIGHT	  '´'
#define CL_TUP		  'Â'
#define CL_TDOWN	  'Á'
#define CL_LLEFT	  'Ù'
#define CL_LRIGHT	  'Ú'
#define CL_LUP		  'À'
#define CL_LDOWN	  '¿'
#define CL_CROSS	  'Å'

	class guicon
	{
		static HANDLE outConsoleHandle;
		static HANDLE inConsoleHandle;
		static WORD currentColor;
		static string inputString;
		static COORD inputcursor;

		static void (*inputcallback)(const char *);

	public:
		static map<string, WORD> colors;

		static void color(WORD text, WORD bg = BLACK)
		{
			currentColor = bg<<4 | text;
			SetConsoleTextAttribute(outConsoleHandle, currentColor);
		}

		static void cursor(int X, int Y)
		{
			COORD position;

			position.X = X;
			position.Y = Y;

			SetConsoleCursorPosition(outConsoleHandle, position);
		}

		static void begininput(void (*fp)(const char *))
		{
			inputcallback = fp;
		}

		static void inputpoll()
		{
			DWORD nevents, numread;
			INPUT_RECORD input;

			GetNumberOfConsoleInputEvents(inConsoleHandle, &nevents);

			while(nevents)
			{
				ReadConsoleInput(inConsoleHandle, &input, 1, &numread);
				if(input.EventType == KEY_EVENT && input.Event.KeyEvent.bKeyDown)
				{
					if(input.Event.KeyEvent.wVirtualKeyCode == VK_RETURN && inputString.length())
					{
						(*inputcallback)(inputString.c_str());
						inputString.clear();
						af::guicon::returninputcursor();
					}
					else if(input.Event.KeyEvent.wVirtualKeyCode == VK_BACK && inputString.length())
					{
						inputString.pop_back();
						returninputcursor();
						cout << ' ';
						returninputcursor();
					}
					else if(inputString.length() <= 51 && input.Event.KeyEvent.uChar.AsciiChar >= 0x20 && input.Event.KeyEvent.uChar.AsciiChar < 127)
					{
						inputString += input.Event.KeyEvent.uChar.AsciiChar;
						cout << input.Event.KeyEvent.uChar.AsciiChar;
					}
				}

				GetNumberOfConsoleInputEvents(inConsoleHandle, &nevents);
			}
		}

		static void writechar(unsigned x, unsigned y, wchar_t c)
		{
			COORD buffSize = {1,1};
			COORD buffPos = {0,0};
			SMALL_RECT newBuffArea = {x, y, x, y};
			CHAR_INFO tile[1];

			tile[0].Char.UnicodeChar = c;
			tile[0].Attributes = currentColor;
			WriteConsoleOutputA(GetStdHandle(STD_OUTPUT_HANDLE), tile, buffSize, buffPos, &newBuffArea);
		}

		static void writebox(unsigned left, unsigned top, unsigned right, unsigned bottom)
		{

		}

		static void clearbox(unsigned left, unsigned top, unsigned right, unsigned bottom)
		{
			for(unsigned x=left; x<=right && x<MAX_CONSOLE_COLUMNS; ++x)
				for(unsigned y=top; y<=bottom && y<MAX_CONSOLE_LINES; ++y)
				{
					cursor(x,y);
					printf(" ");
				}
		}

		static void returninputcursor()
		{
			cursor(int(inputString.length()+1), int(MAX_CONSOLE_LINES-2));
		}

		static void setcursor(bool on, DWORD size = 10)
		{
			CONSOLE_CURSOR_INFO cursorinfo;
			
			cursorinfo.dwSize = 1;
			cursorinfo.bVisible = on;

			SetConsoleCursorInfo(outConsoleHandle, &cursorinfo);
		}

		static void init()
		{
			int hConHandle;
			long lStdHandle;
			CONSOLE_SCREEN_BUFFER_INFO coninfo;
			FILE *fp;

			// allocate a console for this app
			AllocConsole();

			// set the screen buffer to be big enough to let us scroll text
			GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
			coninfo.dwSize.Y = MAX_CONSOLE_LINES;
			coninfo.dwSize.X = MAX_CONSOLE_COLUMNS;
			SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

			coninfo.srWindow.Left = 0;
			coninfo.srWindow.Top = 0;
			coninfo.srWindow.Bottom = 30;
			coninfo.srWindow.Right = 80;
			SetConsoleWindowInfo(GetStdHandle(STD_OUTPUT_HANDLE), true, &coninfo.srWindow);

			HWND hWnd = GetConsoleWindow();
			MoveWindow(hWnd, -800, 0, 800, 600, TRUE);

			// redirect unbuffered STDOUT to the console
			outConsoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
			hConHandle = _open_osfhandle(long(outConsoleHandle), _O_TEXT);
			fp = _fdopen( hConHandle, "w" );
			*stdout = *fp;
			setvbuf( stdout, NULL, _IONBF, 0 );

			// redirect unbuffered STDIN to the console
			inConsoleHandle = GetStdHandle(STD_INPUT_HANDLE);
			hConHandle = _open_osfhandle(long(inConsoleHandle), _O_TEXT);
			fp = _fdopen( hConHandle, "r" );
			*stdin = *fp;
			setvbuf( stdin, NULL, _IONBF, 0 );

			// redirect unbuffered STDERR to the console
			lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);
			hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
			fp = _fdopen( hConHandle, "w" );
			*stderr = *fp;
			setvbuf( stderr, NULL, _IONBF, 0 );

			// make cout, wcout, cin, wcin, wcerr, cerr, wclog and clog
			// point to console as well
			ios::sync_with_stdio();
			
			colors["black"] = BLACK;
			colors["darkred"] = DARKRED;
			colors["darkblue"] = DARKBLUE;
			colors["green"] = GREEN;
			colors["teal"] = TEAL;
			colors["violet"] = VIOLET;
			colors["darkyellow"] = DARKYELLOW;
			colors["gray"] = GRAY;
			colors["red"] = RED;
			colors["pink"] = PINK;
			colors["brightgreen"] = BRIGHTGREEN;
			colors["turquoise"] = TURQUOISE;
			colors["blue"] = BLUE;
			colors["lightgray"] = LIGHTGRAY;
			colors["yellow"] = YELLOW;
			colors["white"] = WHITE;
		}
	};
}

#endif