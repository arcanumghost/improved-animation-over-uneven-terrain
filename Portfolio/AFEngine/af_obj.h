#ifndef _AF_OBJECT_H_
#define _AF_OBJECT_H_

#include "af_rndbl.h"
#include "af_rune.h"
#include <vector>

namespace af
{
    // Game object class
    class Object
    {
        bool m_active;

    public:
        // mvp matrix used for many static objects
        matrix4x4f m_mvpMatrix;
        std::string m_name;

        // renderable pointer, can be GL or DX
        std::vector<Renderable *> m_renderables;
        std::vector<Renderable *> m_shadowRenderables;

        // initializes to zero
        Object(char * = nullptr);
        virtual ~Object();
        // initialize with a renderable
        virtual void createRenderable(float *vBuffer, uint nVertices, uint vertexSize, std::vector<uint> &iBuffer, Shader *shader);
        virtual void updateVertices(float *vBuffer, uint nVertices, uint vertexSize, uint index = 0);
        void createShadowRenderable(float *vBuffer, uint nVertices, uint vertexSize, std::vector<uint> &iBuffer, Shader *shader);
        // 4/24 below comments no longer applicable usage
        // may be overwritten and this added to Renderer for specialized rendering.
        // deactivate renderable to prevent default rendering, then use
        // m_renderable->
        //virtual void objectRender(){};
        virtual void render();
		virtual void shadowRender();
        virtual void objectShadowRender(){};

        bool active()       { return m_active; }
        virtual void activate()     { m_active = true; }
        virtual void deactivate()   { m_active = false; }

        std::string &name() { return m_name; }
    };
}

#endif
