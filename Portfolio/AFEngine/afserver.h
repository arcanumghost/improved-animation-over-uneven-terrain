#pragma once

#include "afpacket.h"

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind/bind.hpp>

using namespace boost::asio::ip;

namespace af
{
	class server
	{ 
		void handle_receive_from(const boost::system::error_code& error, size_t bytes);
		void handle_send_to(const boost::system::error_code& error, size_t bytes);
		void start_receive();

	public:
		// Initialize this server
		server(boost::asio::io_service& _service, short _port);

		// Called if necessary by service.poll, overload with desired functionality
		// realizing that the data below is available for use within the function.
		virtual void receive(size_t bytes) = 0;
		virtual void send();
		void start_send();

	protected:
		boost::asio::io_service &service; 
		short port;
		udp::socket sock;
		udp::endpoint clientaddr;
		static packet packetout;
		static packet::packetdata packetin;
	};
}