#ifndef _AF_RENDERER_H_
#define _AF_RENDERER_H_

#include <vector>
#include <map>
#include <algorithm>
#include "af_rndbl.h"
#include "af_singl.h"
#include "af_rune.h"
#include <AntTweakBar.h>

namespace af
{
    class Object;

    // Singleton factory for shaders, renderables, and textures
    class Renderer : public Singleton<Renderer>
    {
    protected:
        // Keeps track of renderables and some objects with special draw calls
        // Example: BSPs should be rendered using object commands
        //static std::vector<Renderable *> s_renderables;
        static std::vector<Object *> s_objects;
        static std::map<std::string, unsigned> s_objectmap;
        static std::vector<Object *> s_shadowObjects;
		matrix4x4f m_projmat;
        
        // Helper function for adding renderables
        //void addRenderable(Renderable *pRenderable);
    public:
        // Initializes the renderer, reads from AFEngine.ini
        static void initialize(HWND hWnd, RECT &clientRect);

        // Iterates over renderable objects and renders them.
        virtual void render();
        virtual ~Renderer();

        // Produces a DX or GL shader based on the 
        virtual Shader *createShader(std::string filename) = 0;
        virtual Renderable *createRenderable(float *vBuffer, uint nVertices, uint vertexSize, std::vector<uint> &iBuffer, Shader *shader) = 0;
        virtual Texture *createTexture() = 0;
        virtual Texture *createTexture( uint width, uint height, uint *pRGBATexels, bool useAlpha ) = 0;

        // Used for adding objects that render differently
        void addObject(Object *);
        void addShadow(Object *);

        // Native functions used by runescript
        static value getIndex(value &name);
        static value setanim(value &objindex, value &animindex);
        static value active(value &objindex);
        static value activate(value &objindex);
        static value deactivate(value &objindex);

		void setProjection(matrix4x4f &projmat) {m_projmat = projmat;}
        virtual ID3D11ShaderResourceView *getShadowMap() {return nullptr;};
        
        TwBar *myBar;
    };

    // Functions used in initializer to create renderers
    extern void createRendererGL(HWND hWnd, RECT &clientRect);
    extern Renderer *createRendererDX(HWND hWnd, RECT &clientRect);
}

#endif