#include "af_shdr.h"
#include <fstream>

// OutputDebugString support
#include <windows.h>
#include <sstream>
#include <algorithm>
#include <iostream>


namespace af
{
    // Used to map attributes in shader to attribute values in DX and GL
    char *ShaderAttributeStrings[] = 
    {
        "apos",
        "acolor",
        "atexcoord",
        "alighttexcoord",
        "anormal",
        "atangent",
        "aweights",
        "abones"
    };

    GLShaderAttribute::GLShaderAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLvoid *pointer)
        : m_index(index), m_size(size), m_type(type), m_normalized(normalized), m_pointer(pointer)
    {}

    DXShaderAttribute::DXShaderAttribute( D3D11_INPUT_ELEMENT_DESC &_desc )
        : m_desc(_desc)
    {}

    void GLShaderAttribute::enable(int stride)
    {
        glEnableVertexAttribArray( m_index );
        glVertexAttribPointer( m_index, m_size, m_type, m_normalized, stride, m_pointer);
    }

    void DXShaderAttribute::enable(int stride) {}
    
    void GLShaderAttribute::disable()
    {
        glDisableVertexAttribArray( m_index );
    }

    void DXShaderAttribute::disable() {}

    // GL Type strings for Uniforms
    char *GLShaderUniformTypeStrings[] =
    {
        "sampler2D",
        "vec3",
        "mat4"
    };

    // DX Type strings for Uniforms
    char *DXShaderUniformTypeStrings[] =
    {
        "Texture2D",
        "float3",
        "float4x4",
        "float"
    };

    GLShaderUniform::GLShaderUniform( std::string nameString, ShaderUniformType type)
        : ShaderUniform(nameString, type)
    {}

    DXShaderUniform::DXShaderUniform( std::string nameString, ShaderUniformType type)
        : ShaderUniform(nameString, type)
    {}
    
    ShaderUniform::ShaderUniform( std::string nameString, ShaderUniformType type)
        : m_nameString(nameString), m_type(type), m_uniformid(0)
    {}

    void GLShaderUniform::setup( GLint shaderID )
    {
        m_uniformid = glGetUniformLocation( shaderID, m_nameString.c_str());
    }

    void DXShaderUniform::setup( ID3DX11Effect *effect )
    {
        m_effectvariable = effect->GetVariableByName(m_nameString.c_str());
    }

// Parse GLSL variable macro - used it twice, right?
#define PARSE_GLSL_VARIABLE(p, type, name) \
    sourcepointer += strlen(p) + 1; \
    char *type = strtok_s(sourcepointer, " ;\n", &context); \
    sourcepointer += strlen(type) + 1; \
    char *name = strtok_s(sourcepointer, " ;\n", &context); \
    sourcepointer += strlen(name) + 1;

    // Load a shader file - helper function for Shader loading
    int GLShader::LoadShader( const char *filename )
    {
        // Shader id
        GLuint sh;

        // Source
        char *source;

        // Check file name
        const char *ext = strrchr(filename, '.');
        bool validFileName = (strstr(filename, ".vp") == ext) || (strstr(filename, ".fp") == ext);
        
        // Load shader from file
        std::fstream file(filename, std::ios::in | std::ios::binary | std::ios::ate);
        if(file.is_open() && validFileName)
        {
            // Read file
            int length = (int)file.tellg();
            file.seekg( 0 );
            source = new char[length+1];
            file.read(source, length);
            source[length] = 0;
            
            // Create Shader
            switch(ext[1])
            {
            case 'v':
                sh = glCreateShader( GL_VERTEX_SHADER );
                break;
            case 'f':
                sh = glCreateShader( GL_FRAGMENT_SHADER );
                break;
            }

            // Bind and compile source
            glShaderSource( sh, 1, (const GLchar**)(&source), NULL );
            glCompileShader( sh );

            // Error check for shader compile
            GLint success;
            glGetShaderiv( sh, GL_COMPILE_STATUS, &success );

            // Unsuccessful compile, write errors
            if(!success)
            {
                GLint length;
                GLchar *log;

                glGetShaderiv( sh, GL_INFO_LOG_LENGTH, &length );
                log = new GLchar[length];
                glGetShaderInfoLog( sh, length, &length, log );

                std::wstringstream ss;
                ss << "Failed to compile shader \"" << filename << "\": " << log << "." << std::endl;
                OutputDebugString( ss.str().c_str() );

                delete [] log;
                delete [] source;
                return 0;
            }
            else    // Search for and parse Uniforms and Attributes
            {
                char *sourcepointer = source;
                char *context = nullptr;
                int _offset = 0, _nextoffset = 0;
                while(char *p = strtok_s(sourcepointer, " ;\n", &context))
                {
                    if(strcmp("attribute", p) == 0)
                    {
                        // Parse type and name
                        PARSE_GLSL_VARIABLE(p, type, name);

                        // Set up variables
                        GLuint _index;
                        GLuint _size;
                        GLenum _type;

                        // Find the matching attribute enumeration
                        for(int i = SHADER_POSITION; i < SHADER_ATTRIBUTE_LENGTH; i++)
                            if(strcmp(ShaderAttributeStrings[i], name) == 0)
                                _index = i;

                        // Set attribute variables
                        switch(_index)
                        {
                        case SHADER_COLOR:
                            _size = 3; _type = GL_FLOAT; _nextoffset += 12; break;
                        case SHADER_POSITION:
                            _size = 3; _type = GL_FLOAT; _nextoffset += 12; break;
                        case SHADER_TEXCOORD:
                            _size = 2; _type = GL_FLOAT; _nextoffset += 8;  break;
                        case SHADER_LIGHTTEX_COORD:
                            _size = 2; _type = GL_FLOAT; _nextoffset += 8;  break;
                        case SHADER_NORMAL:
                            _size = 3; _type = GL_FLOAT; _nextoffset += 12; break;
                        case SHADER_TANGENT:
                            _size = 4; _type = GL_FLOAT; _nextoffset += 16; break;
                        case SHADER_WEIGHTS:
                            _size = 3; _type = GL_FLOAT; _nextoffset += 12; break;
                        case SHADER_BONES:
                            _size = 4; _type = GL_UNSIGNED_BYTE; _nextoffset += 4; break;
                        default:
                            _size = 0; _type = 0;        _nextoffset += 0;  break;
                        }

                        // Build Attribute
                        glBindAttribLocation( m_program, _index, name );
                        m_attributes.push_back(new GLShaderAttribute(_index, _size, _type, false, reinterpret_cast<GLvoid*>(_offset)));

                        _offset = _nextoffset;
                        m_stride = _nextoffset;
                    }
                    else if(strcmp("uniform", p) == 0)
                    {
                        // Parse type and name
                        PARSE_GLSL_VARIABLE(p, type, name);
                        
                        // Set up variables
                        GLint _type;

                        // Find appropriate uniform
                        for(int i = SHADER_INT1; i < SHADER_UNIFORM_LENGTH; i++)
                            if(strcmp(GLShaderUniformTypeStrings[i], type) == 0)
                                _type = i;

                        // Really hacky to get bone arrays in, I don't want to parse arrays from the source right now
                        if(name[12] == '[')
                        {
                            name[12] = 0;
                            _type += 2;
                        }

                        // Build Attribute
                        m_uniforms.push_back(new GLShaderUniform(name, (ShaderUniformType)_type));
                    }
                    else
                        sourcepointer += strlen(p) + 1;
                }
            }

            // clean up
            delete [] source;
            file.close();
        }
        else
        {
            // Error reporting
            std::wstringstream ss;
            ss << "Failed to load shader: \"" << filename << "\".";
            OutputDebugString( ss.str().c_str() );

            return 0;
        }

        return sh;
    }

    // Constructor for GL Shader
    GLShader::GLShader(const char *vsource, const char *psource)
    {
        // Create program
        m_program = glCreateProgram();
        
        // Attach individual shaders
        m_vshader = LoadShader( vsource );
        m_pshader = LoadShader( psource );

        // If both shaders compiled correctly
        if(m_vshader && m_pshader)
        {
            glAttachShader( m_program, m_vshader );
            glAttachShader( m_program, m_pshader );

            // Link shader program
            glLinkProgram(m_program);

            // error reporting
            GLint success;
            glGetProgramiv(m_program, GL_LINK_STATUS, &success);

            // Report linker errors
            if(!success)
            {
                GLint length;
                GLchar *log;

                glGetShaderiv( m_program, GL_INFO_LOG_LENGTH, &length );
                log = new GLchar[length];
                glGetShaderInfoLog( m_program, length, &length, log );

                std::wstringstream ss;
                ss << "Failed to link shader program: " << log << "." << std::endl;
                OutputDebugString( ss.str().c_str() );
				std::cout << ss.str().c_str() << std::endl;

                delete [] log;

                return;
            }
            else
            {
                // set up shader uniforms
                std::for_each(m_uniforms.begin(), m_uniforms.end(), [&](ShaderUniform *su)
                {
                    ((GLShaderUniform *)su)->setup(m_program);
                });
            }
        }
    }

    // delete shader attributes and uniforms
    Shader::~Shader()
    {
        std::for_each(m_attributes.begin(), m_attributes.end(), [&](ShaderAttribute *sa)
        {
            delete sa;
        });
        std::for_each(m_uniforms.begin(), m_uniforms.end(), [&](ShaderUniform *su)
        {
            delete su;
        });
    }

    // delete GL shader ids
    GLShader::~GLShader()
    {
        glDeleteShader(m_vshader);
        glDeleteShader(m_pshader);
        glDeleteProgram(m_program);
    }

    // enable shader attributes
    void GLShader::makeActiveShader()
    {
        glUseProgram( m_program );

        std::for_each(m_attributes.begin(), m_attributes.end(), [&](ShaderAttribute *sa)
        {
            sa->enable(m_stride);
        });
    }

// ADVANCE_TOKEN macro is used for navigating / parsing the complex DX shader effects
#define ADVANCE_TOKEN(token) \
    length = int(strlen(token)); \
    sourcepointer += length + 1; \
    while(strtok_s(sourcepointer, " ():,;\n\t\r", &context) != sourcepointer) \
        sourcepointer++;

    // Loads and compiles a DX Shader
    DXShader::DXShader(const wchar_t *fsource, ID3D11Device *device)
    {
        // Compile shader and create effect
        ID3D10Blob *fxblob = 0, *errblob = 0;
        if(FAILED(D3DX11CompileFromFile(fsource, 0, 0, "", "fx_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, 0, &fxblob, &errblob, 0)))
        {
            ShaderExit(errblob ? (char *)(errblob->GetBufferPointer()) : "fx file load");
            errblob->Release();        
        }
        HV(D3DX11CreateEffectFromMemory(fxblob->GetBufferPointer(), fxblob->GetBufferSize(), 0, device, &m_effect));
        fxblob->Release();

        // Load the file for parsing
        std::fstream file(fsource, std::ios::in | std::ios::binary | std::ios::ate);
        if(file.is_open())
        {
            // Read file
            int sourcelength = (int)file.tellg();
            int length = 0;
            file.seekg( 0 );
            char *source = new char[sourcelength+1];
            file.read(source, sourcelength);
            source[sourcelength] = 0;
            bool global = true;
            bool maindone = false;

            // Parse tokens
            char *sourcepointer = source;
            char *context = nullptr;
            int _offset = 0, _nextoffset = 0;
            while(char *p = strtok_s(sourcepointer, " ():,;\n\t\r", &context))
            {
                ADVANCE_TOKEN(p);
                if(!maindone && (strcmp("main",p)==0 || strcmp("AttributeVertex",p)==0))
                    while(strcmp("out",p)!=0 && *p!=0x7D)
                    {
                        if(strcmp("AttributeVertex",p)==0)
                        {
                            // Get to the first variable in the AttributeVertex
                            p = strtok_s(sourcepointer, " \n\t\r", &context);
                            ADVANCE_TOKEN(p);
                        }
                        // Add attribute
                        char *type = strtok_s(sourcepointer, " ", &context);
                        ADVANCE_TOKEN(type);
                        char *name = strtok_s(sourcepointer, ":", &context);
                        ADVANCE_TOKEN(name);
                        char *semantic = strtok_s(sourcepointer, ",", &context);
                        ADVANCE_TOKEN(semantic);
                        p = strtok_s(sourcepointer, " \t\n", &context);

                        // Determine attribute
                        int index; 
                        D3D11_INPUT_ELEMENT_DESC desc;
                        memset(&desc, 0, sizeof(D3D11_INPUT_ELEMENT_DESC));
                        for(int i = SHADER_POSITION; i < SHADER_ATTRIBUTE_LENGTH; i++)
                            if(strcmp(ShaderAttributeStrings[i], name) == 0)
                                index = i;

                        // Define attribute variables
                        switch(index)
                        {
                        case SHADER_COLOR:
                            desc.SemanticName = "COLOR";    desc.Format = DXGI_FORMAT_R32G32B32_FLOAT;    _nextoffset += 12; break;
                        case SHADER_POSITION:                                                                                
                            desc.SemanticName = "POSITION"; desc.Format = DXGI_FORMAT_R32G32B32_FLOAT;    _nextoffset += 12; break;
                        case SHADER_TEXCOORD:                                                                                
                            desc.SemanticName = "TEXCOORD"; desc.Format = DXGI_FORMAT_R32G32_FLOAT;       _nextoffset += 8;  desc.SemanticIndex = 0; break;
                        case SHADER_LIGHTTEX_COORD:                                                                          
                            desc.SemanticName = "TEXCOORD"; desc.Format = DXGI_FORMAT_R32G32_FLOAT;       _nextoffset += 8;  desc.SemanticIndex = 1; break;
                        case SHADER_NORMAL:                                                                                  
                            desc.SemanticName = "NORMAL";   desc.Format = DXGI_FORMAT_R32G32B32_FLOAT;    _nextoffset += 12; break;
                        case SHADER_TANGENT:                                                                                 
                            desc.SemanticName = "TANGENT";  desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT; _nextoffset += 16; break;
                        default:
                            break;
                        }

                        desc.AlignedByteOffset = _offset;
                        _offset = m_stride = _nextoffset;

                        // Build attribute
                        m_attributes.push_back(new DXShaderAttribute(desc));

                        // Make sure parsing does not accidentally return to uniforms
                        maindone = true;
                    }
                else if(!maindone && global && (strcmp("float", p)==0 || strcmp("float4x4", p)==0 || strcmp("float3",p)==0 || strcmp("Texture2D",p)==0))
                {
                    // Add uniform
                    char *type = p;
                    char *name = strtok_s(sourcepointer, " :;", &context);
                    ADVANCE_TOKEN(name);

                    // Determine uniform type
                    int _type;
                    for(int i = SHADER_INT1; i < SHADER_UNIFORM_LENGTH; ++i)
                    {
                        if(strcmp(DXShaderUniformTypeStrings[i], type) == 0)
                        {
                            _type = i;
                        }
                    }

                    // Build uniform
                    m_uniforms.push_back(new DXShaderUniform(name, (ShaderUniformType)_type));
                }
                // Determine global scope
                else if(strcmp(p,"{")==0)
                    global = false;
                else if(strcmp(p,"}")==0)
                    global = true;
                
                if(sourcepointer >= source + sourcelength)
                    break;
            }

            // clean up
            delete [] source;
            file.close();
        }

        // Create and set up Vertex Declaration
        D3D11_INPUT_ELEMENT_DESC *decl = new D3D11_INPUT_ELEMENT_DESC[m_attributes.size()];
        memset(decl, 0, sizeof(D3D11_INPUT_ELEMENT_DESC)*m_attributes.size());
        for(unsigned int i=0; i<m_attributes.size(); ++i)
            decl[i] = dynamic_cast<DXShaderAttribute *>(m_attributes[i])->m_desc;

        // Set up Technique
        m_technique = m_effect->GetTechniqueByName("MyTechnique");
        m_pass = m_technique->GetPassByIndex(0);
        HV(m_pass->GetDesc(&m_passDesc));
    
        // Create Input Layout from attributes
        HV(device->CreateInputLayout(decl, unsigned(m_attributes.size()), m_passDesc.pIAInputSignature, m_passDesc.IAInputSignatureSize, &m_inputLayout));

        // Set up uniforms
        std::for_each(m_uniforms.begin(), m_uniforms.end(), [&](ShaderUniform *su)
        {
            ((DXShaderUniform *)su)->setup(m_effect);
        });

        // delete attribute declarations
        delete [] decl;
    }

    // Apply the input layout and pass
    void DXShader::makeActiveShader(ID3D11DeviceContext *m_context)
    {
        m_context->IASetInputLayout(m_inputLayout);

        m_pass->Apply(0, m_context);
    }

    void DXShader::deactivateShader(ID3D11DeviceContext *m_context)
    {
        for(unsigned int u=0; u<m_uniforms.size(); ++u)
        {
            ShaderUniform *uniform = m_uniforms[u];
            ID3DX11EffectVariable *effectvar = ((DXShaderUniform *)uniform)->m_effectvariable;
            switch(uniform->m_type)
            {
            case SHADER_INT1:
                effectvar->AsShaderResource()->SetResource(nullptr);
                break;
            }
        }

        m_context->IASetInputLayout(NULL);

        m_pass->Apply(0, m_context);
    }
    
    // release input layout and effect
    DXShader::~DXShader()
    {
        m_inputLayout->Release();
        m_effect->Release();
    }
}