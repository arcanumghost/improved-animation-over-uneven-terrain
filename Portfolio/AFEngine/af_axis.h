#ifndef _AF_AXIS_H_
#define _AF_AXIS_H_

#include "af_obj.h"

namespace af
{
    // Renderable Vertex is currently in use, future implementation vertex
    struct DefaultVertex
    {
        vec3f position;
        vec3f color;
    };

    // Axis object, useful for debugging
    class Axis : public Object
    {
    public:
        Axis(float radius, Shader *shader);
        ~Axis();

        void update( matrix4x4f &vpMatrix, vec3f position );
    };
}

#endif