#ifndef _AF_SINGLETON_H_
#define _AF_SINGLETON_H_

#include <cassert>

namespace af
{
    // Singleton class used for Renderer, see Renderer for correct usage
    template< class T >
    class Singleton
    {
    private:
        static T *s_pInstance;

    public:
        static T &getInstance()         
        { 
            return *s_pInstance; 
        }
        static void setInstance( T *_pInstance)
        {
            assert(!exists());
            s_pInstance = _pInstance;
        }
        static bool exists()            { return s_pInstance != nullptr;  }
        static void deleteInstance()    
        { 
            assert(exists()); 
            delete s_pInstance;  
        }

    protected:
        Singleton( T* pInstance = nullptr )
        {
            s_pInstance = pInstance;
        }
    };

    // Static instance
    template< class T>
    T *Singleton<T>::s_pInstance;
}

#endif