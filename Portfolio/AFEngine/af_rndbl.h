#ifndef _AF_RENDERABLE_H_
#define _AF_RENDERABLE_H_

#include "targetver.h"
#include <windows.h>

#include "af_txtur.h"
#include "af_shdr.h"
#include "af_matrx.h"

namespace af
{
    // Various render modes, AF_LINES is used for Axis
    enum RenderMode
    {
        AF_LINES,
        AF_TRIANGLES,
        AF_RENDER_MODE_LENGTH
    };

    // Simple position vertex for creating depth maps from objects that cast shadows
    struct SMVertex
    {
        vec3f position;
    };

    // Base renderable objects
    class Renderable
    {
    protected:
        bool m_initialized;         // used to protect against uninitialized renderables
        bool m_active;              // active renderables are drawn natively with render()
        unsigned int m_renderMode;  // render mode
        void **m_uniformdata;       // data for uniform variables

        Renderable(unsigned int _renderMode);

    public:
        // necessary renderable resources
        Shader *m_shader;

        static Shader *s_shadowShader;
        static matrix4x4f s_lightMatrix;
        std::vector<Texture *> m_textures;
		std::vector<uint> m_othertextureids;

        // activation methods
        void activate()   { m_active = true; }
        void deactivate()  { m_active = false; }
        bool active() { return m_active; }
        // initalized get
        bool initialized()  { return m_initialized; }

        // Sets uniform variables for rendering
        void setUniformVariable( uint i, void *data) { if(initialized()) m_uniformdata[i] = data; }
        virtual void updateVBO(float *vBuffer, uint nVertices, uint vertexSize) = 0;

        // Modify rendering settings
        virtual void addTexture(Texture *) = 0;
        virtual void setRenderMode(RenderMode renderMode = AF_TRIANGLES) = 0;

        // full and elemental rendering
        virtual void render() = 0;
		virtual void shadowRender() = 0;
        virtual void renderElements(uint, uint, uint, uint) = 0;
        // virtual void shadowmapRender(uint, uint, uint, uint) = 0;
        
        virtual ~Renderable();
    };

    // GL variant of a renderable
    class GLRenderable : public Renderable
    {
    protected:
        // buffers and buffer counts
        GLuint m_VBO;
        GLuint m_IBO;
        GLsizei m_vCount;
        GLsizei m_iCount;

    public:
        GLRenderable();
        ~GLRenderable();
        
        // Initialize the renderable
        // void init(float *vBuffer, uint nVertices, uint *iBuffer, uint nIndices, Shader *shader);
        void init(float *vBuffer, uint nVertices, uint vertexSize, std::vector<uint> &iBuffer, Shader *shader);
        void updateVBO(float *vBuffer, uint nVertices, uint vertexSize);
        
        // GL-specific functions
        void addTexture(Texture *texture);
        void setRenderMode(RenderMode renderMode);
        void render();
		void shadowRender();
        void renderElements(uint startVertex, uint nVertices, uint startIndex, uint nIndices);
        // void shadowmapRender(uint startVertex, uint nVertices, uint startIndex, uint nIndices) {}
    };

    // DX variant of a renderable
    class DXRenderable : public Renderable
    {
    protected:
        // buffers and counts
        ID3D11Buffer *m_VBO;
        ID3D11Buffer *m_IBO;
        uint m_iCount;
        uint m_vCount; 

        // keep track of device and context for easy access
        ID3D11Device *m_device;
        ID3D11DeviceContext *m_context;

    public:
        DXRenderable(ID3D11Device *, ID3D11DeviceContext *);
        ~DXRenderable();

        // Initialize the renderable
        void init(float *vBuffer, uint nVertices, uint vertexSize, std::vector<uint> &, Shader *);
        void updateVBO(float *vBuffer, uint nVertices, uint vertexSize);

        // DX-specific functions
        void addTexture(Texture *);
        void setRenderMode(RenderMode);
        void render();
        void renderElements(uint, uint, uint, uint);
		void shadowRender(){};
        void shadowmapRender(uint, uint, uint, uint);
    };
}

#endif