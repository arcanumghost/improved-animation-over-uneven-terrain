#define _CRT_SECURE_NO_WARNINGS

#include "af_model.h"
#include <sstream>
#include <algorithm>
#include <string>
#include "af_rndrr.h"
#include "af_blend.h"
#include "af_cam.h"
#include "aflinearblender.h"

#ifdef _DEBUG
#include "geometry.h"
#endif

namespace af
{
#define BUFFER_SIZE 100

    void Model::LoadOBJ(char *path, Shader *_shader)
    {
        // Variables used in loading
        std::vector<vec3f> positions;
        std::vector<vec2f> textureCoords;
        std::vector<vec3f> normals;

        std::map<std::string, Texture*> textures;
        Texture *currentTexture = nullptr;

        std::vector<ModelVertex> vertices;
        std::vector<uint> vertexIndices;
        std::map<std::string, uint> fatvertices;

        std::string geometryName;

        m_updateTime = false;
        m_time = 0.0f;

        vec3f low;
        vec3f high;
        bool firstmesh = true;

        // load the obj file
        std::fstream infile;
        char buffer[BUFFER_SIZE];
        infile.open(path);
        if(!infile.fail())
            while(infile.getline(buffer, BUFFER_SIZE))
            {
                if(*buffer == '#')
                    continue;
                std::stringstream ss(buffer);
                std::string line;
                ss >> line;
                
                if(!line.compare("mtllib"))
                {
                    ss >> line;
                    std::string pathStr(path);
                    std::string mtlPath(pathStr.substr(0,pathStr.find_last_of('\\')));
                    if(!pathStr.compare(mtlPath))
                        mtlPath.clear();
                    mtlPath.append(line);

                    std::fstream mtlfile;
                    char mtlbuffer[BUFFER_SIZE];
                    mtlfile.open(mtlPath);
                    std::string mtlName;
                    std::string texturePath;
                    
                    if(!mtlfile.fail())
                        while(mtlfile.getline(mtlbuffer, BUFFER_SIZE))
                        {
                            std::stringstream mtlSS(mtlbuffer);
                            std::string mtlLine;
                            mtlSS >> mtlLine;

                            if(!mtlLine.compare("newmtl"))
                            {
                                mtlSS >> mtlName;
                            }
                            else if(!mtlLine.compare("map_Kd"))
                            {
                                mtlSS >> mtlLine;
                                texturePath.assign(mtlPath.substr(0,mtlPath.find_last_of('\\')));
                                if(!texturePath.compare(mtlPath))
                                    texturePath.clear();
                                texturePath.append(mtlLine);

                                if(texturePath.size() > 0 && mtlName.size() > 0)
                                {
                                    if(!textures[mtlName.c_str()])
                                    {
                                        wchar_t *wTexturePath = new wchar_t[texturePath.size()+1];
                                        mbstowcs(wTexturePath, texturePath.c_str(), texturePath.size()+1);
                                        textures[mtlName.c_str()] = Renderer::getInstance().createTexture();
                                        textures[mtlName.c_str()]->loadFromFile( wTexturePath, false );
                                        delete [] wTexturePath;
                                    }
                                }
                            }
                        }
                            
                    mtlfile.close();
                }
                else if(!line.compare("usemtl"))
                {
                    ss >> line;
                    currentTexture = textures[line];
                }
                else if(!line.compare("g"))
                {
                    if(!firstmesh)
                    {
                        createRenderable(&vertices[0].position[0], unsigned(vertices.size()), sizeof(ModelVertex), vertexIndices, _shader);
                        m_renderables.back()->addTexture(currentTexture);
                    }
                    vertexIndices.clear();
                    firstmesh = false;
                    ss >> geometryName;
                }
                else if(!line.compare("v"))
                {
                    vec3f v;
                    ss >> line;
                    v.x = float(atof(line.c_str()));
                    ss >> line;
                    v.y = float(atof(line.c_str()));
                    ss >> line;
                    v.z = float(atof(line.c_str()));
                    positions.push_back(v);
                    if(positions.size() == 1)
                        low = high = v;
                    else
                    {
                        low.x  = min(low.x, v.x);
                        high.x = max(high.x, v.x);
                        low.y  = min(low.y, v.y);
                        high.y = max(high.y, v.y);
                        low.z  = min(low.z, v.z);
                        high.z = max(high.z, v.z);
                    }
                }
                else if(!line.compare("vt"))
                {
                    vec2f vt;
                    ss >> line;
                    vt.x = float(atof(line.c_str()));
                    ss >> line;
                    vt.y = float(atof(line.c_str()));
                    ss >> line;
                    textureCoords.push_back(vt);
                }
                else if(!line.compare("vn"))
                {
                    vec3f vn;
                    ss >> line;
                    vn.x = float(atof(line.c_str()));
                    ss >> line;
                    vn.y = float(atof(line.c_str()));
                    ss >> line;
                    vn.z = float(atof(line.c_str()));
                    normals.push_back(vn);
                }
                else if(!line.compare("f"))
                {
                    int nVertices = 0;
                    while(!ss.eof())
                    {
                        ss >> line;
                        ++nVertices;

                        char *fatvertexIndex = new char[line.size()+1];
                        strcpy_s(fatvertexIndex, line.size()+1, line.c_str());
                        vec3i fatvertex;

                        char *s = new char[line.size()+1];
                        strcpy_s(s, line.size()+1, line.c_str());
                        fatvertex.x = atoi(strtok(s, "/"));
                        fatvertex.y = atoi(strtok(NULL, "/"));
                        char *s2 = strtok(NULL, "/");
                        if(s2 != 0)
                            fatvertex.z = atoi(s2);
                        else
                            std::swap(fatvertex.y, fatvertex.z);

                        delete [] s;

                        if(!fatvertices[fatvertexIndex])
                        {
                            fatvertices[fatvertexIndex] = unsigned(vertices.size());

                            ModelVertex rv = { positions[fatvertex.x-1], 
                                               vec2f(),
                                               normals[fatvertex.z-1]};
                            if(fatvertex.y)
                                rv.texCoord = textureCoords[fatvertex.y-1];
                            vertices.push_back(rv);
                        }

                        if(nVertices > 3)
                        {
                            vertexIndices.push_back(vertexIndices[vertexIndices.size()-3]);
                            vertexIndices.push_back(vertexIndices[vertexIndices.size()-2]);
                        }
                        vertexIndices.push_back(fatvertices[fatvertexIndex]);

                        delete [] fatvertexIndex;
                    }
                }
            }
        else // Should probably output a message here... TODO
            exit(-1);

        // Close out and leave
        infile.close();

        createRenderable(&vertices[0].position[0], unsigned(vertices.size()), sizeof(ModelVertex), vertexIndices, _shader);
        m_renderables.back()->addTexture(currentTexture);
    }

    void Model::LoadSubAFM(std::fstream &infile, std::vector<Model *> &models, std::vector<Model *> *boneslist, 
        std::vector<std::vector<Texture *>> &textures, Shader *_shader, Shader *_animshader, matrix4x4f &_zerotran, BSphere *sphere)
    {
        // Header, used for making decisions
        short header;
        
        // variables 
        std::vector<ModelVertex> vertices;
        std::vector<AnimModelVertex> skinnedvertices;
        std::vector<uint> indices;
        
        // Variables for tracking progress with this model list
        unsigned modelsstart = unsigned(infile.tellg())-2;    // -2 for header, already eaten
        unsigned modelslength;
        infile.read((char *)&modelslength, 4);

        matrix4x4f new_zerotran(IDENTITY);

        // Build Models for this list
        while(infile.tellg() < modelsstart + modelslength)
        {
            // Begin a new model
            infile.read((char *)&header, 2);        // Model header
            assert(header==0x4001);
            Model *m = new Model();
            m->m_sphere = sphere;
            m->m_boneslist = boneslist;

            // Variables for tracking progress with this model
            unsigned modelstart = unsigned(infile.tellg())-2;
            unsigned modellength;
            infile.read((char *)&modellength, 4);

            // Build this model
            while(infile.tellg() < modelstart + modellength)
            {
                infile.read((char *)&header, 2);
                switch(header)
                {
                case 0x0005:    // String
                    {
                        // Unused, but parse it and throw it away
                        unsigned int strlength;
                        infile.read((char *)&strlength, 4);
                        char *modelname = new char[strlength-6+1];
                        infile.read(modelname, strlength-6);
                        modelname[strlength-6] = 0;
                        m->m_name = m_name;
                        m->m_name += '_';
                        m->m_name += modelname;
                        delete []  modelname;

                        // Import the type
                        infile.read((char *)&m->m_mtype, 4);
                        if(m->m_mtype == BONE)   // Bones
                        {
                            boneslist->push_back(m);
                        }

                        // Import the zero position
                        infile.read((char *)&m->m_zerotran, sizeof(matrix4x4f));
                        new_zerotran = m->m_zerotran*_zerotran;
                        Inverse(new_zerotran, m->m_zerotran);
                        break;
					}
				case 0x5000:    // Matrix List
					{
						unsigned int animlength;
						infile.read((char *)&animlength, 4);
						m->m_relTranMatrix.resize((animlength-6)/sizeof(matrix4x4f));
						infile.read((char *)&m->m_relTranMatrix[0], animlength-6);
						break;
					}
				case 0x5001:    // Tran Anim List
					{
						unsigned int animlength;
						infile.read((char *)&animlength, 4);
						vector<quat> trots;
						vector<vec3f> ttrans;
						vector<vec3f> tscale;
						animlength -= 6; animlength /= (sizeof(quat) + 2*sizeof(vec3f));
						m->m_relTranMatrix.reserve(animlength);
						trots.resize(animlength);
						ttrans.resize(animlength);
						tscale.resize(animlength);
						infile.read((char *)&trots[0], animlength*sizeof(quat));
						infile.read((char *)&ttrans[0], animlength*sizeof(vec3f));
						infile.read((char *)&tscale[0], animlength*sizeof(vec3f));
						for(unsigned i=0; i<animlength; ++i)
							m->m_relTranMatrix.push_back(trots[i].mat4(ttrans[i], tscale[i]));
						break;
					}
                case 0x4002:    // Mesh
                    {
                        unsigned int meshstart = unsigned(infile.tellg())-2;
                        unsigned int meshlength;
                        infile.read((char *)&meshlength, 4);
                        unsigned int matid;         infile.read((char *)&matid, 4);
                        unsigned int vertexmask;    infile.read((char *)&vertexmask, 4);

                        /*if(vertexmask != (VERTEX_DIFFTEXCOORD | VERTEX_NORMAL | VERTEX_POSITION))
                            exit(-1);*/

                        infile.read((char *)&header, 2);
                        unsigned int vertexlistsize;    infile.read((char *)&vertexlistsize, 4);

                        if(m->m_mtype == SKIN)
                        {
                            skinnedvertices.resize((vertexlistsize-6)/48);
                            infile.read((char *)&skinnedvertices[0], vertexlistsize-6);
                            /*for(unsigned sindex=0; sindex<skinnedvertices.size(); ++sindex)
								cout << float(skinnedvertices[sindex].weights[0]) << "  " <<
								float(skinnedvertices[sindex].weights[1]) << "  " <<
								float(skinnedvertices[sindex].weights[2]) << "  " <<
								float(1.0f - (skinnedvertices[sindex].weights[0] + skinnedvertices[sindex].weights[1] + skinnedvertices[sindex].weights[2])) << endl;*/
                        }
                        else
                        {
                            vertices.resize((vertexlistsize-6)/32);
                            infile.read((char *)&vertices[0], vertexlistsize-6);
                        }

                        infile.read((char *)&header, 2);
                        unsigned int indexlistsize;    infile.read((char *)&indexlistsize, 4);

                        indices.resize((indexlistsize-6)/4);
                        infile.read((char *)&indices[0], indexlistsize-6);

                        if(infile.tellg() < meshstart + meshlength)
                        {
                            vector<matrix4x4f> bonetrans;

                            infile.read((char *)&header, 2);
                            unsigned bonetransize;      infile.read((char *)&bonetransize, 4);

                            bonetrans.resize((bonetransize-6)/sizeof(matrix4x4f));
                            infile.read((char *)&bonetrans[0], bonetransize-6);

                            m->m_meshbonetrans.push_back(bonetrans);
                        }

                        // Push back this mesh
                        if(m->m_mtype == SKIN)
                            m->createRenderable(&skinnedvertices[0].position[0], unsigned(skinnedvertices.size()), sizeof(AnimModelVertex), indices, _animshader);
                        else
                            m->createRenderable(&vertices[0].position[0], unsigned(vertices.size()), sizeof(ModelVertex), indices, _shader);

                        m->m_renderables.back()->addTexture(textures[matid][0]);
                        m->m_renderables.back()->addTexture(textures[matid][1]);
                    }
                    break;
                case 0x4000:    // Model list
                    LoadSubAFM(infile, m->m_children, boneslist, textures, _shader, _animshader, new_zerotran, sphere);
                    break;
                default:
                    DebugBreak();
                }
            }

            // Push it on 
            models.push_back(m);
        }
            
            // Resources
            /*infile.read((char *)&length, 4);
            modelsposition+=length;
            infile.read((char *)&header, 2);        // String header
            
            position = 6 + strlength;

            while(position < length)
            {
                unsigned int meshlength;
                infile.read((char *)&header, 2);    // Mesh header
                infile.read((char *)&meshlength, 4);
                position += meshlength;

                unsigned int matid;         infile.read((char *)&matid, 4);
                unsigned int vertexmask;    infile.read((char *)&vertexmask, 4);

                if(vertexmask != (VERTEX_DIFFTEXCOORD | VERTEX_NORMAL | VERTEX_POSITION))
                    exit(-1);

                infile.read((char *)&header, 2);
                unsigned int vertexlistsize;    infile.read((char *)&vertexlistsize, 4);

                vertices.resize((vertexlistsize-6)/32);
                infile.read((char *)&vertices[0], vertexlistsize-6);

                infile.read((char *)&header, 2);
                unsigned int indexlistsize;    infile.read((char *)&indexlistsize, 4);

                indices.resize((indexlistsize-6)/4);
                infile.read((char *)&indices[0], indexlistsize-6);

                // Push back this mesh
                m_meshes.push_back(new Mesh(vertices, indices, _shader, textures[matid]));
            }*/
    }

    void Model::LoadAFM(char *path, Shader *_shader, Shader *_animshader)
    {
        // Variables used in loading
        std::vector<std::vector<Texture *>> &textures = m_textures;

        m_updateTime = false;
        m_time = 0.0f;

        // load the obj file
        std::fstream infile(path, std::ios::binary | std::ios::in);
        
        if(infile.fail())
            throw "Didn't open file";

        // Clear / check header
        unsigned short header;              infile.read((char *)&header, 2);
        unsigned int length;                infile.read((char *)&length, 4);
        unsigned short version;             infile.read((char *)&version, 2);
        unsigned int frames;                infile.read((char *)&frames, 4);
        m_sphere = new BSphere();           infile.read((char *)m_sphere, 16);
        delete m_sphere;                    // HAHAHA most broken code HACK

        if(header != 0xAFFF || version < 0x0101)
            exit(-1);

        // Collect materials
        infile.read((char *)&header, 2);
        infile.read((char *)&length, 4);
        unsigned int position=6;
        while(position < length)
        {
            unsigned int matlength;
            unsigned int texturemask;
            std::vector<Texture *> material;
            infile.read((char *)&header, 2);
            infile.read((char *)&matlength, 4);
            position += matlength;
            unsigned matposition = 10;
            infile.read((char *)&texturemask, 4);

            while(matposition < matlength)
            {
                // Read texture filename
                unsigned int textureNameLength = 0;
                infile.read((char *)&header, 2);
                infile.read((char *)&textureNameLength, 4);
                matposition += textureNameLength;
                textureNameLength-=6;
                char *texturePath = new char[textureNameLength+1];
                infile.read(texturePath, textureNameLength);
                texturePath[textureNameLength] = 0;

                wchar_t *wTexturePath = new wchar_t[textureNameLength+1];
                mbstowcs(wTexturePath, texturePath, textureNameLength+1);

                // Load texture from file and file it away
                Texture *temptexture = Renderer::getInstance().createTexture();
                temptexture->loadFromFile(wTexturePath);
                material.push_back(temptexture);

                delete [] texturePath;
                delete [] wTexturePath;
            }

            textures.push_back(material);
        }

        // Load models, start by simply passing the nframes, models, and textures
        infile.read((char *)&header, 2);            // Model List header
        assert(header==0x4000);

        // Register the name of this object
        string pathname(path);
        pathname = pathname.substr(0, pathname.size()-4);
        m_name = pathname;

        m_boneslist = new std::vector<Model *>;
        m_rootmodel = true;

        matrix4x4f _zerotran(IDENTITY);
        LoadSubAFM(infile, m_children, m_boneslist, textures, _shader, _animshader, _zerotran, m_sphere);

        Renderer::getInstance().addObject(this);

        // Close file and leave
        infile.close();
    }

    // Start a default model, privately used by AFM Model loader
    Model::Model()
        : m_boneslist(nullptr), m_mtype(DUMMY), m_sphere(nullptr),
          m_blendtree(nullptr), m_rootmodel(false), m_defaultshader(nullptr)
    {
        
    }

    // Load a model
    Model::Model(char *path, Shader *_shader, Shader *_animshader)
        : m_boneslist(nullptr), m_mtype(DUMMY), m_sphere(nullptr),
          m_blendtree(nullptr), m_rootmodel(false), m_defaultshader(nullptr)
    {
        std::string pathstr(path);
        std::string pathext = pathstr.substr(pathstr.find_last_of('.') + 1);
        if(!pathext.compare("obj") || !pathext.compare("OBJ"))
            LoadOBJ(path, _shader);
        else if(!pathext.compare("afm") || !pathext.compare("AFM"))
            LoadAFM(path, _shader, _animshader);
        else
            throw "Invalid model extension";
    }

    void Model::debugbreak(xboxeventdata &e)
    {
        DebugBreak();
    }

    void Model::moveXBox(xboxeventdata &e)
    {
        clamp(e.pos.x, -1.0f, 1.0f);
        clamp(e.pos.y, -1.0f, 1.0f);

        float croty = af::g_camera.rotation().y;
        vec2f modelcoords;
        modelcoords.x = e.pos.x*cos(croty) - e.pos.y*sin(croty);
        modelcoords.y = e.pos.y*cos(croty) + e.pos.x*sin(croty);

        clamp(modelcoords.x, -1.0f, 1.0f);
        clamp(modelcoords.y, -1.0f, 1.0f);

        m_rotation_goal.y = acos(modelcoords.y);
        if(modelcoords.x < 0.0f)
            m_rotation_goal.y *= -1.0f;
        m_rotation.y = m_rotation.y*.9f + m_rotation_goal.y*.1f;

        float tweight = ((linearblender *)m_blendtree->source)->m_weight = modelcoords.dist();

        // Debug blend weight
        //tweight = ((linearblender *)m_blendtree->source)->m_weight = 1.0f;

        // Update translation with the true rotation and true weight, not the goals affected by input above
        //m_translation.x += .23f * sin(m_rotation.y) * tweight;
        //m_translation.z -= .23f * cos(m_rotation.y) * tweight;
    }

    void Model::activate()
    {
        for(unsigned i=0; i<m_children.size(); ++i)
            m_children[i]->activate();
        Object::activate();
    }

    void Model::deactivate()
    {
        for(unsigned i=0; i<m_children.size(); ++i)
            m_children[i]->deactivate();
        Object::deactivate();
    }

    // Todo: give models orientation and have impulse be relative to that?
    /*void applyImpulse2D(const vec2f& impulse)
    {
        vec2f rotImp(
    }*/

    void Model::update(float time, const matrix4x4f &vp_Mat, const matrix4x4f &modelMat, 
		const vec3f &light, bool useblender, Anim *animation )
    {
        matrix4x4f transformedMatrix;
        // BUG this should be the same for skinned and unskinned, but the hierarchy is doubling a top-level
        // transformation, which is why the top level is skipped here. Problem likely lies in the 'updateanim'
        // function of af_anim
        if( m_relTranMatrix.size() && m_mtype != SKIN )
        {
            float t = time*30; //fps
            int i = int(t)%m_relTranMatrix.size();  // index
            transformedMatrix = m_relTranMatrix[i]*modelMat;
        }
        else
            transformedMatrix = modelMat;

		/*if(useblender && !animation)
		{
			animation = AnimBlender::getInstance().m_anims[0];
		}

		if(useblender)
		{
			transformedMatrix = animation->m_m_matrix;
		}*/
            
		m_useblender = useblender;

        matrix4x4f modelInv;
        if(m_mtype == SKIN)
        {
            if(!Inverse(transformedMatrix, modelInv))   
                DebugBreak();
        }
        else
        {
            if(!Inverse(transformedMatrix, modelInv))
                DebugBreak();
        }

        /* Translate the light from world to model space before sending to graphics card
        //Now done on the graphics card ^^;
        m_light.x = light.x*modelInv.m[0] + light.y*modelInv.m[4] + light.z*modelInv.m[ 8] + modelInv.m[12];
        m_light.y = light.x*modelInv.m[1] + light.y*modelInv.m[5] + light.z*modelInv.m[ 9] + modelInv.m[13];
        m_light.z = light.x*modelInv.m[2] + light.y*modelInv.m[6] + light.z*modelInv.m[10] + modelInv.m[14];//*/
        
        // Line that screws up the light for the land, fixes for model
        // Need to separate the M and VP for matrices passed in to all shaders and figure them out
        m_light=light;

        m_mvpMatrix = transformedMatrix*vp_Mat;
        m_vp_matrix = vp_Mat;
        m_m_matrix = transformedMatrix;
        /*if(time == 0.0f)
        {
            Inverse(transformedMatrix, m_zerotran);
            assert(m_zerotran[15]>.5f);
        }*/

        // Used for geometry... aka not yet
        /*if(m_updateTime)
            m_time += 1.0f/60.0f;
        else
            m_time = 0.0f;*/

        std::for_each(m_renderables.begin(), m_renderables.end(), [&](Renderable *r) 
        {
            r->setUniformVariable( 0, &m_vp_matrix.m );
            r->setUniformVariable( 1, &m_m_matrix.m );
            r->setUniformVariable( 2, &m_light );
        });

		for(unsigned i=0; i<m_children.size(); ++i)
		{
			if(useblender)
				m_children[i]->update(time, vp_Mat, transformedMatrix, light, useblender);//, animation->m_children[i]);
			else
				m_children[i]->update(time, vp_Mat, transformedMatrix, light);
        }
    }

	void Model::shadowRender()
	{
		std::for_each(m_renderables.begin(), m_renderables.end(), [&](Renderable *r)
		{
			//r->setUniformVariable( 0, &m_mvpMatrix.m);
			r->shadowRender();
		});
	}

    void Model::blend()
    {
        // Nicely ask the blend tree to do its thing.
        m_blendtree->work();
        // ... guess that's it for this part ... kind of a wrapper function ...

        framedata &fd = m_blendtree->m_workingframe;

        // HACK have I ever done anything hackier? so hardcoded...
        if(!m_name.compare("girlidle"))
        {
            m_translation.z += -cos(m_rotation.y)*fd.m_children[4]->m_tran.z - sin(m_rotation.y) * fd.m_children[4]->m_tran.x;
            m_translation.x +=  sin(m_rotation.y)*fd.m_children[4]->m_tran.z - cos(m_rotation.y) * fd.m_children[4]->m_tran.x;

            fd.m_children[4]->m_tran.x = fd.m_children[4]->m_tran.z = 0;
        }
    }

    void Model::applyframedata(framedata &fd, const matrix4x4f &modelMat)
    {
        m_anothertemporarymatrix = fd.m_rot.mat4(fd.m_tran, fd.m_scale)*modelMat;

        for(int i=0; i<m_children.size(); ++i)
            m_children[i]->applyframedata(*(fd.m_children[i]), m_anothertemporarymatrix);
    }

    void Model::render()
    {
        // package the framedata and modelmat
        if(m_blendtree)
            applyframedata(m_blendtree->m_workingframe, matrix4x4f(IDENTITY));

        if(m_mtype == SKIN && m_renderables.size())
        {
            unsigned ri = 0;
            for(int rindex=0; rindex<m_renderables.size(); ++rindex)
            {
                matrix4x4f bonematrices[100];

                unsigned i;

                for(i=0; i<m_boneslist->size() && i<100; ++i)
                {
                    bonematrices[i] = m_meshbonetrans[ri][i] * (*m_boneslist)[i]->m_anothertemporarymatrix;
                    
#ifdef _DEBUG
                    if(!m_defaultshader)
                        m_defaultshader = af::Renderer::getInstance().createShader("shaders\\default");
                    if(m_boneaxes.size()==i)
                        m_boneaxes.push_back(new af::Axis(1.0f, m_defaultshader));
                    matrix4x4f tmat = (*m_boneslist)[i]->m_anothertemporarymatrix*m_m_matrix;
                    vec3f tvec3(tmat.m[12], tmat.m[13], tmat.m[14]);
                    m_boneaxes[i]->update(tmat*m_vp_matrix, vec3f(0,0,0));
#endif
                }

                for(; i<100; ++i)
                    bonematrices[i] = matrix4x4f(IDENTITY);

				m_renderables[rindex]->setUniformVariable( 0, &m_vp_matrix.m );
                m_renderables[rindex]->setUniformVariable( 3, &bonematrices );

                if(m_renderables[rindex]->active())
                    m_renderables[rindex]->render();
            }
        }
        else
        {
            std::for_each(m_renderables.begin(), m_renderables.end(), [&](Renderable *r) 
            {
                r->setUniformVariable( 0, &m_vp_matrix.m );
                r->setUniformVariable( 1, &m_m_matrix.m );
            });

			if(!m_useblender)
	            Object::render();
        }
    }

    Model::~Model()
    {
        /*if(m_mtype == SKIN)
            delete m_boneslist;*/

        if(m_rootmodel)
        {
            delete m_boneslist;
            for(int i=0; i<m_textures.size(); ++i)
                for(int j=0; j<m_textures[i].size(); ++j)
                    delete m_textures[i][j];
        }

        /*for(unsigned i=0; i<m_textures.size(); ++i)
            delete m_textures[i];*/

        if(m_defaultshader)
        {
            delete m_defaultshader;
            m_defaultshader = nullptr;
        }

        for(unsigned i=0; i<m_boneaxes.size(); ++i)
            delete m_boneaxes[i];

        std::for_each(m_children.begin(), m_children.end(), [&](Model *child)
        {
            delete child;
        });

        delete m_blendtree;
    }
}