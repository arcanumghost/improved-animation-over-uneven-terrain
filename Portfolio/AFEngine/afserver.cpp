#include "afserver.h"

namespace af
{
	server::server(boost::asio::io_service& _service, short _port)
		: service(_service), port(_port), sock(service, udp::endpoint(udp::v4(), _port))
	{
		start_receive(); 
	}

	void server::handle_receive_from(const boost::system::error_code& error, size_t bytes)
	{
		if (!error && bytes > 0)
			receive(bytes);
		start_receive();
	}

	void server::send()
	{}

	void server::handle_send_to(const boost::system::error_code &error,
		size_t bytes)
	{
		if(!error & (bytes > 0))
			send();
	}

	void server::start_send()
	{
		sock.async_send_to(boost::asio::buffer(packetout.data.raw, packetout.datalength),
			clientaddr,
			boost::bind(&server::handle_send_to, this,
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
	}

	void server::start_receive()
	{
		sock.async_receive_from(
			boost::asio::buffer(packetin.raw, MAXPACKETSIZE),
			clientaddr,
			boost::bind(&server::handle_receive_from, this,
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
	}

	packet server::packetout;
	packet::packetdata server::packetin;
}