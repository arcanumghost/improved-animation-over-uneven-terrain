#pragma once

#include "afblendnode.h"

namespace af
{
    class linearblender : public blendnode
    {
    public:
        blendnode *m_baseinput;
        blendnode *m_weightedinput;
        float m_weight;

        const framedata *const work();
        virtual ~linearblender();
    };
}