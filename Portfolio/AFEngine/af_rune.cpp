#include "af_rune.h"

#include <sstream>
#include <algorithm>
#include <fstream>
#include "af_rndrr.h"
#include "af_input.h"
#include "af_blend.h"

namespace af
{
    // Global stringstream
    fstream g_ss;

    char *ByteCodeStrings[] =
    {
        "JUMP",         "JF",           "ASS",          "IASS",
        "PUSH_VAR",     "PUSH_IVAR",    "PUSH_VAL",     "PUSH_STR",
        "PUSH_VEC",     "ADD",          "SUB",          "MUL",
        "DIV",          "EQ",           "NEQ",          "LT",
        "GT",           "AND",          "OR",           "NEG",
        "NOT",          "POP",          "CALL",         "NCAL",
        "RET",          "ABORT"
    };

    // tabs output for a level
    void tabs(int level)
    {
        for(int i=0;i<level;++i)
            g_ss << "  ";
    }

    extern char *RuneStrings[];    

    char *UnaryStrings[] =
    {
        "Negate",       "Not"
    };

    char *BinaryStrings[] =
    {
        "Add",          "Subtract",     "Multiply",     "Divide",
        "Equal",        "Not Equal",    "Less Than",    "Greater Than",
        "Bool And",     "Bool Or",      "Assign"
    };

    RuneParser::RuneParser(char *filename)
        : m_analyzer(filename), m_parsed(false)
    {
        g_ss.open("output.txt", ios::out);
    }

    RuneParser::~RuneParser()
    {

    }
    
    void RuneParser::close()
    {
        g_ss.close();
    }

    bool RuneParser::parse()
    {
        m_analyzer.next();
        try 
        { 
            while(m_analyzer.token() != TOKEN_END_OF_FILE)
                parseFdef();
            m_parsed = true;
        }
        catch( ParserError e)
        {
            e.display();
            m_parsed = false;
        }
        return (m_parsed && !m_analyzer.error());
    }
        
    void RuneParser::expect(RuneToken t)
    {
        if(m_analyzer.token()!=t)
        {
            stringstream ss;
            ss << "Expected " << RuneStrings[t] << " token, found " << 
                RuneStrings[m_analyzer.token()] << " (" << 
                m_analyzer.strdata() << ") instead";
            throw ParserError(m_analyzer.line(), ss.str().c_str());
        }
        m_analyzer.next();
    }

    void RuneParser::parseFdef()
    {
        string name = m_analyzer.strdata();
        if(m_functions.count(name))
            throw ParserError(m_analyzer.line(), "Function redefinition");
        switch(m_analyzer.token())
        {
        case TOKEN_MAIN:
            m_currentFunction = m_functions[name] = new fdefnode("main");
            m_analyzer.next();
            break;
        case TOKEN_IDENTIFIER:
            m_currentFunction = m_functions[name] = new fdefnode(name);
            m_analyzer.next();
            expect(TOKEN_GROUP_BEGIN);
            while(m_analyzer.token() != TOKEN_GROUP_END)
            {
                if(m_currentFunction->m_args.size())
                    expect(TOKEN_SEPARATOR);
                if(m_analyzer.token() != TOKEN_IDENTIFIER)
                    expect(TOKEN_IDENTIFIER);
                m_currentFunction->m_args.push_back(new varnode(m_analyzer.strdata()));

                m_analyzer.next();
            }
            for(unsigned i=0; i<m_currentFunction->m_args.size(); ++i)
                m_currentFunction->m_vars[m_currentFunction->m_args[i]->m_name] 
                    = i-unsigned(m_currentFunction->m_args.size())-1;
            expect(TOKEN_GROUP_END);
            break;
        }

        if(m_analyzer.token() == TOKEN_BLOCK_BEGIN)
            m_currentFunction->m_body = (blcknode *)parseStmt();
        else
        {
            stringstream ss;
            if(!m_currentFunction->m_name.compare("main"))
                ss << "Main function keyword must be followed by a statement block";
            else
                ss << "Function arguments must be followed by a statement block";
            throw ParserError(m_analyzer.line(), ss.str().c_str());
        }
    }

    runenode *RuneParser::parseExpr(ExpressionType etype)
    {
        switch(etype)
        {
        case EXPR:
            {
                runenode *boolexpr = parseExpr(BOOLEXPR);
                if(m_analyzer.token() == TOKEN_ASSIGN)
                {
                    m_analyzer.next();
                    if(!(boolexpr->m_rtype==RT_IVAR || boolexpr->m_rtype==RT_VAR))
                        throw ParserError(m_analyzer.line(), "Invalid variable assignment, assignments must be identifiers or indexed identifiers");
                    boolexpr = new bnrynode(BINARY_ASSIGN, boolexpr, parseExpr(EXPR));
                    
                    // Check for new variable and add to function variables
                    if(((bnrynode *)boolexpr)->m_left->m_rtype==RT_VAR)
                    {
                        varnode *var = (varnode *)((bnrynode *)boolexpr)->m_left;
                        if(m_currentFunction->m_vars.find(var->m_name)==m_currentFunction->m_vars.end())
                            m_currentFunction->m_vars[var->m_name] = 
                                unsigned(m_currentFunction->m_vars.size()-m_currentFunction->m_args.size()+1);
                    }
                }
                return boolexpr;
            }
        case BOOLEXPR:
            {
                runenode *compexpr = parseExpr(COMPEXPR);
                while(m_analyzer.token()==TOKEN_AND || m_analyzer.token()==TOKEN_OR)
                    switch(m_analyzer.token())
                    {
                    case TOKEN_AND:
                        m_analyzer.next();
                        compexpr = new bnrynode(BINARY_AND, compexpr, parseExpr(BOOLEXPR));
                        break;
                    case TOKEN_OR:
                        m_analyzer.next();
                        compexpr = new bnrynode(BINARY_OR, compexpr, parseExpr(BOOLEXPR));
                        break;
                    }
                return compexpr;
            }
        case COMPEXPR:
            {
                runenode *plusexpr = parseExpr(PLUSEXPR);
                switch(m_analyzer.token())
                {
                case TOKEN_EQ:
                    m_analyzer.next();
                    return new bnrynode(BINARY_EQ, plusexpr, parseExpr(PLUSEXPR));
                case TOKEN_NEQ:
                    m_analyzer.next();
                    return new bnrynode(BINARY_NEQ, plusexpr, parseExpr(PLUSEXPR));
                case TOKEN_LT:
                    m_analyzer.next();
                    return new bnrynode(BINARY_LT, plusexpr, parseExpr(PLUSEXPR));
                case TOKEN_GT:
                    m_analyzer.next();
                    return new bnrynode(BINARY_GT, plusexpr, parseExpr(PLUSEXPR));
                default:
                    return plusexpr;
                }
            }
        case PLUSEXPR:
            {
                runenode *multexpr = parseExpr(MULTEXPR);
                while(m_analyzer.token()==TOKEN_ADD || m_analyzer.token()==TOKEN_SUB)
                {
                    if(m_analyzer.token() == TOKEN_ADD)
                    {
                        m_analyzer.next();
                        multexpr = new bnrynode(BINARY_ADD, multexpr, parseExpr(MULTEXPR));
                    }
                    else if(m_analyzer.token() == TOKEN_SUB)
                    {
                        m_analyzer.next();
                        multexpr = new bnrynode(BINARY_SUB, multexpr, parseExpr(MULTEXPR));
                    }
                }
                return multexpr;
            }
        case MULTEXPR:
            {
                runenode *fctr = parseExpr(FCTR);
                while(m_analyzer.token()==TOKEN_MUL || m_analyzer.token()==TOKEN_DIV)
                {
                    if(m_analyzer.token() == TOKEN_MUL)
                    {
                        m_analyzer.next();
                        fctr = new bnrynode(BINARY_MUL, fctr, parseExpr(FCTR));
                    }
                    else if(m_analyzer.token() == TOKEN_DIV)
                    {
                        m_analyzer.next();
                        fctr = new bnrynode(BINARY_DIV, fctr, parseExpr(FCTR));
                    }
                }
                return fctr;
            }
        case FCTR:
            switch(m_analyzer.token())
            {
            case TOKEN_GROUP_BEGIN:
                {
                    expect(TOKEN_GROUP_BEGIN);
                    runenode *e = parseExpr();
                    expect(TOKEN_GROUP_END);
                    return e;
                }
            case TOKEN_IDENTIFIER:
                {
                    string name = m_analyzer.strdata();
                    m_analyzer.next();
                    switch(m_analyzer.token())
                    {
                    case TOKEN_VEC_BEGIN:
                        {
                            expect(TOKEN_VEC_BEGIN);
                            runenode *i = parseExpr();
                            expect(TOKEN_VEC_END);
                            if(m_currentFunction->m_vars.find(name)==m_currentFunction->m_vars.end())
                            {
                                stringstream ss;
                                ss << "Vector " << name << " must be initialized before use";
                                throw ParserError(m_analyzer.line(), ss.str().c_str());
                            }
                            return new ivarnode(name, i);
                        }
                    case TOKEN_GROUP_BEGIN:
                        {
                            funcnode *f = new funcnode(name);
                            expect(TOKEN_GROUP_BEGIN);
                            while(m_analyzer.token() != TOKEN_GROUP_END)
                            {
                                if(f->m_args.size())
                                    expect(TOKEN_SEPARATOR);
                                f->m_args.push_back(parseExpr());
                            }
                            expect(TOKEN_GROUP_END);
                            return f;
                        }
                    case TOKEN_ASSIGN:
                        return new varnode(name);
                    default:
                        if(m_currentFunction->m_vars.find(name)==m_currentFunction->m_vars.end())
                        {
                            stringstream ss;
                            ss << "Variable " << name << " must be assigned before use";
                            throw ParserError(m_analyzer.line(), ss.str().c_str());
                        }
                        return new varnode(name);
                    }
                }
            case TOKEN_STRING:
                {
                    runenode *str = new strnode(m_analyzer.strdata());
                    m_analyzer.next();
                    return str;
                }
            case TOKEN_NUMBER:
                {
                    runenode *num = new numnode(m_analyzer.numdata());
                    m_analyzer.next();
                    return num;
                }
            case TOKEN_VEC_BEGIN:
                {
                    vecnode *vec = new vecnode();
                    expect(TOKEN_VEC_BEGIN);
                    while(m_analyzer.token() != TOKEN_VEC_END)
                    {
                        if(vec->m_elements.size())
                            expect(TOKEN_SEPARATOR);
                        vec->m_elements.push_back(parseExpr());
                    }
                    expect(TOKEN_VEC_END);
                    return vec;
                }
            case TOKEN_SUB:
                expect(TOKEN_SUB);
                return new unrynode(UNARY_NEG, parseExpr(FCTR));
            case TOKEN_NOT:
                expect(TOKEN_NOT);

                return new unrynode(UNARY_NOT, parseExpr(FCTR));
            }
            break;
        }

        stringstream ss;
        ss << "Unhandled expression parse on " << RuneStrings[m_analyzer.token()] 
            << " " << m_analyzer.strdata() << endl;
        throw ParserError(m_analyzer.line(), ss.str().c_str());
        return nullptr;
    }

    runenode *RuneParser::parseStmt()
    {
        switch(m_analyzer.token())
        {
        case TOKEN_IF:
            {
                m_analyzer.next();
                expect(TOKEN_GROUP_BEGIN);
                runenode *e = parseExpr();
                expect(TOKEN_GROUP_END);
                runenode *ifstmt = parseStmt();
                runenode *elsestmt = nullptr;
                if(m_analyzer.token() == TOKEN_ELSE)
                {
                    m_analyzer.next();
                    elsestmt = parseStmt();
                }
                return new ifnode(e, ifstmt, elsestmt);
            }
        case TOKEN_RETURN:
            {
                expect(TOKEN_RETURN);
                rtrnnode *rtrn = new rtrnnode(parseExpr());
                expect(TOKEN_SEMICOLON);
                return rtrn;
            }
        case TOKEN_WHILE:
            {
                expect(TOKEN_WHILE);
                expect(TOKEN_GROUP_BEGIN);
                whlenode *whle = new whlenode(parseExpr());
                expect(TOKEN_GROUP_END);
                whle->m_loop = parseStmt();
                return whle;
            }
        case TOKEN_BLOCK_BEGIN:
            {
                blcknode *block = new blcknode();
                expect(TOKEN_BLOCK_BEGIN);
                while(m_analyzer.token() != TOKEN_BLOCK_END)
                    block->m_stmts.push_back(parseStmt());
                expect(TOKEN_BLOCK_END);
                return block;
            }
        case TOKEN_SEMICOLON:
            expect(TOKEN_SEMICOLON);
            return new blcknode();
        default:
            {
                runenode *expr = parseExpr();
                expr = new popnode(expr);
                expect(TOKEN_SEMICOLON);
                return expr;
            }
        }
        
        stringstream ss;
        ss << "Unhandled statement parse on " << RuneStrings[m_analyzer.token()] 
            << " " << m_analyzer.strdata() << endl;
        throw ParserError(m_analyzer.line(), ss.str().c_str());
    }

    // helper function for replacing addresses
    void RuneCodeGenerator::replaceaddr(unsigned location, unsigned newaddr)
    {
        int bci = (unsigned)newaddr;
        for(unsigned i=0; i<4; ++i, bci>>=8)
            m_bytecode[location + i] = ((char)bci);
    }

    // returns true upon success and false upon failure
    bool RuneCodeGenerator::genCode(runenode *node)
    {
        // Generate code from the functions
        if(!node)
        {
            // Register native functions
            NativeFDef::registerfunction(&breakvm, "debug", "");
            NativeFDef::registerfunction(&rnum,   "rand", "nn");
            NativeFDef::registerfunction(&print1, "print1", "a");
            NativeFDef::registerfunction(&print2, "print2", "aa");
            NativeFDef::registerfunction(&print3, "print3", "aaa");
            NativeFDef::registerfunction(&print4, "print4", "aaaa");
            NativeFDef::registerfunction(&print5, "print5", "aaaaa");
            NativeFDef::registerfunction(&Renderer::getIndex, "objindex", "s");
            // NativeFDef::registerfunction(&Renderer::setanim, "setanim", "nn");
            NativeFDef::registerfunction(&Renderer::active, "active", "n");
            NativeFDef::registerfunction(&Renderer::activate, "activate", "n");
            NativeFDef::registerfunction(&Renderer::deactivate, "deactivate", "n");
            NativeFDef::registerfunction(&AnimBlender::nplay, "play", "sn");
            NativeFDef::registerfunction(&InputController::keystate, "keystate", "n");
            NativeFDef::registerfunction(&InputController::keydown, "keydown", "n");
            NativeFDef::registerfunction(&InputController::keyup, "keyup", "n");

            try
            {
                emit(BC_CALL);
                (m_main = (*m_functions)["main"])->m_references.push_back(unsigned(m_bytecode.size()));
                emit(0);
                emit(signed(m_main->m_vars.size()));
                emit(BC_ABRT);
                genCode(m_main);
                m_functions->erase("main");
                for_each(m_functions->begin(), m_functions->end(), [&](pair<string, fdefnode *> fdefpair)
                {
                    genCode(fdefpair.second);
                });
                linkCode();

                // 0 terminate the sequence so the writing exits / generally good practice
                emit(BC_ERR);
                return true;
            }
            catch (CompilerError e)
            {
                e.display();
                return false;
            }
        }

        switch(node->m_rtype)
        {
        case RT_FDEF:
            {
                fdefnode *fdef = (fdefnode *)node;
                m_currentFunction = fdef;
                fdef->m_address = unsigned(m_bytecode.size());
                genCode(fdef->m_body);

                // Create a return if one doesn't exist
                if(m_bytecode[m_bytecode.size()-5] != BC_RET)
                {
                    emit(BC_PUSH_VAL);
                    emit(0);
                    emit(BC_RET);
                    emit(signed(fdef->m_args.size()));
                }
                return true;
            }
        case RT_IF:
            {
                ifnode *ifn = (ifnode *)node;

                // Condition
                genCode(ifn->m_cond);
                emit(BC_JF);
                int jfcmdaddr = unsigned(m_bytecode.size());
                emit(0);

                // If Loop
                genCode(ifn->m_if);
                replaceaddr(jfcmdaddr, unsigned(m_bytecode.size()));

                // Optional else loop
                if(ifn->m_else)
                {
                    // Add a jump out of the if part over else
                    emit(BC_JUMP);
                    int jumpcmdaddr = unsigned(m_bytecode.size());
                    emit(0);

                    // re-replace If loop jf address
                    replaceaddr(jfcmdaddr, unsigned(m_bytecode.size()));

                    // generate the loop and exit
                    genCode(ifn->m_else);
                    replaceaddr(jumpcmdaddr, unsigned(m_bytecode.size()));
                }

                return true;
            }
        case RT_WHLE:
            {
                whlenode *whle = (whlenode *)node;

                // Condition
                int condaddr = unsigned(m_bytecode.size());
                genCode(whle->m_cond);
                emit(BC_JF);
                int jumpcmdaddr = unsigned(m_bytecode.size());
                emit(0);

                // Loop and auto-jump back
                genCode(whle->m_loop);
                emit(BC_JUMP);
                emit(condaddr);

                replaceaddr(jumpcmdaddr, unsigned(m_bytecode.size()));
                return true;
            }
        case RT_FUNC:
            {
                funcnode *func = (funcnode *)node;
                fdefnode *fdef;
                NativeFDef *nfdef;

                // Try func as a member-defined function
                if(!m_functions->count(func->m_name))
                {
                    // Try using func as a native function
                    if(!NativeFDef::s_deftable.count(func->m_name))
                    {
                        stringstream ss;
                        ss << "Undefined function \"" << func->m_name << "\"";
                        throw CompilerError(ss.str().c_str());
                    }
                    nfdef = &(NativeFDef::s_deftable[func->m_name]);
                }
                else
                    fdef = (*m_functions)[func->m_name];

                if(func->m_args.size() != (!m_functions->count(func->m_name)?nfdef->m_nargs:fdef->m_args.size()))
                {
                    stringstream ss;
                    ss << "Mismatched number of arguments in \"" << func->m_name << "\"";
                    throw CompilerError(ss.str().c_str());
                }
                for(unsigned i=0; i<func->m_args.size(); ++i)
                    genCode(func->m_args[i]);

                if(!m_functions->count(func->m_name))
                {
                    emit(BC_NCAL);
                    emit(nfdef->m_id);
                }
                else
                {
                    emit(BC_CALL);
                    fdef->m_references.push_back(unsigned(m_bytecode.size()));
                    emit(0);
                    emit(signed(fdef->m_vars.size()-fdef->m_args.size()));
                }
                return true;
            }
        case RT_BLCK:
            {
                blcknode *blck = (blcknode *)node;
                for(unsigned i=0; i<blck->m_stmts.size(); ++i)
                    genCode(blck->m_stmts[i]);
                return true;
            }
        case RT_VAR:
            {
                varnode *var = (varnode *)node;
                emit(BC_PUSH_VAR);
                emit(m_currentFunction->m_vars[var->m_name]);
                return true;
            }
        case RT_VEC:
            {
                vecnode *vec = (vecnode *)node;
                for(unsigned i=0; i<vec->m_elements.size(); ++i)
                    genCode(vec->m_elements[i]);
                emit(BC_PUSH_VEC);
                emit(signed(vec->m_elements.size()));
                return true;
            }
        case RT_IVAR:
            {
                ivarnode *ivar = (ivarnode *)node;
                genCode(ivar->m_index);
                emit(BC_PUSH_IVAR);
                emit(m_currentFunction->m_vars[ivar->m_name]);
                return true;
            }
        case RT_BNRY:
            {
                bnrynode *bnry = (bnrynode *)node;
                genCode(bnry->m_right);
                genCode(bnry->m_left);
                switch(bnry->m_btype)
                {
                case BINARY_ADD:    emit(BC_ADD);   return true;
                case BINARY_SUB:    emit(BC_SUB);   return true;
                case BINARY_MUL:    emit(BC_MUL);   return true;
                case BINARY_DIV:    emit(BC_DIV);   return true;
                // Assignments replace the PUSH_VAR command with the assign 2 ByteCodes previous
                case BINARY_ASSIGN:    m_bytecode[m_bytecode.size()-5] -= 2;   return true;
                case BINARY_LT:     emit(BC_LT);    return true;
                case BINARY_GT:     emit(BC_GT);    return true;
                case BINARY_EQ:     emit(BC_EQ);    return true;
                case BINARY_NEQ:    emit(BC_NEQ);   return true;
                case BINARY_AND:    emit(BC_AND);   return true;
                case BINARY_OR:     emit(BC_OR);    return true;
                default:
                    {
                        stringstream ss;
                        ss << "Undefined or unhandled Binary operator";
                        throw CompilerError(ss.str().c_str());
                    }
                }
            }
        case RT_UNRY:
            {
                unrynode *unry = (unrynode *)node;
                switch(unry->m_utype)
                {
                case UNARY_NEG:     genCode(unry->m_right);     emit(BC_NEG);   return true;
                case UNARY_NOT:     genCode(unry->m_right);     emit(BC_NOT);   return true;
                default:
                    {
                        stringstream ss;
                        ss << "Undefined or unhandled Unary operator";
                        throw CompilerError(ss.str().c_str());
                    }
                }
            }
        case RT_NUM:
            {
                numnode *num = (numnode *)node;
                emit(BC_PUSH_VAL);
                emit(num->m_data);
                return true;
            }
        case RT_STR:
            {
                strnode *str = (strnode *)node;
                emit(BC_PUSH_STR);
                emit(str->m_data.c_str());
                return true;
            }
        case RT_POP:
            {
                popnode *pop = (popnode *)node;
                genCode(pop->m_expr);
                emit(BC_POP);
                return true;
            }
        case RT_RTRN:
            {
                rtrnnode *rtrn = (rtrnnode *)node;
                genCode(rtrn->m_expr);
                emit(BC_RET);
                emit(signed(m_currentFunction->m_args.size()));
                return true;
            }
        default:
            {
                stringstream ss;
                ss << "Undefined or unhandled RuneNode";
                throw CompilerError(ss.str().c_str());
            }
        }
    }

    // emit helper functions for building bytecode
    void RuneCodeGenerator::emit(ByteCode bc)
    {
        m_bytecode.push_back((char)bc);
    }
    void RuneCodeGenerator::emit(float bcf)
    {
        int *fauxfloat = (int *)(&bcf);
        emit(*fauxfloat);
    }
    void RuneCodeGenerator::emit(int bci)
    {
        // TODO fix number parsing so negative values are parsed as values
        //      ... currently parsed as (-(3.14))
        // The positive nature of numbers passing through here is messily
        // dependent on the sign of the floats and integers being saved
        for(unsigned i=0; i<4; ++i, bci>>=8)
            m_bytecode.push_back((char)bci);
    }
    void RuneCodeGenerator::emit(const char *bcstr)
    {
        char *pstr = const_cast<char *>(bcstr);

        while(m_bytecode.back()!=0)
            m_bytecode.push_back(*pstr++);
    }

    // Revisits stored locations that need linking and replaces null values
    // with valid function locations
    void RuneCodeGenerator::linkCode()
    {
        // Link the main, should always be address 9, one reference, replaced at 1
        replaceaddr(m_main->m_references[0], m_main->m_address);

        // Link function calls
        for_each(m_functions->begin(), m_functions->end(), [&](pair<string, fdefnode *> f)
        {
            for(unsigned r=0; r<f.second->m_references.size(); ++r)
                replaceaddr(f.second->m_references[r], f.second->m_address);
        });
    }

    // Print functionality from parser down through various nodes
    void RuneParser::print()
    {
        for_each(m_functions.begin(), m_functions.end(), [&](pair<string, fdefnode *> fpair)
        {
            fpair.second->print();
        });
    }
    /*void RuneParser::output()
    {
        cout << g_ss.str();

        fstream file("output.txt", ios::out);
        file.write(g_ss.str().c_str(), g_ss.str().length());
        file.close();
    }*/

    // Print functionality for bytecode
    int asint(char *&bytecode)
    {
        int value=0;
        for(unsigned i=0; i<4; ++i)
        {
            unsigned t = unsigned char(*bytecode++);
            value|=t<<8*i;
        }
        return value;
    }
    float asfloat(char *&bytecode)
    {
        int x = asint(bytecode);
        return *(float *)(&x);
    }
    void RuneCodeGenerator::printCode()
    {
        for_each(m_functions->begin(), m_functions->end(), [&](pair<string, fdefnode *> fpair)
        { 
            delete fpair.second;
        });
        delete m_main;

        char *bcp = &m_bytecode[0];
        for(;;)
        {
            if(*bcp)    g_ss << bcp-&m_bytecode[0] << ": " << ByteCodeStrings[*bcp-0x41] << " ";
            switch(*bcp++)
            {
            case BC_ERR:
                return;
            case BC_CALL:
                g_ss << asint(bcp) << " ";
            case BC_JUMP:       
            case BC_JF:
            case BC_PUSH_VAR:
            case BC_PUSH_VEC:
            case BC_PUSH_IVAR:
            case BC_ASS:
            case BC_IASS:
            case BC_NCAL:
            case BC_RET:
                g_ss << asint(bcp) << endl;
                break;
            case BC_PUSH_VAL:
                g_ss << asfloat(bcp) << endl;
                break;
            case BC_PUSH_STR:
                g_ss << '\"' << bcp << "\"\n";
                while(*bcp++!=0);
                break;
            default:
                g_ss << endl;
                break;
            }
        }
    }

    int RuneRuntime::getiarg()
    {
        return asint(m_ip);
    }
    float RuneRuntime::getfarg()
    {
        return asfloat(m_ip);
    }
    string RuneRuntime::getsarg()
    {
        string sp;
        sp+=m_ip;
        while(*m_ip++!=0);
        return sp;
    }

// Handles binary numeric instructions.
#define BNI(instr, div)\
{\
    value &a = stack(1);\
    value &b = stack(2);\
    if(a.m_type!=V_NUM || b.m_type!=V_NUM)  throw RuntimeError(unsigned(m_ip-m_code)-1, "Must use Numeric Types");\
    if(div && b.m_numval==0)                throw RuntimeError(unsigned(m_ip-m_code)-1, "Divide by 0");\
    b.m_numval = a.m_numval instr b.m_numval;\
    pop();\
    break;\
}

#define UNI(instr)\
{\
    value &a = stack(1);\
    if(a.m_type!=V_NUM)             throw RuntimeError(unsigned(m_ip-m_code)-1, "Must use Numeric Types");\
    a.m_numval = instr(a.m_numval);\
    break;\
}

#define BI(instr)\
if(stack(1).m_type==V_NUM) BNI(instr, false)\
else\
{\
    value &a = stack(1);\
    value &b = stack(2);\
    if(a.m_type!=V_STR || b.m_type!=V_STR)  throw RuntimeError(unsigned(m_ip-m_code)-1, "Must use Numeric or String Types");\
    throw RuntimeError(unsigned(m_ip-m_code)-1, "Must use Numeric Types");\
    break;\
}

    // Native random number
    // Should destroy before handing to uneducated LDs
    value rnum(value &low, value &high)
    {
        value v; 
        v.m_type = V_NUM; 
        v.m_numval = rand()%int(high.m_numval-low.m_numval+1) + low.m_numval;
        if(v.m_numval<0) throw RuntimeError(0, "Illegal random number");
        return v;
    }

    // Native break function
    value breakvm()
    {
        // Place a breakpoint here
        value returnv; returnv.m_type = V_ERROR; returnv.m_fptr = 0xbadf00d;
        return returnv;
    }
    
    // Recursive print for abort and native print
    value print(value &v)
    {
        switch(v.m_type)
        {
        case V_NUM:
            g_ss << v.m_numval;
            break;
        case V_STR:
            g_ss << v.m_strval->v;
            break;
        case V_VEC:
            g_ss << '[';
            for(unsigned i=0; i<unsigned(v.m_vecval->v.size()); ++i)
            {
                print(v.m_vecval->v[i]);
                if(i < v.m_vecval->v.size()-1) 
                    g_ss << ", ";
            }
            g_ss << ']';
            break;
        default:
            g_ss << "Invalid return value from main";
        }
        value returnv; returnv.m_type = V_ERROR; returnv.m_fptr = 0xbadf00d;
        return returnv;
    }

    value print2(value &v1, value &v2)
    {
        print(v1); print(v2);
        value returnv; returnv.m_type = V_ERROR; returnv.m_fptr = 0xbadf00d;
        return returnv;
    }
    value print3(value &v1, value &v2, value &v3)
    {
        print(v1); print(v2); print(v3);
        value returnv; returnv.m_type = V_ERROR; returnv.m_fptr = 0xbadf00d;
        return returnv;
    }
    value print4(value &v1, value &v2, value &v3, value &v4)
    {
        print(v1); print(v2); print(v3); print(v4);
        value returnv; returnv.m_type = V_ERROR; returnv.m_fptr = 0xbadf00d;
        return returnv;
    }
    value print5(value &v1, value &v2, value &v3, value &v4, value &v5)
    {
        print(v1); print(v2); print(v3); print(v4); print(v5);
        value returnv; returnv.m_type = V_ERROR; returnv.m_fptr = 0xbadf00d;
        return returnv;
    }
    
    unsigned RefCountable::s_vecallocs = 0;
    unsigned RefCountable::s_strallocs = 0;
    unsigned RefCountable::s_vecdeallocs = 0;
    unsigned RefCountable::s_strdeallocs = 0;

    map<string, NativeFDef> NativeFDef::s_deftable;
    vector<NativeFDef> NativeFDef::s_deflist;

#define IndexCheck value &v = getlocal();\
if(v.m_type != V_VEC)   throw RuntimeError(unsigned(m_ip-m_code)-5, "Illegal indexing of non-vector variable");\
vector<value> &pvec = v.m_vecval->v;\
if(stack(1).m_type != V_NUM)    throw RuntimeError(unsigned(m_ip-m_code)-5, "Indices must be number values");\
if(unsigned(stack(1).m_numval) >= pvec.size() || int(stack(1).m_numval) < 0)   throw RuntimeError(unsigned(m_ip-m_code)-5, "Index out of bounds");

    void NativeFDef::registerfunction(void *fptr, char *name, char *args)
    {
        // Set up the native function definition
        NativeFDef nativefdef;
        nativefdef.m_fptr = fptr;
        nativefdef.m_nargs = 5;     // default to full, decrement on non-existant
        nativefdef.m_id = unsigned(s_deflist.size());
        for(unsigned i=0; i < 5; args+=(*args != 0), ++i)
        {
            switch(*args)
            {
            case 's':
                nativefdef.m_vtypes[i] = V_STR; break;
            case 'v':
                nativefdef.m_vtypes[i] = V_VEC; break;
            case 'n':
                nativefdef.m_vtypes[i] = V_NUM; break;
            default:
                nativefdef.m_nargs--;
            case 'a':   // ANY
                nativefdef.m_vtypes[i] = V_ERROR; break;
            }
        }
        
        // Registration
        if(NativeFDef::s_deftable.count(name))
        {
            stringstream ss;
            ss << "Redefined function " << name;
            throw CompilerError(ss.str().c_str());
        }
        NativeFDef::s_deftable[name] = nativefdef;
        NativeFDef::s_deflist.push_back(nativefdef);
    }

    VecVal::~VecVal()
    { 
        for(unsigned i=0; i<v.size(); ++i) 
            DEC(v[i]);
    }

    bool RuneRuntime::run()
    {
        m_ip = m_code;
        try
        {
            for(;;) switch(*m_ip++)
            {
            case BC_JUMP: m_ip = m_code+getiarg();  break;
            case BC_JF:
                {
                    char *tip=m_code+getiarg(); 
                    if(stack(1).m_numval==0)    
                        m_ip = tip;
                    pop();
                    break;
                }
            case BC_ASS:        
                {
                    value &v = getlocal();
                    INC(stack(1));      DEC(v);
                    v = stack(1);       break;
                }
            case BC_IASS:       
                {
                    IndexCheck
                    INC(stack(2));  DEC(pvec[int(stack(1).m_numval)]);
                    pvec[int(stack(1).m_numval)] = stack(2);  pop();  break;
                }
            case BC_PUSH_VAR:   
                {
                    if(m_stack.size()>10000) throw RuntimeError(unsigned(m_ip-m_code), "Stack overflow");
                    value &v = getlocal();
                    INC(v);     push(v);          break;
                }
            case BC_PUSH_IVAR:  
                {
                    if(m_stack.size()>10000) throw RuntimeError(unsigned(m_ip-m_code), "Stack overflow");
                    value &v = getlocal();
                    if(v.m_type != V_VEC)   throw RuntimeError(unsigned(m_ip-m_code)-5, "Illegal indexing of non-vector variable");
                    vector<value> &pvec = v.m_vecval->v;
                    if(stack(1).m_type != V_NUM)    
                        throw RuntimeError(unsigned(m_ip-m_code)-5, "Indices must be number values");
                    if(unsigned(stack(1).m_numval) >= pvec.size() || int(stack(1).m_numval) < 0)   throw RuntimeError(unsigned(m_ip-m_code)-5, "Index out of bounds");
                    value v2 = pvec[int(stack(1).m_numval)];
                    INC(v2);
                    pop();      push(v2);    break;
                }
            case BC_PUSH_VAL:
                {
                    if(m_stack.size()>10000) throw RuntimeError(unsigned(m_ip-m_code), "Stack overflow");
                    value v;    v.m_type=V_NUM;     v.m_numval=getfarg();     
                    push(v);    break;
                }
            case BC_PUSH_STR:
                {
                    if(m_stack.size()>10000) throw RuntimeError(unsigned(m_ip-m_code), "Stack overflow");
                    value v;    v.m_type=V_STR;
                    v.m_strval = new StrVal();
                    v.m_strval->v = getsarg();
                    INC(v);     RefCountable::s_strallocs++;
                    push(v);    break;
                }
            case BC_PUSH_VEC:
                {
                    if(m_stack.size()>10000) throw RuntimeError(unsigned(m_ip-m_code), "Stack overflow");
                    value v;    v.m_type=V_VEC;     
                    v.m_vecval    = new VecVal();
                    v.m_vecval->v.resize(getiarg());
                    for(int i=unsigned(v.m_vecval->v.size())-1; i>=0; --i)
                    {
                        v.m_vecval->v[i] = stack(1);
                        pop();
                    }
                    INC(v);     RefCountable::s_vecallocs++;
                    push(v);
                    break;
                }
            case BC_ADD:        BI(+);          // String concatenation
            case BC_SUB:        BNI(-, false);
            case BC_MUL:        BNI(*, false);
            case BC_DIV:        BNI(/, true);
            case BC_EQ:         BI(==);         // String / Vector equality check
            case BC_NEQ:        BI(!=);         // String / Vector equality check
            case BC_OR:         BNI(||, false);
            case BC_AND:        BNI(&&, false);
            case BC_GT:         BNI(>, false);
            case BC_LT:         BNI(<, false);
            case BC_POP:        DEC(stack(1));      pop();      break;
            case BC_NEG:        UNI(-);
            case BC_NOT:        UNI(!);
            case BC_CALL:
                {   
                    value v;
                    v.m_type=V_ADDR;    v.m_addr=m_ip+8;    char *tip=m_code+getiarg();      push(v);            
                    v.m_type=V_FPTR;    v.m_fptr=m_fp;      m_fp=unsigned(m_stack.size());   push(v);
                    v.m_type=V_ERROR;   v.m_addr = 0x00000000;
                    for(int i=0, n=getiarg(); i<n; ++i) 
                        push(v);
                    m_ip = tip;
                    break;
                }
            case BC_NCAL:
                {
                    unsigned funcIndex = getiarg();
                    value v;
                    NativeFDef &func = NativeFDef::s_deflist[funcIndex];
                    unsigned nargs = func.m_nargs;

                    // Check function arguments (V_ERROR in m_vtypes means any type allowed)
                    for(unsigned i=0; i<nargs; ++i)
                        if(func.m_vtypes[i] != V_ERROR && stack(nargs-i).m_type != func.m_vtypes[i])
                            throw RuntimeError(unsigned(m_ip-m_code)-1, "Invalid type in native function");

                    // Pick a function call
                    switch(nargs)
                    {
                    case 0:
                        v = fp0(func.m_fptr)(); break;
                    case 1:
                        v = fp1(func.m_fptr)(stack(1)); break;
                    case 2:
                        v = fp2(func.m_fptr)(stack(2), stack(1)); break;
                    case 3:
                        v = fp3(func.m_fptr)(stack(3), stack(2), stack(1)); break;
                    case 4:
                        v = fp4(func.m_fptr)(stack(4), stack(3), stack(2), stack(1)); break;
                    case 5:
                        v = fp5(func.m_fptr)(stack(5), stack(4), stack(3), stack(2), stack(1)); break;
                    }

                    // Resolve the stack
                    for(unsigned i=0; i<nargs; ++i)
                        { DEC(stack(1)); pop(); }
                    INC(v); push(v);
                    break;
                }
            case BC_RET:
                {
                    value v=stack(1);   INC(v); 
                    int nargs = getiarg();
                    int tfp = m_stack[m_fp].m_fptr;
                    while(m_stack.size() > unsigned(m_fp)) { DEC(stack(1)); pop(); }
                    m_ip = stack(1).m_addr; pop();
                    while(nargs--) { DEC(stack(1)); pop(); }
                    push(v);
                    m_fp = tfp;
                    break;
                }
            case BC_ABRT:
                {
                    g_ss << "\n\nFinal return value: ";
                    print(stack(1));
                    g_ss << '\n';
                    DEC(stack(1));   pop();
                    g_ss << "String Allocations:   " << RefCountable::s_strallocs << endl;
                    g_ss << "String Deallocations: " << RefCountable::s_strdeallocs << endl;
                    if(RefCountable::s_strdeallocs < RefCountable::s_strallocs)
                        g_ss << "Warning: Something is seriously wrong here\n";
                    g_ss << "Vector Allocations:   " << RefCountable::s_vecallocs << endl;
                    g_ss << "Vector Deallocations: " << RefCountable::s_vecdeallocs << endl;
                    if(RefCountable::s_vecdeallocs < RefCountable::s_vecallocs)
                        g_ss << "Warning: Vector Cycles detected\n";
                    return true;
                }
            default: throw RuntimeError(unsigned(m_ip-m_code)-1, "Unhandled instruction");
            }
        }
        catch(RuntimeError r)
        {
            r.display();
        }
        return false;
    }
    
    // Print functions for runenodes
    void varnode::print(int level)
    {
        tabs(level);    g_ss << m_name << endl;
    }

    void ivarnode::print(int level)
    {
        tabs(level);    g_ss << m_name << " @ \n";
        m_index->print(level+1);
    }

    void strnode::print(int level)
    {
        tabs(level);    g_ss << '"' << m_data << "\"\n";
    }

    void numnode::print(int level)
    {
        tabs(level);    g_ss << m_data << endl;
    }

    void vecnode::print(int level)
    {
        tabs(level);    g_ss << "vector\n";
        for(unsigned int i=0; i<m_elements.size(); ++i)
            m_elements[i]->print(level+1);
    }

    void ifnode::print(int level)
    {
        tabs(level);    g_ss << "if\n";
        m_cond->print(level+1);
        m_if->print(level+1);
        if(m_else)
        {
            tabs(level);    g_ss << "else\n";
            m_else->print(level+1);
        }
    }

    void whlenode::print(int level)
    {
        tabs(level);    g_ss << "while\n";
        m_cond->print(level+1);
        m_loop->print(level+1);
    }

    void rtrnnode::print(int level)
    {
        tabs(level);    g_ss << "return\n";
        m_expr->print(level+1);
    }

    void blcknode::print(int level)
    {
        tabs(level);    g_ss << "block\n";
        for(unsigned int i = 0; i < m_stmts.size(); ++i)
            m_stmts[i]->print(level+1);
    }

    void fdefnode::print(int level)
    {
        g_ss << m_name << " : \n";
        for(unsigned int i = 0; i < m_args.size(); ++i)
            m_args[i]->print(level+1);
        m_body->print(level+1);
    }

    void funcnode::print(int level)
    {
        tabs(level);    g_ss << m_name << " : \n";
        for(unsigned int i = 0; i < m_args.size(); ++i)
            m_args[i]->print(level+1);
    }

    void bnrynode::print(int level)
    {
        tabs(level);    g_ss << BinaryStrings[m_btype] << endl;
        m_left->print(level+1);
        m_right->print(level+1);
    }

    void unrynode::print(int level)
    {
        tabs(level);    g_ss << UnaryStrings[m_utype] << endl;
        m_right->print(level+1);
    }

    void popnode::print(int level)
    {
        tabs(level);    g_ss << "POP!\n";
        m_expr->print(level+1);
    }

    void LexicalError::display(){ g_ss << "Lexical Error (" << line << "): " << msg.c_str() << endl; }
    void ParserError::display(){ g_ss << "Parser Error (" << line << "): " << msg.c_str() << endl; }
    void CompilerError::display(){ g_ss << "Compiler Error: " << msg.c_str() << endl; }
    void RuntimeError::display(){ g_ss << "Runtime Error (" << ip << "): " << msg.c_str() << endl; }
}