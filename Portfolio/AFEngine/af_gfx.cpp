#include "af_gfx.h"
#include <stdio.h>

// link all the libraries
#pragma comment ( lib, "opengl32.lib" )
#pragma comment ( lib, "glu32.lib" )
#pragma comment ( lib, "glew32.lib" )

#pragma comment ( lib, "d3d11.lib" )
#ifdef _DEBUG
#pragma comment ( lib, "d3dx11d.lib" )
#else
#pragma comment( lib, "d3dx11.lib" )
#endif
#pragma comment ( lib, "d3dcompiler.lib" )
#pragma comment ( lib, "Effects11.lib" )

// Error checking used in DX sequences. Incomplete, currently just a debug break
void CheckForDxError(const char *file, int line, HRESULT hr)
{
    if (!FAILED(hr))
        return;

    // Cause the debugger to break here so we can fix the problem
    DebugBreak();
}
    
// Used for Shader Errors
void ShaderErrorandExit(const char* message)
{
    char buf[2048];
    wchar_t wbuf[2048];
    size_t nChar;
    sprintf_s(buf, "Shader Error: %s", message);
    mbstowcs_s(&nChar, wbuf, buf, 2048);
    OutputDebugString(wbuf);

    exit(-1);
}