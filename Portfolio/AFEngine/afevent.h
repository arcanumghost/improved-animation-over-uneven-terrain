#pragma once

#include "af_vec.h"
#include "XBOX_Controller.h"

namespace af
{
	// Types of events that can be registered
	enum keytype
	{
		KEY_PRESS,
		KEY_RELEASE,
		KEY_UP,
		KEY_DOWN
	};

	// Types of mouse events that can be registered
	enum mousetype
	{
		MOUSE_CLICK,
		MOUSE_UNCLICK,
		MOUSE_MOVE,
		MOUSE_DRAG,
		MOUSE_DBLCLICK
	};

    enum xboxtype
    {
        XBOX_PRESS,
        XBOX_RELEASE,
        XBOX_UP,
        XBOX_DOWN,
        XBOX_NEUTRAL_STICK,
        XBOX_ANGLED_STICK
    };

	// For self-documenting purposes, CODE Move to cpp?
	enum keycode
	{
		KEY_PRESSRELEASE = 1,
		KEY_UPDOWN = 128
	};

    // Data structure for key events
	struct keyeventtype
	{
		keyeventtype(keytype e, char k)
			: key(k), event(e)
		{}

		char key;
		keytype event;
	};

    // Data structure for mouse events
	struct mouseeventtype
	{
		mouseeventtype(mousetype e, char k)
			: key(k), event(e)
		{}

		char key;
		mousetype event;
	};

    // Data structure for mouse data. NOTE I have no idea what this is for?
	struct mouseeventdata
	{
		mouseeventdata(mouseeventtype e, vec2i p)
			: key(e.key), event(e.event), pos(p.x, p.y)
		{}

		char key;
		mousetype event;
		vec2i pos;
	};

    // Data structure for xbox events
    // keys begin with VK_PAD_ and are a full hex word.
    struct xboxeventtype
    {
        xboxeventtype(xboxtype e, WORD k)
            : key(k), event(e)
        {}

        WORD key;
        xboxtype event;
    };

    // Data structure for xbox data. NOTE I have no idea what this is for?
	struct xboxeventdata
	{
		xboxeventdata(xboxeventtype e, vec2f p)
			: key(e.key), event(e.event), pos(p.x, p.y)
		{}

		WORD key;
		xboxtype event;
		vec2f pos;
	};

	// Root function bind, templated by type of event as shown above (key, mouse, gamepad)
	template <typename eventargtype>
	struct rootfunctionbind
	{
		virtual void fire( eventargtype eventarg ) const = 0;
	};

	template <typename functiontype, typename classtype, typename eventargtype>
	struct functionbind : public rootfunctionbind<eventargtype>
	{
		functionbind(functiontype f, classtype l)
			: eventfunction(f), listener(l)
		{}
		
		functiontype eventfunction;
		classtype listener;

		virtual void fire( eventargtype eventarg ) const
		{
			(listener->*(eventfunction))( eventarg );
		}
	};
}