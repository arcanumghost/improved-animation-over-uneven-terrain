// 
#ifndef _AF_TEXTURE_H
#define _AF_TEXTURE_H

#include "af_utils.h"
#include "af_gfx.h"
#include <string>

namespace af
{
	class Texture
	{
	public:
		Texture();
		virtual ~Texture();

        // Various get methods
		std::wstring getFilePath() const;
		uint getWidth() const;
		uint getHeight() const;
		uint getTextureUint() const;
		bool hasAlpha() const;
		bool isLoaded() const;

        // Loads for textures
		virtual void loadFromMemory( uint width, uint height, uint *pRGBATexels, bool useAlpha ) = 0;
        void loadFromFile( const wchar_t *szTexturePath, bool targa = false, bool flippedNormalY = false );

	protected:
        uint textureWidth, textureHeight;
        bool m_flippedNormalY;
		std::wstring textureFileName;
		bool alpha;

        // Helper function
		bool isPowerOfTwo( uint x ) const;

    public:
		uint texture;
	};

    // OpenGL variant of the texture, uses texture uint for loading
    class GLTexture : public Texture
    {
        GLTexture::~GLTexture();
        void loadFromMemory( uint width, uint height, uint *pRGBATexels, bool useAlpha );
    };

    // DirectX variant of the texture, stores the device for easy access and
    // keeps track of its shader resource view
    class DXTexture : public Texture
    {
        ID3D11Device *m_device;

    public:
        DXTexture::~DXTexture();
        void setDevice(ID3D11Device *device) { m_device = device; }
        ID3D11ShaderResourceView *getShaderResource() { return m_tex; }
        void loadFromMemory( uint width, uint height, uint *pRGBATexels, bool useAlpha );
        ID3D11ShaderResourceView *m_tex;
    };
}

#endif