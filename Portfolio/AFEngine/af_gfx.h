#ifndef _AF_GRAPHICS_INCLUDES_H_
#define _AF_GRAPHICS_INCLUDES_H_

// OpenGL support
#include <GL\glew.h>
#include <gl\GL.h>
#include <gl\glu.h>

// DirectX support
#include <d3d11.h>
#include <d3dx11.h>
#include "d3dx11effect.h"
#include "d3dxGlobal.h"

// For debug breaks on DX errors
void CheckForDxError(const char *file, int line, HRESULT hr);
#define HV(hr) CheckForDxError(__FILE__,__LINE__,hr)

// For DX compiler errors
void ShaderErrorandExit(const char* message);
#define ShaderExit(msg) ShaderErrorandExit(msg);

#endif